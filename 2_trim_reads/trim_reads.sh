#!/usr/bin/env bash

STEP_NUMBER=2
ARGS="$@"

SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_steps.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)

assignPipelineFlags -c -a -p -T -E -S -C -j SEQ_COUNT_TRIMMING -m 21600 -J `basename $0`
assignArgs $ARGS
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ] || [ "$HELP" -eq "1" ]; then
    exit $EXIT_STATUS
fi
checkArgs
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit $EXIT_STATUS
fi

SAMPLES=( `getSamples $RUN_DIRECTORY` )
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "Step $STEP_NUMBER failed: Error extracting samples from SampleSheet.csv"
    exit $EXIT_STATUS
fi
NUM_SAMPLES=${#SAMPLES[@]}
if [[ ! $NUM_SAMPLES =~ ^[0-9]+$ ]]; then
    # shouldn't get here
    echoAndLogMessage "Step $STEP_NUMBER failed: num_samples calculated is not an integer: $NUM_SAMPLES"
    exit 1
fi

JOB_DIRECTORY=$RUN_DIRECTORY/`basename $SCRIPT_DIR`
mkdir -p $JOB_DIRECTORY
RUN_SCRIPT_TEMPLATE=$SCRIPT_DIR/run_trim_reads_template.sh
RUN_SCRIPT=$JOB_DIRECTORY/run_trim_reads.sh

# escape special characters for regex substitution
FORMATTED_JOB_NAME=${JOB_NAME//\//\\\/}
FORMATTED_JOB_NAME=${FORMATTED_JOB_NAME//@/\\@}
EMAIL_ADDRESS_CONFIG=${EMAIL_ADDRESS_CONFIG//@/\\@}
FORMATTED_JOB_DIRECTORY=${JOB_DIRECTORY//\//\\\/}
PARENT_JOB_CONFIG=${PARENT_JOB_CONFIG//\$/\\\$}

executeCmd "cp $RUN_SCRIPT_TEMPLATE $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/$FORMATTED_JOB_NAME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_SAMPLES\@/$NUM_SAMPLES/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_BASES_TO_KEEP\@/$NUM_BASES_TO_KEEP/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@STEP_NUMBER\@/$STEP_NUMBER/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@PARENT_JOB\@/$PARENT_JOB/g; $RUN_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;" $RUN_SCRIPT

RUN_CMD="qsub $RUN_SCRIPT"

function runCurrentStep {
    logMessage "$RUN_CMD"
    local RESPONSE=`submitScriptAsClusterJob $RUN_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$RESPONSE"
    fi
    local JOB_ID=`extractClusterJobIdFromSubmissionResponse "$RESPONSE"`
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$JOB_ID"
        exit $EXIT_STATUS
    fi
    local NEXT_STEP_NUMBER=$((STEP_NUMBER+1))
    local NEXT_STEP=${PIPELINE_STEPS[$((NEXT_STEP_NUMBER-1))]}
    local NEXT_CMD="`dirname $SCRIPT_DIR`/${NEXT_STEP_NUMBER}_${NEXT_STEP}/${NEXT_STEP}.sh -r $RUN_DIRECTORY"
    if [ ! -z "$EMAIL" ]; then
        NEXT_CMD+=" -e $EMAIL"
    elif [ "$NO_EMAIL" -eq "1" ]; then
        NEXT_CMD+=" -n"
    fi
    if [ "$AUTO_CASCADE" -eq "1" ]; then
        NEXT_CMD+=" -A"
    fi
    NEXT_CMD+=" -b $BASE_QUALITY_CUTOFF"
    NEXT_CMD+=" -a $AVERAGE_QUALITY_CUTOFF"
    NEXT_CMD+=" -H $MAX_HAMMING_DISTANCE"
    NEXT_CMD+=" -I $MAX_END_INDEL_DISTANCE"
    NEXT_CMD+=" -s $SEQUENCE_CHUNK_SIZE"
    NEXT_CMD+=" -c $MAX_NUM_CONCURRENT_TASKS"
    if [ "$NO_INDELS" -eq "1" ]; then
        NEXT_CMD+=" -i"
    fi
    NEXT_CMD+=" -U $ABACUS_USERNAME"
    NEXT_CMD+=" -o $REPORT_FILEPATH"
    NEXT_CMD+=" -P $JOB_ID -t"
    if [ ! -z "$LOG_FILE" ]; then
       NEXT_CMD+=" -L $LOG_FILE"
    fi

    if [ -z "$ABACUS_PASSWORD" ]; then
        if [ "$AUTO_CASCADE" -eq "1" ]; then
            echoAndLogMessage "Abacus password (-p) is required for report creation step; terminating after current step"
            exit 0
        fi
    else
        NEXT_CMD+=" -p $ABACUS_PASSWORD"
    fi
    if [ "$VERBOSE" -eq "1" ]; then
        NEXT_CMD+=" -v"
    fi

    if [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
        executeCmd "$NEXT_CMD" 0
    else
        echo
        echo "Next step: $NEXT_STEP. Run? (y/n)"
        read ANSWER
        if [[ "$ANSWER" =~ ^[Yy] ]]; then
            executeCmd "$NEXT_CMD" 0
        else
            if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
                echo "Response not understood - not proceeding with next step"
            fi
            echo "To run: $NEXT_CMD"
        fi
    fi
}

if [ "$AUTORUN" -eq "1" ] || [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
    runCurrentStep
else
    echo "Submit job now? (y/n)"
    read ANSWER
    if [[ "$ANSWER" =~ ^[Yy] ]]; then
        runCurrentStep
    else
        if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
            echo "Response not understood - not proceeding with current step"
        fi
        echoAndLogMessage "To run: $RUN_CMD"
    fi
fi
