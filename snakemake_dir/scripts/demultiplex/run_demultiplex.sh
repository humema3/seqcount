#!/bin/bash
#$ -S /bin/bash
#$ -N $JOB_NAME
#$ -wd $JOB_DIRECTORY
#$ -j y
#$ -l h_rt=$MAX_RUNTIME
#$ -l m_mem_free=$MAX_MEM
#$ -binding linear:1
#$ -m $EMAIL_NOTIFICATIONS
#$ -M $EMAIL_ADDRESS

# fill in using q or another pipeline manager, or by hand
# omission is fine
OPTS_STRING=$OPTS_STRING

BCL2FASTQ2=/usr/prog/bcl2fastq2/2.17.1.14/bin/bcl2fastq
STDERR_OUTFILE=$JOB_DIRECTORY/bcl2fastq2_log.txt

RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
CMD="$BCL2FASTQ2 -R $RUN_DIRECTORY $OPTS_STRING"
echo "$CMD 2> $STDERR_OUTFILE" >&2
$CMD 2> $STDERR_OUTFILE

#EXIT_STATUS=$?
#RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
#if [ ! -f $RUN_LOG_FILE ]; then
#    echo -e "JOB_ID\tDATE_INT\tDATE\tSTATUS" > $RUN_LOG_FILE
#fi
#echo -n -e "$JOB_ID\t`date +\"%s\"`\t`date +\"%F_%T\"`\t" >> $RUN_LOG_FILE
#if [ "$EXIT_STATUS" -eq "0" ]; then
#    echo "SUCCESS" >> $RUN_LOG_FILE
#    exit 0
#else
#    echo "FAIL" >> $RUN_LOG_FILE
#    exit 100
#fi