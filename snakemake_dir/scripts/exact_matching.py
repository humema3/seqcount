import gzip
import os
import pickle
import sys
from argparse import ArgumentParser
from datetime import datetime

# FASTQ file(s) 
# CRISPR guide library
# output: SMF id	Sequence	Count

def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def logMessage(message, toStdOut = False):
    logMessage = currentTime() + "\t\t" + message
    if toStdOut:
        print(logMessage)
    else:
        sys.stderr.write(logMessage + "\n")

def badQuality(qualLine, phredFormat = 33, baseCutoff = 15, averageCutoff = 25):
    totalScore = 0
    for charIndex in range(len(qualLine)):
        symbol = qualLine[charIndex]
        if type(symbol) == str:
            symbol = ord(symbol)
        phredScore = symbol - phredFormat
        if phredScore < baseCutoff:
            return True
        totalScore += phredScore

    return totalScore/len(qualLine) < averageCutoff

def processReadSequence(sequence, quality, countsMap, searchReverseComplements, noQualFilter, baseCutoff, averageCutoff):
    if not noQualFilter and badQuality(quality, baseCutoff=baseCutoff, averageCutoff=averageCutoff):
        return

    if sequence in countsMap:
        countsMap[sequence] += 1
    elif searchReverseComplements:
        r = rc(sequence)
        if r in countsMap:
            countsMap[r] += 1
        else:
            countsMap[sequence] = 1
    else:
        countsMap[sequence] = 1


def processLibSequence(smfId, sequence, library):
    if sequence in library and library[sequence] != smfId:
        raise AssertionError("More than one SMF id maps to the same sequence: " + library[sequence] + ", " + smfId + " --> " + sequence)
    library[sequence] = smfId

comp = {'A': 'T', 'T': 'A', 'G': 'C', 'C': 'G', 'N': 'N'}
def rc(seq):
    rc = ''
    for base in seq[::-1]:
        try:
            rc += comp[base]
        except KeyError:
            print("Invalid sequence " + seq)
            raise
    return rc

def processReadFile(readFile, countsMap, searchReverseComplements, noQualFilter, baseCutoff, averageCutoff):
    lineNum = 0
    sequence = ""
    quality = ""
    if readFile.endswith(".gz"):
        f = gzip.open(readFile, 'r')
    else:
        f = open(readFile, 'r')
    for line in f:
        if lineNum % 4 == 1:
            if lineNum % 4000000 == 1:
                logMessage("Processing sequence # " + str( int((lineNum - 1) / 4 + 1) ) + " in file " + readFile)
            if lineNum > 1:
                processReadSequence(sequence=sequence, quality=quality, countsMap=countsMap, searchReverseComplements=searchReverseComplements,
                                    noQualFilter=noQualFilter, baseCutoff=baseCutoff, averageCutoff=averageCutoff)
            sequence = line.strip()
        elif lineNum % 4 == 3:
           quality = line.strip()
        lineNum += 1
    if lineNum > 3:
        processReadSequence(sequence=sequence, quality=quality, countsMap=countsMap, searchReverseComplements=searchReverseComplements,
                                    noQualFilter=noQualFilter, baseCutoff=baseCutoff, averageCutoff=averageCutoff)
    f.close()

def processLibFile(libraryFile, library):
    lineNum = 0
    smfId = ""
    seq = ""
    f = open(libraryFile, 'r')
    for line in f:
        if lineNum % 2 == 0:
            if lineNum > 0:
                processLibSequence(smfId, seq, library)
            smfId = line.strip()[1:]
        else:
            seq = line.strip()
        lineNum += 1
    processLibSequence(smfId, seq, library)
    f.close()

def createLibrary(libraryFiles):
    library = {}
    for libraryFile in libraryFiles:
        processLibFile(libraryFile, library)
    return library

def decodeByteKeys(dic):
    decodedDict = {}
    for key, val in dic.items():
        if type(key) == bytes:
            decodedDict[key.decode("utf-8")] = val
        else:
            decodedDict[key] = val
    return decodedDict

def main(args):
    parser = ArgumentParser()
    parser.add_argument('-r', '--read_files', dest='read_files', required=False, default=None)
    parser.add_argument('-l', '--library_files', dest='library_files', required=False, default=None)
    parser.add_argument('--matches', dest='matches', required=False, default="matched.txt")
    parser.add_argument('--missing', dest='missing', required=False, default="unmatched.txt")
    parser.add_argument('--read_counts_pickle', dest='read_counts_pickle', required=False, default='read_counts.pickle')
    parser.add_argument('--library_pickle', dest='library_pickle', required=False, default='library.pickle')
    parser.add_argument('--load_counts_from_pickle', dest='load_counts_from_pickle', required=False, default=False, action='store_true')
    parser.add_argument('--load_lib_from_pickle', dest='load_lib_from_pickle', required=False, default=False, action='store_true')
    parser.add_argument('--search_reverse_complements', dest='search_reverse_complements', required=False, default=False, action='store_true')
    parser.add_argument('--no_qual_filter', dest='no_qual_filter', required=False, default=False, action='store_true')
    parser.add_argument('-b', '--base_cutoff', dest='base_cutoff', required=False, type=int, default=15)
    parser.add_argument('-a', '--average_cutoff', dest='average_cutoff', required=False, type=int, default=25)
    args = parser.parse_args(args)
    readFiles = args.read_files.split(",") if args.read_files is not None else None
    libraryFiles = args.library_files.split(",") if args.library_files is not None else None
    countsOutputFile = args.matches
    missingSeqsFile = args.missing
    readCountsPickle = args.read_counts_pickle
    libraryPickle = args.library_pickle
    loadCountsFromPickle = args.load_counts_from_pickle
    loadLibFromPickle = args.load_lib_from_pickle
    searchReverseComplements = args.search_reverse_complements
    baseCutoff = args.base_cutoff
    averageCutoff = args.average_cutoff
    noQualFilter = args.no_qual_filter
    if not loadCountsFromPickle and readFiles is None:
        raise ValueError("Must supply -read_files unless -load_counts_from_pickle is used")
    if not loadLibFromPickle and libraryFiles is None:
        raise ValueError("Must supply -library_files unless -load_lib_from_pickle is used")

    if loadCountsFromPickle:
        logMessage("Loading sequence counts from pickle " + readCountsPickle)
        countsMap = pickle.load(open(readCountsPickle, 'rb'))
    else:
        logMessage("Tabulating sequence counts from FASTQ files " + ", ".join(readFiles))
        countsMap = {}
        for readFile in readFiles:
            processReadFile(readFile=readFile, countsMap=countsMap, searchReverseComplements=searchReverseComplements,
                            noQualFilter=noQualFilter, baseCutoff=baseCutoff, averageCutoff=averageCutoff)
        logMessage("Saving counts map as pickle to " + readCountsPickle)
        pickle.dump(countsMap, file=open(readCountsPickle, 'wb'))

    # readFiles = snakemake.input.split(",")
    # libraryFiles = snakemake.params['libraries'].split(",")
    # noQualFilter = snakemake.params['no_qual_filter']
    # baseCutoff = snakemake.params['base_cutoff']
    # averageCutoff = snakemake.params['average_cutoff']
    # readCountsPickle = snakemake.params['read_counts_pickle']
    # libraryPickle = snakemake.params['library_pickle']
    # countsOutputFile = os.path.join(os.path.dirname(snakemake.output), 'matched.txt')
    # missingSeqsFile = os.path.join(os.path.dirname(snakemake.output), 'unmatched.txt')
    # searchReverseComplements = False
    # if os.path.isfile(readCountsPickle):
    #     logMessage("Loading sequence counts from pickle " + readCountsPickle)
    #     countsMap = pickle.load(open(readCountsPickle, 'rb'))
    # else:
    #     logMessage("Tabulating sequence counts from FASTQ files " + ", ".join(readFiles))
    #     countsMap = {}
    #     for readFile in readFiles:
    #         processReadFile(readFile=readFile, countsMap=countsMap, searchReverseComplements=searchReverseComplements,
    #                         noQualFilter=noQualFilter, baseCutoff=baseCutoff, averageCutoff=averageCutoff)
    #     logMessage("Saving counts map as pickle to " + readCountsPickle)
    #     pickle.dump(countsMap, file=open(readCountsPickle, 'wb'))
    # if os.path.isfile(libraryPickle):
    #     logMessage("Loading library from pickle " + libraryPickle)
    #     library = pickle.load(open(libraryPickle, 'rb'))
    # else:
    #     logMessage("Creating library dictionary")
    #     library = createLibrary(libraryFiles)
    #     logMessage("Saving library as pickle to " + libraryPickle)
    #     pickle.dump(library, file=open(libraryPickle, 'wb'))

    logMessage("Size of library: " + str(len(library)))

    logMessage("Decoding byte strings")
    countsMap = decodeByteKeys(countsMap)

    logMessage("Searching library for found sequences")
    missing = {}
    libSeqs = {}
    nonLibCounts = []
    # print("Sorting default counts: " + currentTime())
    unmatchedLibSeqs = []
    for seq in library:
        if seq not in countsMap:
            countsMap[seq] = 0
    # counts = sorted(countsMap.items(), key = lambda x: (-1*x[1],x[0]))
    counts = countsMap.items()
    logMessage("Compiling sequences in/not in library")
    for seq, count in counts:
        if seq in library:
            libSeqs[library[seq]] = (seq, count)
        elif searchReverseComplements:
            revComp = rc(seq)
            if revComp in library:
                libSeqs[library[revComp]] = (revComp, count)
            else:
                nonLibCounts.append((seq, count))
        else:
            nonLibCounts.append((seq, count))
    logMessage("Sorting library sequences by count and SMF id")
    libCounts = sorted(libSeqs.items(), key = lambda x: (-1*x[1][1], x[0]))
    logMessage("Sorting non-library sequences by count and sequence")
    nonLibCounts = sorted(nonLibCounts, key = lambda x: (-1*x[1], x[0]))
    if type(nonLibCounts[0][0]) == bytes:
        nonLibCounts = [(seq.decode("utf-8"), count) for seq, count in nonLibCounts]

    logMessage("Writing matches to " + countsOutputFile)
    f = open(countsOutputFile, 'w')
    for count in libCounts:
        smfId = count[0]
        sequence = count[1][0]
        countVal = count[1][1]
        f.write(smfId + "\t" + sequence + "\t" + str(countVal) + "\n")
    f.close()

    for count in nonLibCounts:
        sequence = count[0]
        countVal = count[1]
        if sequence in missing:
            missing[sequence] += countVal
        elif searchReverseComplements:
            revComp = rc(sequence)
            if revComp in missing:
                missing[revComp] += countVal
            else:
                missing[sequence] = countVal
        else:
            missing[sequence] = countVal

    logMessage("Writing missing sequences and counts to " + missingSeqsFile)
    f = open(missingSeqsFile, 'w')
    missingCounts = sorted(missing.items(), key = lambda x: (-1*x[1], x[0]))
    for sequence in missingCounts:
        line = sequence[0]
        if searchReverseComplements:
            line += "\t" + rc(sequence[0])

        try:
            line += "\t" + str(sequence[1]) + "\n"
        except TypeError as e:
            raise e
        f.write(line)
    f.close()
    logMessage("Done")

if __name__ == "__main__":
    main(sys.argv[1:])
