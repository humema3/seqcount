#!/usr/bin/env bash

VERBOSE=@VERBOSE@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "$RUN_DIRECTORY/util_functions.sh not found" >&1 >&2
    exit 100
fi
source $RUN_DIRECTORY/util_functions.sh
if [ ! -f $RUN_DIRECTORY/pipeline_steps.sh ]; then
    echo "$RUN_DIRECTORY/pipeline_steps.sh not found" >&1 >&2
    exit 100
fi
source $RUN_DIRECTORY/pipeline_steps.sh
SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv

SAMPLES=( `getSamples $RUN_DIRECTORY` )
NUM_SAMPLES=${#SAMPLES[@]}
NUM_PROJECTS=`getNumProjects $RUN_DIRECTORY`

STEP_NUMBER=@STEP_NUMBER@
PARENT_JOB=@PARENT_JOB@
if [ ! -z "$PARENT_JOB" ]; then
    waitForClusterJob $PARENT_JOB
    PREV_STEP_NUMBER=$((STEP_NUMBER-1))
    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
    NUM_TASKS_TO_CHECK=$NUM_SAMPLES
    TASK_CHECK=`checkSuccessfulTasks $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB $NUM_TASKS_TO_CHECK`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$TASK_CHECK" >&1 >&2
        exit $EXIT_STATUS
    fi
fi

CREATE_REPORT_SCRIPT=$JOB_DIRECTORY/create_report.py
ABACUS_USERNAME=@ABACUS_USERNAME@
ABACUS_PASSWORD=@ABACUS_PASSWORD@
OUTPUT_FILEPATH=@OUTPUT_FILEPATH@

SAMPLES=( `getSamples $RUN_DIRECTORY` )

SAMPLE_SMF_IDS=""
COUNTS_FILES=""
UNMATCHED_FILES=""
for SAMPLE in ${SAMPLES[@]}; do
    SAMPLE_INFO=( `echo $SAMPLE | tr "," "\n"` )
    SAMPLE_NAME=${SAMPLE_INFO[0]}
    SAMPLE_PROJECT=${SAMPLE_INFO[2]}
    SAMPLE_SMF_IDS+=${SAMPLE_NAME},
    SAMPLE_OUTPUT_DIR=$RUN_DIRECTORY/Aligned/Seeded/$SAMPLE_PROJECT/$SAMPLE_NAME
    COUNTS_FILES+=${SAMPLE_OUTPUT_DIR}/matched.txt,
    UNMATCHED_FILES+=${SAMPLE_OUTPUT_DIR}/unmatched.txt,
done
SAMPLE_SMF_IDS=`chop $SAMPLE_SMF_IDS`
COUNTS_FILES=`chop $COUNTS_FILES`
UNMATCHED_FILES=`chop $UNMATCHED_FILES`

PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python
CMD="$PYTHON -u $CREATE_REPORT_SCRIPT -s $SAMPLE_SMF_IDS -c $COUNTS_FILES -u $UNMATCHED_FILES -o $OUTPUT_FILEPATH --sample_sheet $SAMPLE_SHEET -p $ABACUS_PASSWORD --abacus_user $ABACUS_USERNAME -n $NUM_PROJECTS"
if [ "$VERBOSE" -eq "1" ]; then
    echo "$CMD" >&2
fi
$CMD
EXIT_STATUS=$?

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
if [ ! -f $RUN_LOG_FILE ]; then
    echo -e "JOB_ID\tDATE\tSTATUS" > $RUN_LOG_FILE
fi
echo -n -e "$$\t`date +\"%F_%T\"`\t" >> $RUN_LOG_FILE
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "FAIL" >> $RUN_LOG_FILE
    exit 100
else
    echo "SUCCESS" >> $RUN_LOG_FILE
    exit 0
fi