#!/usr/bin/env bash

ARGS="$@"
declare -A ALIGN_PERCENTAGES
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh

assignPipelineFlags -J compare_results
assignArgs $ARGS

function parseOldAlignmentPercentage {
    local SAMPLE_SMF_ID=$1
    local STATS_FILE=$2
    local RESULT
    if [ "${#ALIGN_PERCENTAGES}" -eq "0" ]; then
        local PARSING_SCRIPT=$SCRIPT_DIR/parse_old_align_percentages.py
        local RESULTS=( `$PYTHON -u $PARSING_SCRIPT $STATS_FILE` )
        for RESULT in ${RESULTS[@]}; do
            local RESULT_ARR=( `splitString $RESULT ,` )
            ALIGN_PERCENTAGES[${RESULT_ARR[0]}]=${RESULT_ARR[1]}
        done
    fi
    echo ${ALIGN_PERCENTAGES[$SAMPLE_SMF_ID]}
}

RUN_DIRECTORY=$1
RESULTS_DIR=$RUN_DIRECTORY/Aligned
OLD_RESULTS_DIR=$RESULTS_DIR/countsnew
NEW_RESULTS_DIR=$RESULTS_DIR/Seeded
#SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
OUTPUT_FILE=$RUN_DIRECTORY/compare_results.txt

FCID=`getFlowcellId $RUN_DIRECTORY`
SAMPLES=( `getSamples $RUN_DIRECTORY` )
echo -e "SAMPLE_SMF_ID\tOLD_NUM_READS_ALIGNED\tOLD_PERCENT_READS_ALIGNED\tNEW_NUM_READS_ALIGNED\tNEW_PERCENT_READS_ALIGNED" > $OUTPUT_FILE
for SAMPLE in ${SAMPLES[@]}; do
    SAMPLE_INFO=( `echo $SAMPLE | tr "," "\n"` )
    SAMPLE_SMF_ID=${SAMPLE_INFO[0]}
    #SAMPLE_NUM_ID=${SAMPLE_INFO[1]}
    SAMPLE_POOL=${SAMPLE_INFO[2]}
    INDEX=`getIndex $RUN_DIRECTORY $SAMPLE_SMF_ID`
    COUNTS_FILENAME=${SAMPLE_SMF_ID}_${INDEX}_${SAMPLE_POOL}_FCID${FCID}_count.csv
    OLD_COUNTS_FILE=$OLD_RESULTS_DIR/$COUNTS_FILENAME
    NEW_COUNTS_FILE=$NEW_RESULTS_DIR/$SAMPLE_POOL/$SAMPLE_SMF_ID/$COUNTS_FILENAME
    NEW_MATCHED_FILE=$NEW_RESULTS_DIR/$SAMPLE_POOL/$SAMPLE_SMF_ID/matched.txt
    NEW_UNMATCHED_FILE=`dirname $NEW_MATCHED_FILE`/unmatched.txt
    OLD_STATS_FILE=$RESULTS_DIR/Project_${SAMPLE_POOL}/Summary_Stats_${FCID}/Sample_Summary.htm
    OLD_PCT_READS_ALIGNED=`parseOldAlignmentPercentage $SAMPLE_SMF_ID $OLD_STATS_FILE`
    NEW_PCT_READS_ALIGNED=`calculateNewAlignmentPercentage $NEW_MATCHED_FILE $NEW_UNMATCHED_FILE`
    OLD_NUM_READS_ALIGNED=0
    for n in `cut -d',' -f2 $OLD_COUNTS_FILE`; do
        if [[ "$n" =~ ^# ]]; then # reached reference counts
            break
        fi
        OLD_NUM_READS_ALIGNED=$((OLD_NUM_READS_ALIGNED+n))
    done
    NEW_NUM_READS_ALIGNED=0
    for n in `cut -d',' -f2 $NEW_COUNTS_FILE`; do
        if [[ "$n" =~ ^# ]]; then # reached reference counts
            break
        fi
        NEW_NUM_READS_ALIGNED=$((NEW_NUM_READS_ALIGNED+n))
    done
    echo -e "$SAMPLE_SMF_ID\t$OLD_NUM_READS_ALIGNED\t$OLD_PCT_READS_ALIGNED\t$NEW_NUM_READS_ALIGNED\t$NEW_PCT_READS_ALIGNED" >> $OUTPUT_FILE
done
