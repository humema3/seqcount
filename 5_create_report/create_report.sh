#!/usr/bin/env bash

STEP_NUMBER=5
ARGS="$@"

SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_steps.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)

assignPipelineFlags -p -C -J `basename $0`
assignArgs $ARGS
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ] || [ "$HELP" -eq "1" ]; then
    exit $EXIT_STATUS
fi
checkArgs
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit $EXIT_STATUS
fi

SAMPLES=( `getSamples $RUN_DIRECTORY` )
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "Step $STEP_NUMBER failed: Error extracting samples from SampleSheet.csv"
    exit $EXIT_STATUS
fi
NUM_SAMPLES=${#SAMPLES[@]}

# check that last step succeeded in most recent attempt
if [ ! -z "$PARENT_JOB" ]; then
    echoAndLogMessage "Waiting for cluster job $PARENT_JOB to finish..."
    waitForClusterJob $PARENT_JOB
    PREV_STEP_NUMBER=$((STEP_NUMBER-1))
    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
    TASK_CHECK=`checkSuccessfulTasks $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB $NUM_SAMPLES`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$TASK_CHECK"
        exit $EXIT_STATUS
    fi
fi

RUN_SCRIPT_TEMPLATE=$SCRIPT_DIR/run_create_report_template.sh
JOB_DIRECTORY=$RUN_DIRECTORY/`basename $SCRIPT_DIR`
mkdir -p $JOB_DIRECTORY
RUN_SCRIPT=$JOB_DIRECTORY/run_create_report.sh

# escape special characters for regex substitution
FORMATTED_JOB_DIRECTORY=${JOB_DIRECTORY//\//\\\/}
REPORT_FILEPATH=${REPORT_FILEPATH//\//\\\/}
ABACUS_PASSWORD=${ABACUS_PASSWORD//\//\\\/}
ABACUS_PASSWORD=${ABACUS_PASSWORD//@/\\@}
ABACUS_PASSWORD=${ABACUS_PASSWORD//\$/\\\$}
ABACUS_PASSWORD=${ABACUS_PASSWORD//%/\\%}

executeCmd "cp `dirname $SCRIPT_DIR`/util_functions.sh -t $RUN_DIRECTORY"
executeCmd "cp `dirname $SCRIPT_DIR`/pipeline_steps.sh -t $RUN_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/create_report.py -t $JOB_DIRECTORY"

executeCmd "cp $RUN_SCRIPT_TEMPLATE $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@ABACUS_USERNAME\@/$ABACUS_USERNAME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@ABACUS_PASSWORD\@/$ABACUS_PASSWORD/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@OUTPUT_FILEPATH\@/$REPORT_FILEPATH/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@PARENT_JOB\@/$PARENT_JOB/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@STEP_NUMBER\@/$STEP_NUMBER/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $RUN_SCRIPT"

RUN_CMD="bash $RUN_SCRIPT"

function runCurrentStep {
    logMessage "$RUN_CMD"
    $RUN_CMD
}

if [ "$AUTORUN" -eq "1" ] || [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
    runCurrentStep
else
    echo "Submit now? (y/n)"
    read ANSWER
    if [[ "$ANSWER" =~ ^[Yy] ]]; then
        runCurrentStep
    else
        if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
            echo "Response not understood - not running"
        fi
        echo "To run: $RUN_CMD"
    fi
fi