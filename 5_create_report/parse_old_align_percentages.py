import sys
from html.parser import HTMLParser

class AlignPercentageParser(HTMLParser):
    def __init__(self, filepath):
        HTMLParser.__init__(self)
        self.startTag = "Sample Results Summary : Read 1"
        self.endTag = "Sample Results Summary : Read 2"
        self.inProgress = False
        self.thCount = 0
        self.tdCount = 0
        self.sampleToPercent = {}
        self.colNum = -1
        with open(filepath, 'r') as f:
            for line in f.readlines():
                self.feed(line.strip())

    def handle_starttag(self, tag, attrs):
        if tag == "tr":
            self.tdCount = 0
        elif tag == "th":
            self.thCount += 1
        elif tag == "td":
            self.tdCount += 1

    def handle_data(self, data):
        if data == self.startTag:
            self.inProgress = True
            self.thCount = -1 # don't count the top "Sample Info" header line
        elif data == self.endTag:
            self.inProgress = False
        elif self.inProgress:
            if data == "% Align (PF)":
                self.colNum = self.thCount
            elif self.tdCount == 1:
                self.sampleSmfId = data
            elif self.tdCount == self.colNum:
                self.sampleToPercent[self.sampleSmfId] = data

    def print_percentage(self, sampleSmfId):
        print(sampleSmfId + "," + self.sampleToPercent[sampleSmfId])

    def print_percentages(self):
        for sample in self.sampleToPercent:
            self.print_percentage(sample)

if __name__ == "__main__":
    parser = AlignPercentageParser(sys.argv[1])
    parser.print_percentages()
