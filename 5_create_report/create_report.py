import cx_Oracle as odb
import pandas as pd
import os
import re
import sys
import warnings
from argparse import ArgumentParser
from collections import defaultdict
from datetime import datetime

def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def warn(warning, file):
    warnings.warn(warning)
    with open(file, 'a') as f:
        f.write(warning + "\n")

def fetchCellLineInfoFromAbacus(smfId, conn):
    # query = "SELECT DISTINCT CELLLINE_NAME, POOL_NAME, COMPOUND_NAME FROM ABACUS.AB_SAMPLE_RESULTS_V WHERE SAMPLE_SMF_ID = '" + smfId + "'"
    query = "SELECT DISTINCT BIOSPECIMEN_NAME, POOL_NAME, AGENT_NAME FROM ABACUS.AB_SAMPLE_DETAILS_V WHERE PHYSICAL_SMF_ID = '" + smfId + "'"
    df = pd.read_sql(query, conn)
    if len(df) == 0:
        return (None, None, None)
    # assert result is not None, "No cell line info found for SMF id " + smfId
    assert len(df) == 1, "More than one row of cell line info found for SMF id " + smfId
    result = df.iloc[0]
    return (result.BIOSPECIMEN_NAME, result.POOL_NAME.replace(" ", "_"), result.AGENT_NAME)

def parseFromSampleSheet(smfId, sampleSheet, numPools):
    f = open(sampleSheet, 'r')
    headerFound = False
    columnNumbers = None
    fcid = None
    sampleId = None
    index = None
    lanes = []
    pool = None
    for line in f:
        line = line.strip()
        if headerFound:
            fields = line.split(",")
            if fields[columnNumbers["Sample_Name"]] == smfId:
                assert fcid is None or fcid == fields[columnNumbers["FCID"]], "More than one flowcell id for SMF id " + smfId + ": " + fcid + ", " + fields[columnNumbers["FCID"]]
                fcid = fields[columnNumbers["FCID"]]
                lanes.append(fields[columnNumbers["Lane"]])
                assert sampleId is None or sampleId == int(fields[columnNumbers["Sample_Id"]]), "More than one numeric sample id for SMF id " + smfId + ": " + str(sampleId) + ", " + fields[columnNumbers["Sample_Id"]]
                sampleId = int(fields[columnNumbers["Sample_Id"]])
                assert index is None or index == fields[columnNumbers["index"]], "More than one index for SMF id " + smfId + ": " + index + ", " + fields[columnNumbers["index"]]
                index = fields[columnNumbers["index"]]
                assert pool is None or pool == fields[columnNumbers["Sample_Project"]], "More than one pool for SMF id " + smfId + ": " + index + ", " + fields[columnNumbers["Sample_Project"]]
                pool = fields[columnNumbers["Sample_Project"]]
        else:
            m = re.match(r'^\[Data\]', line)
            if m is not None:
                headerFound = True
                columnHeader = f.readline().strip().split(",")
                columnNumbers = {}
                for i in range(len(columnHeader)):
                    columnNumbers[columnHeader[i]] = i
    f.close()
    assert headerFound, "No column header found for sample sheet " + sampleSheet
    assert fcid is not None, "Error: Flowcell id not found for SMF id " + smfId
    assert sampleId is not None, "Error: Sample id not found for SMF id " + smfId
    assert index is not None, "Error: Index not found for SMF id " + smfId
    assert len(lanes) > 0, "Error: Lane not found for SMF id " + smfId
    assert pool is not None, "Error: Pool not found for SMF id " + smfId
    lanes = ",".join(map(str, sorted(map(int, lanes))))
    pool = pool.replace(" ", "_")
    isReference = sampleId <= numPools
    return (fcid, lanes, sampleId, index, isReference, pool)

def countAlignedReads(countsFile):
    numMatched = 0
    c = open(countsFile, 'r')
    for line in c:
        numMatched += float(line.strip().split("\t")[2])
    c.close()
    return numMatched

def countUnalignedReads(unmatchedFile):
    numUnmatched = 0
    u = open(unmatchedFile, 'r')
    for line in u:
        numUnmatched += int(line.strip().split("\t")[1])
    u.close()
    return numUnmatched


def fetchBarcodeFromAbacus(smfId, conn, runDate, warningsFile):
    query = "SELECT c.BARCODE, c.CREATED_ON FROM ABACUS.AB_SAMPLE s, ABACUS.AB_SAMPLE_CONTAINER sc, ABACUS.AB_CONTAINER c, ABACUS.AB_STATUS st WHERE s.PHYSICAL_SMF_ID = '" + smfId + "' AND s.ID = sc.SAMPLE_ID AND sc.CONTAINER_ID = c.ID AND c.INDEX_PRIMER_ID IS NOT NULL AND s.USER_STATUS_ID = st.ID AND st.NAME IN ('in progress', 'completed')"
    # cursor.execute(query)
    df = pd.read_sql(query, conn)
    # assert result is not None, "No barcode found for SMF id " + smfId
    if len(df) == 0:
        return None
    result = df.iloc[0]
    barcode = result.BARCODE
    row = 1
    # assert result2 is None, "More than one barcode found for SMF id " + smfId + ": " + result[0] + ", " + result2[0]
    dateFormat = '%Y-%m-%d %H:%M:%S.%f'
    firstDuplicate = True
    while row < len(df):
        result2 = df.iloc[row]
        if firstDuplicate:
            warn("More than one barcode found for SMF id " + smfId + ": " + str(result.BARCODE) + ", " + str(result2.BARCODE), warningsFile)
            firstDuplicate = False
        else:
            warn("Another barcode found for SMF id " + smfId + ": " + str(result2.BARCODE), warningsFile)
        date1 = result.CREATED_ON
        date2 = result2.CREATED_ON
        dist1 = abs(date1 - runDate)
        dist2 = abs(date2 - runDate)
        if dist1 < dist2:
            barcode = result.BARCODE
        elif dist1 > dist2:
            barcode = result2.BARCODE
        else:
            raise ValueError("More than one barcode found for SMF id " + smfId + ", and both containers have equal creation timestamps: " + str(result.BARCODE) + " = " + str(date1) + "; " + str(result2.BARCODE) + " = " + str(date2))
        result = result2
        row += 1
    return barcode


def fetchReferenceSamplePoolFromAbacus(smfId, conn):
    query = "SELECT CONCEPT FROM ABACUS.AB_POOL_MV WHERE SAMPLE_SMF_ID = '" + smfId + "'"
    df = pd.read_sql(query, conn)
    assert len(df) > 0, "No pool found for SMF id " + smfId
    result = df.iloc[0]
    assert len(df) == 1, "More than one pool found for SMF id " + smfId + ": " + result.CONCEPT + ", " + df.iloc[1].CONCEPT
    return result.CONCEPT.replace(" ", "_")


def buildDataFrame(files, sampleSheet, numPools, abacusConn, warningsFile, runDate):
    # header = ['#', 'FCID', '2D_BARCODE', 'SAMPLE_SMF_ID', 'CELL_LINE', 'COMPOUND', 'ALIGNED_READS', 'UNALIGNED_READS', 'FCID', 'ALIGNMENT_PERCENT', 'POOL_NAME', 'LANES', 'INDEX', 'COMMENT']
    sampleNum = 1
    referenceSamples = defaultdict(list)
    nonReferenceSamples = defaultdict(list)
    for smfId, (countsFile, unmatchedFile) in files:
        fcid, lanes, sampleId, index, isReference, sampleSheetPool = parseFromSampleSheet(smfId, sampleSheet, numPools)
        alignedReadCount = countAlignedReads(countsFile)
        unalignedReadCount = countUnalignedReads(unmatchedFile)
        totalReadCount = alignedReadCount + unalignedReadCount
        if isReference:
            referenceSamples['#'].append('P')
            referenceSamples['FCID'].append(fcid)
            referenceSamples['LANES'].append(lanes)
            referenceSamples['INDEX'].append(index)
            referenceSamples['2D_BARCODE'].append('N/A')
            referenceSamples['SAMPLE_SMF_ID'].append(smfId)
            abacusPool = fetchReferenceSamplePoolFromAbacus(smfId, abacusConn)
            assert abacusPool is None or abacusPool == sampleSheetPool, "Pool listed in sample sheet (" + sampleSheetPool + ") differs from pool listed in Abacus (" + abacusPool + ")"
            referenceSamples['CELL_LINE'].append(abacusPool + ' Plasmid')
            referenceSamples['POOL_NAME'].append(sampleSheetPool)
            referenceSamples['COMPOUND'].append('None')
            referenceSamples['ALIGNED_READS'].append(alignedReadCount)
            referenceSamples['TOTAL_READS'].append(totalReadCount)
            referenceSamples['ALIGNMENT_PERCENT'].append(alignedReadCount/totalReadCount * 100)
            referenceSamples['COMMENT'].append('')
        else:
            nonReferenceSamples['#'].append(sampleNum)
            sampleNum += 1
            nonReferenceSamples['FCID'].append(fcid)
            nonReferenceSamples['LANES'].append(lanes)
            nonReferenceSamples['INDEX'].append(index)
            barcode = fetchBarcodeFromAbacus(smfId, abacusConn, runDate, warningsFile)
            if barcode is None:
                warn("No barcode information found for SMF id " + smfId, warningsFile)
            nonReferenceSamples['2D_BARCODE'].append(barcode)
            nonReferenceSamples['SAMPLE_SMF_ID'].append(smfId)
            cellLine, abacusPool, compound = fetchCellLineInfoFromAbacus(smfId, abacusConn)
            if cellLine is None:
                warn("No cell line information found for SMF id " + smfId, warningsFile)
            nonReferenceSamples['CELL_LINE'].append(cellLine)
            assert abacusPool is None or abacusPool == sampleSheetPool, "Pool listed in sample sheet (" + sampleSheetPool + ") differs from pool listed in Abacus (" + abacusPool + ")"
            nonReferenceSamples['POOL_NAME'].append(sampleSheetPool)
            if compound is not None:
                compound = compound.replace(' | ', ',')
            nonReferenceSamples['COMPOUND'].append(compound)
            nonReferenceSamples['ALIGNED_READS'].append(alignedReadCount)
            nonReferenceSamples['TOTAL_READS'].append(totalReadCount)
            nonReferenceSamples['ALIGNMENT_PERCENT'].append(alignedReadCount/totalReadCount * 100)
            nonReferenceSamples['COMMENT'].append('')
    referenceSamples = pd.DataFrame(referenceSamples)
    nonReferenceSamples = pd.DataFrame(nonReferenceSamples)
    df = referenceSamples.append(nonReferenceSamples)
    df['ALIGNED_READS'] = df['ALIGNED_READS'].astype(int)
    df['TOTAL_READS'] = df['TOTAL_READS'].astype(int)
    df['COMPOUND'] = df['COMPOUND'].fillna('None')
    df['2D_BARCODE'] = df['2D_BARCODE'].fillna('Unknown')
    df['CELL_LINE'] = df['CELL_LINE'].fillna('Unknown')
    return df

def extractDate(sampleSheet):
    runDir = os.path.basename(os.path.dirname(sampleSheet))
    m = re.match(r'(\d{2})(\d{2})(\d{2})_.+', runDir)
    assert m is not None, "Could not parse date from directory " + runDir
    return '20' + m.group(1) + '-' + m.group(2) + '-' + m.group(3)

def main(argv):
    defaultAbacusUrl = 'jdbc:oracle:thin:@ldap://tns.na.novartis.net:389/abacus_usca_prod,cn=oraclecontext,dc=research,dc=pharma'
    defaultAbacusUser = 'abacus'
    defaultWarningsFilename = 'warnings.txt'
    parser = ArgumentParser()
    parser.add_argument('-s', '--sample_smf_ids', dest='sample_smf_ids', help="Comma-delimited list of sample SMF ids to include", type=str, required=True)
    parser.add_argument('-c', '--counts_files', dest='counts_files', help="Comma-delimited list of matched read count files", type=str, required=True)
    parser.add_argument('-u', '--unmatched_files', dest='unmatched_files', help="Comma-delimited list of unmatched sequence files", type=str, required=True)
    parser.add_argument('-o', '--output', dest='output', help="Output file", type=str, required=True)
    parser.add_argument('--sample_sheet', dest='sample_sheet', help="Filepath to sample sheet", type=str, required=True)
    parser.add_argument('-n', '--num_pools', dest='num_pools', help='Number of pools in this run', type=int, required=True)
    parser.add_argument('--abacus_url', dest='abacus_url', help="JDBC string to connect to ABACUS", type=str, required=False,
                        default=defaultAbacusUrl)
    parser.add_argument('--abacus_user', dest='abacus_user', help="Username for ABACUS (default = " + defaultAbacusUser + ")", type=str, required=False, default=defaultAbacusUser)
    parser.add_argument('-p', '--abacus_pw', dest='abacus_pw', help="Password for ABACUS", type=str, required=True)
    parser.add_argument('--oracle_thin_driver_jar_path', dest='oracle_thin_driver_jar_path', help="Path to Oracle Thin JDBC Driver jar (not required)", type=str, required=False,
                        default='/usr/prog/onc/seqcount/1.0.0/ojdbc7.jar')
    parser.add_argument('-w', '--warnings_file', dest='warnings_file', help="File for warnings in case of missing ABACUS data (default = " + defaultWarningsFilename + ")", type=str, required=False, default="")
    parser.add_argument('-D', '--run_date', dest='run_date', help='Date of experiment run (YYYY-MM-DD) (extracted from sample sheet directory by default', type=str, required=False, default=None)
    args = parser.parse_args(argv)
    sampleSmfIds = args.sample_smf_ids.split(',')
    countsFiles = args.counts_files.split(',')
    unmatchedFiles = args.unmatched_files.split(',')
    numPools = args.num_pools
    numCountsFiles = len(countsFiles)
    numUnmatchedFiles = len(unmatchedFiles)
    numSamples = len(sampleSmfIds)
    if numSamples != numCountsFiles or numSamples != numUnmatchedFiles:
        raise ValueError("Different numbers of samples supplied: " + numSamples + " SMF ids, " + numCountsFiles + " counts files, " + numUnmatchedFiles + " unmatched files")
    files = tuple(zip(sampleSmfIds, zip(countsFiles, unmatchedFiles)))
    output = args.output
    sampleSheet = args.sample_sheet
    abacusUrl = args.abacus_url
    abacusUser = args.abacus_user
    abacusPassword = args.abacus_pw
    oracleThinDriverJarPath = args.oracle_thin_driver_jar_path
    warningsFile = os.path.join(os.path.dirname(sampleSheet), args.warnings_file if args.warnings_file != "" else defaultWarningsFilename)
    runDate = args.run_date if args.run_date is not None else extractDate(sampleSheet)
    runDate = datetime.strptime(runDate, '%Y-%m-%d')

    # conn = jaydebeapi.connect('oracle.jdbc.driver.OracleDriver', [abacusUrl, abacusUser, abacusPassword], oracleThinDriverJarPath)
    conn = odb.connect(abacusUser + '/' + abacusPassword + '@abacus_usca_prod')
    # curs = conn.cursor()

    df = buildDataFrame(files, sampleSheet=sampleSheet, numPools=numPools, abacusConn=conn, warningsFile=warningsFile, runDate = runDate)
    df.to_csv(output, sep = "\t", float_format = '%.2f', index = False, na_rep = 'N/A', columns = ['#', 'FCID', 'LANES', 'INDEX', '2D_BARCODE', 'SAMPLE_SMF_ID', 'CELL_LINE', 'POOL_NAME', 'COMPOUND', 'ALIGNED_READS', 'TOTAL_READS', 'ALIGNMENT_PERCENT', 'COMMENT'])

    # curs.close()
    conn.close()

if __name__ == "__main__":
    main(sys.argv[1:])