#!/usr/bin/env bash
#$ -S /bin/bash
#$ -N @JOB_NAME@
#$ -wd @JOB_DIRECTORY@
#$ -j y
#$ -t 1-@NUM_SAMPLES@
#$ -l h_rt=@MAX_RUNTIME@
#$ -binding linear:1
#$ -hold_jid @PARENT_JOB@
@EMAIL_NOTIFICATIONS_CONFIG@
@EMAIL_ADDRESS_CONFIG@

VERBOSE=@VERBOSE@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "util_functions.sh not found" >&1 >&2
    exit 100
fi
source $RUN_DIRECTORY/util_functions.sh

MERGING_SCRIPT=$JOB_DIRECTORY/merge_output_files.py

NUM_SEQUENCE_CHUNKS=@NUM_SEQUENCE_CHUNKS@
RUN_JOB=@RUN_JOB@
if [ ! -z "$RUN_JOB" ]; then
    RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
    TASK_CHECK=`checkSuccessfulTasks $RUN_LOG_FILE $RUN_JOB $NUM_SEQUENCE_CHUNKS`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$TASK_CHECK" >&1 >&2
        exit 100
    fi
fi

SAMPLES=( `getSamples $RUN_DIRECTORY` )
NUM_SAMPLES=${#SAMPLES[@]}
if [ "$NUM_SAMPLES" -ne "@NUM_SAMPLES@" ]; then
    echo "Number of samples provided for parallelization (@NUM_SAMPLES@) does not match number found in sample sheet ($NUM_SAMPLES)" >&1 >&2
    exit 100
fi
SAMPLE_NUM=$SGE_TASK_ID
SAMPLE_INFO=( `echo ${SAMPLES[$((SAMPLE_NUM-1))]} | tr "," "\n"` )
SAMPLE_NAME=${SAMPLE_INFO[0]}
SAMPLE_PROJECT=${SAMPLE_INFO[2]}

SAMPLE_OUTPUT_DIR=$RUN_DIRECTORY/Aligned/Seeded/$SAMPLE_PROJECT/$SAMPLE_NAME
MATCHED_FILES=$SAMPLE_OUTPUT_DIR/matched.txt.*
MERGED_SEEDED_MATCHED_FILE=$SAMPLE_OUTPUT_DIR/matched_new.txt
UNMATCHED_FILES=$SAMPLE_OUTPUT_DIR/unmatched.txt.*
MERGED_UNMATCHED_FILE=$SAMPLE_OUTPUT_DIR/unmatched.txt

PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python

echo "Merging matched files" >&2
CMD1="$PYTHON -u $MERGING_SCRIPT $MATCHED_FILES -o $MERGED_SEEDED_MATCHED_FILE"
if [ "$VERBOSE" -eq "1" ]; then
    echo $CMD1 >&2
fi
$CMD1
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "Error in merging matched sequence files" >&1 >&2
    while [ -e $LOCKFILE ]; do
        sleep 1
    done
    lockfile $JOB_DIRECTORY/cc.lock
    echo -e "$JOB_ID\t$SGE_TASK_ID\t$JOB_NAME\t$CHUNK_NUM\t`date +\"%F_%T\"`\tFAIL" >> $RUN_LOG_FILE
    rm -f $JOB_DIRECTORY/cc.lock
    exit 100
fi
rm $MATCHED_FILES

echo "Merging unmatched files" >&2
CMD2="$PYTHON -u $MERGING_SCRIPT $UNMATCHED_FILES -o $MERGED_UNMATCHED_FILE -u"
if [ "$VERBOSE" -eq "1" ]; then
    echo $CMD2 >&2
fi
$CMD2
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "Error in merging unmatched sequence files"
    while [ -e $LOCKFILE ]; do
        sleep 1
    done
    lockfile $JOB_DIRECTORY/cc.lock
    echo -e "$JOB_ID\t$SGE_TASK_ID\t$JOB_NAME\t$CHUNK_NUM\t`date +\"%F_%T\"`\tFAIL" >> $RUN_LOG_FILE
    rm -f $JOB_DIRECTORY/cc.lock
    exit 100
fi
rm $UNMATCHED_FILES

echo "Merging seeded match counts with exact match counts" >&2
EXACT_MATCH_FILE=$RUN_DIRECTORY/Aligned/Exact/$SAMPLE_PROJECT/$SAMPLE_NAME/matched.txt
FULLY_MERGED_MATCHED_FILE=$SAMPLE_OUTPUT_DIR/matched.txt
CMD3="$PYTHON -u $MERGING_SCRIPT $EXACT_MATCH_FILE $MERGED_SEEDED_MATCHED_FILE -o $FULLY_MERGED_MATCHED_FILE"
if [ "$VERBOSE" -eq "1" ]; then
    echo $CMD3 >&2
fi
$CMD3
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "Error in merging seeded + exact match counts" >&1 >&2
    while [ -e $LOCKFILE ]; do
        sleep 1
    done
    lockfile $JOB_DIRECTORY/cc.lock
    echo -e "$JOB_ID\t$SGE_TASK_ID\t$JOB_NAME\t$CHUNK_NUM\t`date +\"%F_%T\"`\tFAIL" >> $RUN_LOG_FILE
    rm -f $JOB_DIRECTORY/cc.lock
    exit 100
fi

echo "Rounding down fractional counts and generating counts file for upload" >&2
TEMP_FILE_1=$SAMPLE_OUTPUT_DIR/temp1.txt
TEMP_FILE_2=$SAMPLE_OUTPUT_DIR/temp2.txt
TEMP_FILE_3=$SAMPLE_OUTPUT_DIR/temp3.txt
cut -f1,2 $FULLY_MERGED_MATCHED_FILE > $TEMP_FILE_1
for i in `cut -f3 $FULLY_MERGED_MATCHED_FILE`; do printf "%.0f\n" $i; done > $TEMP_FILE_2
paste $TEMP_FILE_1 $TEMP_FILE_2 > $TEMP_FILE_3
rm $TEMP_FILE_1
rm $TEMP_FILE_2
INDEX=`getIndex $RUN_DIRECTORY $SAMPLE_NAME`
FLOWCELL_ID=`getFlowcellId $RUN_DIRECTORY`
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "Error retrieving flowcell id"
    echo $FLOWCELL_ID
    exit 100
fi
#COUNTS_FILE=$SAMPLE_OUTPUT_DIR/counts.csv
COUNTS_FILE="$SAMPLE_OUTPUT_DIR/${SAMPLE_NAME}_${INDEX}_${SAMPLE_PROJECT}_FCID${FLOWCELL_ID}_count.csv"
cut -f1,3 --output-delimiter=',' $TEMP_FILE_3 | sort -k1,1 > $COUNTS_FILE
rm $TEMP_FILE_3

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
LOCKFILE=$JOB_DIRECTORY/cc.lock
while [ -e $LOCKFILE ]; do
    sleep 1
done
lockfile $LOCKFILE
echo -e "$JOB_ID\t$SGE_TASK_ID\t$JOB_NAME\t$CHUNK_NUM\t`date +\"%F_%T\"`\tSUCCESS" >> $RUN_LOG_FILE
rm -f $LOCKFILE