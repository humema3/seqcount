#!/usr/bin/env bash
#$ -S /bin/bash
#$ -N @JOB_NAME@
#$ -wd @JOB_DIRECTORY@
#$ -j y
#$ -l h_rt=@MAX_RUNTIME@
#$ -binding linear:1
#$ -hold_jid @PARENT_JOB@
@EMAIL_NOTIFICATIONS_CONFIG@
@EMAIL_ADDRESS_CONFIG@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "util_functions.sh not found" >&1 >&2
    exit 1
fi
source $RUN_DIRECTORY/util_functions.sh

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
LOCKFILE=$JOB_DIRECTORY/cc.lock
if [ ! -f $RUN_LOG_FILE ]; then
    echo -e "JOB_ID\tTASK_ID\tJOB_NAME\tCHUNK_NUMBER\tDATE\tSTATUS" > $RUN_LOG_FILE
fi

NUM_SEQUENCE_CHUNKS=@NUM_SEQUENCE_CHUNKS@
PARENT_JOB=@PARENT_JOB@
if [ ! -z "$PARENT_JOB" ]; then
    NUM_LOG_FILES=`ls $RUN_LOG_FILE.$PARENT_JOB.* | wc -l`
    if [ "`grep -c \"$PARENT_JOB\" $RUN_LOG_FILE`" -eq "0" ]; then
        if [ "$NUM_LOG_FILES" -ne "$NUM_SEQUENCE_CHUNKS" ]; then
            echo "Logging only completed for $NUM_LOG_FILES out of $NUM_SEQUENCE_CHUNKS tasks" >&2
            exit 1
        fi
        for i in `seq 1 $NUM_SEQUENCE_CHUNKS`; do
            cat $RUN_LOG_FILE.$PARENT_JOB.$i >> $RUN_LOG_FILE
        done
        rm $RUN_LOG_FILE.$PARENT_JOB.*
    fi
    TASK_CHECK=`checkSuccessfulTasks $RUN_LOG_FILE $PARENT_JOB $NUM_SEQUENCE_CHUNKS`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$TASK_CHECK" >&1 >&2
        exit $EXIT_STATUS
    fi
    rm $JOB_DIRECTORY/SEQ_COUNT_SEED_BASED_MATCH.o$PARENT_JOB.*
fi