__author__ = 'humema3'
import math
import sys
from argparse import ArgumentParser
from datetime import datetime

def hamdist_str(s1, s2, max_dist):
    # if len(s1) != len(s2):
    #    raise ValueError("Unequal lengths: " + s1 + ", " + s2)
    d = 0
    for i in range(len(s1)):
        if s1[i:i+1] != s2[i:i+1]:
            d += 1
            if d > max_dist:
                return None
    return d

def end_indel_dist(s1, s2, max_dist):
    for i in range(1, max_dist):
        if s1[i:] == s2[:-1*i] or s1[:-1*i] == s2[i:]:
            return i
    return None

def seqDist(s1, s2, maxDist, useEndIndels):
    if len(s1) != len(s2):
       raise ValueError("Unequal lengths: " + s1 + ", " + s2)
    hamDist = hamdist_str(s1, s2, maxDist)
    if not useEndIndels:
        return hamDist
    indelDist = end_indel_dist(s1, s2, maxDist)
    if hamDist is None and indelDist is None:
        return None
    elif hamDist is None:
        return indelDist
    elif indelDist is None:
        return hamDist
    else:
        return min(hamDist, indelDist)

def getClosestSeeds(seeds, sequence, maxDist, useEndIndels):
        minDist = math.inf
        closestSeeds = []
        for seed in seeds:
            #dist = hamdist_str(seed, sequence, maxDist)
            dist = seqDist(seed, sequence, maxDist, useEndIndels)
            if dist is not None and dist < maxDist:
                if dist < minDist:
                    minDist = dist
                    closestSeeds = [seed]
                elif dist == minDist:
                    closestSeeds.append(seed)
        return closestSeeds

def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def logMessage(message, toStdOut = False):
    logMessage = currentTime() + "\t\t" + message
    if toStdOut:
        print(logMessage)
    else:
        sys.stderr.write(logMessage + "\n")

def main(argv):
    parser = ArgumentParser()
    parser.add_argument('-s', '--seed_sequence_file', dest='seed_sequence_file', required=True, type=str)
    parser.add_argument('-i', '--initial_unmatched_sequence_file', dest='initial_unmatched_sequence_file', required=True, type=str)
    parser.add_argument('-c', '--counts_file', dest='counts_file', required=False, default="counts.txt", type=str)
    parser.add_argument('-f', '--final_unmatched_sequence_file', dest='final_unmatched_sequence_file', required=False, default="unmatched.txt", type=str)
    parser.add_argument('-m', '--max_hamming_distance', dest='maximum_hamming_distance', required=False, default=3, type=int)
    parser.add_argument('-e', '--max_end_indel_distance', dest='maximum_end_indel_distance', required=False, default=None, type=int)
    parser.add_argument('-w', '--without_end_indels', default=False, dest='without_end_indels', action='store_true')
    args = parser.parse_args(argv)
    seedSequenceFile = args.seed_sequence_file
    initialUnmatchedSequenceFile = args.initial_unmatched_sequence_file
    countsFile = args.counts_file
    finalUnmatchedSequenceFile = args.final_unmatched_sequence_file
    maxHammingDistance = args.maximum_hamming_distance
    withoutEndIndels = args.without_end_indels
    maxEndIndelDistance = args.maximum_end_indel_distance
    if maxEndIndelDistance is None:
        maxEndIndelDistance = maxHammingDistance

    logMessage("Gathering seed sequences")
    seeds = {}
    s = open(seedSequenceFile, 'r')
    for line in iter(s):
        smfId, sequence, count = line.strip().split()
        count = int(count)
        seeds[sequence] = (smfId, count)
    s.close()
    seedSequences = seeds.keys()

    logMessage("Trying to match unmatched sequences")
    unmatched = []
    matchedSequences = {}
    i = open(initialUnmatchedSequenceFile, 'r')
    for line in iter(i):
        sequence, count = line.strip().split()
        count = int(count)
        seedMatches = getClosestSeeds(seedSequences, sequence, maxHammingDistance, not withoutEndIndels)
        if len(seedMatches) == 0:
            unmatched.append((sequence, count))
        else:
            numMatches = len(seedMatches)
            for match in seedMatches:
                if match in matchedSequences:
                    matchedSequences[match][1] += count/numMatches
                else:
                    matchedSequences[match] = [seeds[match][0], count/numMatches]
    i.close()

    logMessage("Writing new counts to " + countsFile)
    output = sorted(matchedSequences.items(), key = lambda x: (-1*x[1][1], x[1][0])) # sort by decreasing count, secondarily by increasing SMF id
    c = open(countsFile, 'w')
    for sequence, (smfId, count) in output:
        count = float("%.3f" % count) # round decimal places
        if count == int(count):
            count = int(count) # get rid of trailing .0
        c.write(smfId + "\t" + sequence + "\t" + str(count) + "\n")
    c.close()

    logMessage("Writing final unmatched sequences to " + finalUnmatchedSequenceFile)
    f = open(finalUnmatchedSequenceFile, 'w')
    for sequence, count in unmatched:
        f.write(sequence + "\t" + str(int(count)) + "\n")
    f.close()

if __name__ == "__main__":
    main(sys.argv[1:])