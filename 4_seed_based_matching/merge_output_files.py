import sys
from argparse import ArgumentParser
from glob import glob

def buildKnownSeqDict(file, seqDict):
    f = open(file, 'r')
    for line in f:
        smfId, seq, count = line.strip().split()
        count = float(count)
        if seq in seqDict:
            assert seqDict[seq][0] == smfId, "Sequence " + seq + " mapped to different SMF ids: " + smfId + ", " + seqDict[seq][0]
            seqDict[seq][1] += count
        else:
            seqDict[seq] = [smfId, count]
    f.close()


def buildUnknownSeqDict(file, seqDict):
    f = open(file, 'r')
    for line in f:
        seq, count = line.strip().split()
        count = float(count)
        if seq in seqDict:
            seqDict[seq] += count
        else:
            seqDict[seq] = count
    f.close()

def buildSeqDict(file, seqDict, hasSmfId):
    if hasSmfId:
        buildKnownSeqDict(file, seqDict)
    else:
        buildUnknownSeqDict(file, seqDict)


def printKnownSequences(seqDict, outputFile):
    sequences = sorted(seqDict.items(), key = lambda x: (-1*x[1][1], x[1][0]))
    o = open(outputFile, 'w')
    for seq, (smfId, count) in sequences:
        count = float("%.3f" % count)
        if count == int(count):
            count = int(count)
        o.write(smfId + "\t" + seq + "\t" + str(count) + "\n")
    o.close()


def printUnknownSequences(seqDict, outputFile):
    sequences = sorted(seqDict.items(), key = lambda x: (-1*x[1], x[0]))
    o = open(outputFile, 'w')
    for seq, count in sequences:
        count = float("%.3f" % count)
        if count == int(count):
            count = int(count)
        o.write(seq + "\t" + str(count) + "\n")
    o.close()

def printSequences(seqDict, outputFile, hasSmfId):
    if hasSmfId:
        printKnownSequences(seqDict, outputFile)
    else:
        printUnknownSequences(seqDict, outputFile)


def main(argv):
    parser = ArgumentParser()
    parser.add_argument('files', nargs='+', type=str)
    parser.add_argument('-o', '--output', dest='output', type=str, required=True)
    parser.add_argument('-u', '--no_smf_id', dest='no_smf_id', default=False, action='store_true')
    args = parser.parse_args(argv)
    files = args.files
    if len(files) == 1 and files[0].endswith('*'):
        files = glob(files[0])
    output = args.output
    hasSmfId = not args.no_smf_id
    seqDict = {}
    for file in files:
        buildSeqDict(file, seqDict, hasSmfId)
    printSequences(seqDict, output, hasSmfId)

if __name__ == "__main__":
    main(sys.argv[1:])
