#!/usr/bin/env bash

STEP_NUMBER=4
ARGS="$@"

SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_steps.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)

assignPipelineFlags -c -a -p -S -C -j SEQ_COUNT_SEED_BASED_MATCH -m 14399 -J `basename $0`
assignArgs $ARGS
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ] || [ "$HELP" -eq "1" ]; then
    exit $EXIT_STATUS
fi
checkArgs
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit 100
fi

SAMPLES=( `getSamples $RUN_DIRECTORY` )
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "Step $STEP_NUMBER failed: Error extracting samples from SampleSheet.csv"
    exit $EXIT_STATUS
fi
NUM_SAMPLES=${#SAMPLES[@]}

# exact matching needs to finish before number of tasks can be determined
if [ ! -z "$PARENT_JOB" ]; then
    echo "Waiting for cluster job $PARENT_JOB to finish..."
    if [ ! -z "$LOG_FILE" ]; then # don't duplicate to stderr
        logMessage "Waiting for cluster job $PARENT_JOB to finish..."
    fi
    waitForClusterJob $PARENT_JOB
fi

NUM_SEQUENCE_CHUNKS=`getNumSequenceChunks $RUN_DIRECTORY $SEQUENCE_CHUNK_SIZE`
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "$NUM_SEQUENCE_CHUNKS"
    exit $EXIT_STATUS
fi

JOB_DIRECTORY=$RUN_DIRECTORY/`basename $SCRIPT_DIR`
mkdir -p $JOB_DIRECTORY
RUN_SCRIPT_TEMPLATE=$SCRIPT_DIR/run_seed_based_matching_template.sh
RUN_SCRIPT=$JOB_DIRECTORY/run_seed_based_matching.sh
CONCATENATE_LOGS_SCRIPT_TEMPLATE=$SCRIPT_DIR/concatenate_logs_template.sh
CONCATENATE_LOGS_SCRIPT=$JOB_DIRECTORY/concatenate_logs.sh
FINISH_SCRIPT_TEMPLATE=$SCRIPT_DIR/finish_seed_based_matching_template.sh
FINISH_SCRIPT=$JOB_DIRECTORY/finish_seed_based_matching.sh

# escape special characters for regex substitution
FORMATTED_JOB_NAME=${JOB_NAME//\//\\\/}
FORMATTED_JOB_NAME=${FORMATTED_JOB_NAME//@/\\@}
PARENT_JOB_CONFIG=${PARENT_JOB_CONFIG//\//\\\/}
PARENT_JOB_CONFIG=${PARENT_JOB_CONFIG//@/\\@}
FORMATTED_JOB_DIRECTORY=${JOB_DIRECTORY//\//\\\/}
EMAIL_ADDRESS_CONFIG=${EMAIL_ADDRESS_CONFIG//@/\\@}
PARENT_JOB_CONFIG=${PARENT_JOB_CONFIG//\$/\\\$}

executeCmd "cp `dirname $SCRIPT_DIR`/util_functions.sh -t $RUN_DIRECTORY"
executeCmd "cp `dirname $SCRIPT_DIR`/pipeline_steps.sh -t $RUN_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/seed_based_matching.py -t $JOB_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/merge_output_files.py -t $JOB_DIRECTORY"

executeCmd "cp $RUN_SCRIPT_TEMPLATE $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/$FORMATTED_JOB_NAME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_SEQUENCE_CHUNKS\@/$NUM_SEQUENCE_CHUNKS/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@SEQUENCE_CHUNK_SIZE\@/$SEQUENCE_CHUNK_SIZE/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_HAMMING_DISTANCE\@/$MAX_HAMMING_DISTANCE/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_END_INDEL_DISTANCE\@/$MAX_END_INDEL_DISTANCE/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_CONCURRENT_TASKS\@/$MAX_NUM_CONCURRENT_TASKS/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@NO_INDELS\@/$NO_INDELS/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@STEP_NUMBER\@/$STEP_NUMBER/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@PARENT_JOB\@/$PARENT_JOB/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $RUN_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;" $RUN_SCRIPT

executeCmd "cp $CONCATENATE_LOGS_SCRIPT_TEMPLATE $CONCATENATE_LOGS_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/concatenate_logs_$FORMATTED_JOB_NAME/g; $CONCATENATE_LOGS_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $CONCATENATE_LOGS_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $CONCATENATE_LOGS_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_SEQUENCE_CHUNKS\@/$NUM_SEQUENCE_CHUNKS/g; $CONCATENATE_LOGS_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $CONCATENATE_LOGS_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $CONCATENATE_LOGS_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $CONCATENATE_LOGS_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $CONCATENATE_LOGS_SCRIPT

executeCmd "cp $FINISH_SCRIPT_TEMPLATE $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/finish_${FORMATTED_JOB_NAME}/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@EMAIL\@/$EMAIL/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_SAMPLES\@/$NUM_SAMPLES/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_SEQUENCE_CHUNKS\@/$NUM_SEQUENCE_CHUNKS/g; $FINISH_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $FINISH_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $FINISH_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $FINISH_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $FINISH_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $FINISH_SCRIPT

RUN_CMD="qsub $RUN_SCRIPT"
CONCATENATE_LOGS_CMD="qsub $CONCATENATE_LOGS_SCRIPT"
FINISH_CMD="qsub $FINISH_SCRIPT"

function runCurrentStep {
    logMessage "$RUN_CMD"
    local RUN_RESPONSE=`submitScriptAsClusterJob $RUN_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$RUN_RESPONSE"
    fi
    local RUN_JOB_ID=`extractClusterJobIdFromSubmissionResponse "$RUN_RESPONSE"`
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$RUN_JOB_ID"
        exit $EXIT_STATUS
    fi
    executeCmd "perl -i -pe s/\@PARENT_JOB\@/$RUN_JOB_ID/g; $CONCATENATE_LOGS_SCRIPT" 0
    logMessage "$CONCATENATE_LOGS_CMD"
    local CONCATENATE_LOGS_RESPONSE=`submitScriptAsClusterJob $CONCATENATE_LOGS_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$CONCATENATE_LOGS_RESPONSE"
    fi
    local CONCATENATE_LOGS_JOB_ID=`extractClusterJobIdFromSubmissionResponse "$CONCATENATE_LOGS_RESPONSE"`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$CONCATENATE_LOGS_JOB_ID"
        exit $EXIT_STATUS
    fi
    executeCmd "perl -i -pe s/\@PARENT_JOB\@/$CONCATENATE_LOGS_JOB_ID/g; $FINISH_SCRIPT" 0
    executeCmd "perl -i -pe s/\@RUN_JOB\@/$RUN_JOB_ID/g; $FINISH_SCRIPT" 0
    logMessage "$FINISH_CMD"
    local FINISH_RESPONSE=`submitScriptAsClusterJob $FINISH_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$FINISH_RESPONSE"
    fi
    local FINISH_JOB_ID=`extractClusterJobIdFromSubmissionResponse "$FINISH_RESPONSE"`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$FINISH_JOB_ID"
        exit $EXIT_STATUS
    fi
    local NEXT_STEP_NUMBER=$((STEP_NUMBER+1))
    local NEXT_STEP=${PIPELINE_STEPS[$((NEXT_STEP_NUMBER-1))]}
    local NEXT_CMD="`dirname $SCRIPT_DIR`/${NEXT_STEP_NUMBER}_${NEXT_STEP}/${NEXT_STEP}.sh -r $RUN_DIRECTORY"
    if [ "$AUTO_CASCADE" -eq "1" ]; then
        NEXT_CMD+=" -R" # last step: just add autorun flag
    fi
    NEXT_CMD+=" -U $ABACUS_USERNAME"
    NEXT_CMD+=" -o $REPORT_FILEPATH"
    NEXT_CMD+=" -P $FINISH_JOB_ID"
    if [ ! -z "$LOG_FILE" ]; then
        NEXT_CMD+=" -L $LOG_FILE"
    fi
    if [ "$VERBOSE" -eq "1" ]; then
        NEXT_CMD+=" -v"
    fi

    if [ -z "$ABACUS_PASSWORD" ]; then
        if [ "$AUTO_CASCADE" -eq "1" ]; then
            echoAndLogMessage "Abacus password (-p) is required for report creation step; terminating after current step"
            exit 0
        fi
    else
        NEXT_CMD+=" -p $ABACUS_PASSWORD"
    fi

    if [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
        executeCmd "$NEXT_CMD" 0
    else
        echo
        echo "Next step: $NEXT_STEP. Run? (y/n)"
        read ANSWER
        if [[ "$ANSWER" =~ ^[Yy] ]]; then
            executeCmd "$NEXT_CMD" 0
        else
            if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
                echo "Response not understood - not proceeding with next step"
            fi
            echo "To run: $NEXT_CMD"
        fi
    fi
}
if [ "$AUTORUN" -eq "1" ] || [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
    runCurrentStep
else
    echo "Submit job now? (y/n)"
    read ANSWER
    if [[ "$ANSWER" =~ ^[Yy] ]]; then
        runCurrentStep
    else
        echoAndLogMessage "To submit: 1) $RUN_CMD\n2) perl -i -pe \"s/\\@PARENT_JOB\\@/<job_id_from_step_1>/g;\" $CONCATENATE_LOGS_SCRIPT\n3) $CONCATENATE_LOGS_CMD\n4) perl -i -pe \"s/\\@PARENT_JOB\\@/<job_id_from_step_3>/g;\" $FINISH_SCRIPT\n5) $FINISH_CMD"
    fi
fi
