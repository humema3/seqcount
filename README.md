# README #

## What is this repository for? ##

SeqCount, NGT's new counting pipeline for pool-based sequencing experiments

Version 1.0.0

## Who can I talk to? ##

For any questions or issues, contact Maxwell Hume at [maxwell.hume@novartis.com](mailto:maxwell.hume@novartis.com).

## How do I get set up? ##

The code is located at `/usr/prog/onc/seqcount/1.0.0`. To load the dependencies and add the directory to your path, run the following (or include it in your .bashrc file):
```
module use /usr/prog/onc/modules
module purge
module load seqcount/1.0.0
```
Source code can also be cloned directly from the [Bitbucket repository](https://bitbucket.org/humema3/seqcount), but hopefully you have no reason to do this.

## How do I run? ##

### Automated run of whole pipeline ###

The basic structure of the command to run the pipeline is:

`[my521@mymachine ~]$ run_seqcount.sh [options] myAbacusPassword my_run_directory`

You can omit the `[options]` part altogether to use all the default options. The full list of options is included below, but some sensible ones to consider including are to remove email notifications from HPC cluster-based jobs if you don't want to flood your inbox (you can use the `qstat` command to check on your jobs), and to include the step to trim the reads in the FASTQ files, to 20 bp (the standard CRISPR guide length) by default.

`[my521@mymachine ~]$ run_seqcount.sh --no_email --trim_reads myAbacusPassword my_run_directory`

To run in the background and persist through any server disconnect:

`[my521@mymachine ~]$ nohup run_seqcount.sh [options] myAbacusPassword my_run_directory > output_log.txt &`

This will append essential progress messages, errors, warnings, etc., to `output_log.txt` or whatever file you specify. 

The Abacus password is paired with a username that you can supply as an option, but which by default is the read-only "abacus" account.

`run_seqcount.sh` should be in your PATH after loading the modulefile so you can run it from any directory.



### Running individual steps ###

The pipeline is made of the following sequential steps:

1. Demultiplex: runs bcl2fastq2 to convert the .bcl files into .fastq.gz files
2. Trim reads (optional): discards all but the first `N` bases of each read
3. Exact matching: first matching step. Matches the reads to the pool's FASTA library file exactly.
4. Seed-based matching: second matching step. Matches the remaining unmatched reads to the library file approximately using distance-based metrics.
5. Create report: Prepares a report for distribution containing the read alignment percentages for each sample and other data.
6. Abacus upload: Uploads the count results into the Abacus database.

Except for the final step (Abacus upload), all of the pipeline steps can be queued in a sequential fashion all at once. Checks are in place to ensure that each step must finish before the next one begins. Thus, all of the option flags throughout the whole set of steps are unique, and can be supplied all at once for the first step, or for any downstream steps if run from an intermediate step. The steps can also be queued and run individually. The Abacus upload step is kept separate so that the user can ensure that everything has gone as planned before committing to an upload.

From within the code base (`/usr/prog/onc/seqcount/1.0.0`), each step has its respective scripts located in a directory labeled as <step_number>_<step_name>, e.g., `1_demultiplex`. To run the step, the user runs the script <step_name> located in this directory.

```
[my521@mymachine ~] cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 1_demultiplex/demultiplex.sh [options] myAbacusPassword my_run_directory
```

See below for further details on usage, arguments, and options.

Each step has two stages:

1. Job prep: copy the script template into the run directory and fill in the appropriate values
2. Job submission: run the script and/or submit as cluster job

By default, a prompt will appear to confirm that the user wants to proceed with the job submission after the prep is completed. 

```
[humema3@nrusca-ldl30118 ~]$ cd /usr/prog/onc/seqcount/1.0.0
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh --verbose /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:53             Copying util_functions.sh to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:54             Copying pipeline_steps.sh to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:54             Writing to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
Submit job now? (y/n)
```

After submission, prompt will then appear to ask if the user wants to prep the next step. 
```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
Submit job now? (y/n)
y
2016-12-06 17:55:57            qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
Next step: trim_reads. Run? (y/n)

```

The user can proceed down the entire pipeline this way. If the user declines to continue at any point, the command for the next stage or step will be printed out and the user can run it at his/her convenience.
```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
Submit job now? (y/n)
n
To run: qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
```

To make it easier, some options are included to remove these prompts and automatically proceed with job submission or the next step of the pipeline:

* `-R|--autorun`: automatically submit current step
```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh --verbose -R /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:53             Copying util_functions.sh to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:54             Copying pipeline_steps.sh to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX
2016-12-06 17:55:54             Writing to /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
2016-12-06 17:55:57             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
2012-12-06 17:56:00             Job was modified before it was accepted. No Memory requirements has been specified. As a result, a default 4GB value has been assigned. See http://web.global.nibr.novartis.net/apps/confluence/display/scicomp/SciComp+Cluster+Scheduling+Policies for more information. Your job 8809815 ("SEQ_COUNT_DEMULTIPLEX") has been submitted
Next step: trim_reads. Run? (y/n)
n
To run: /usr/prog/onc/seqcount/1.0.0/2_trim_reads/trim_reads.sh -r /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX -B 20 -n -b 10 -a 30 -H 3 -I 3 -s 3000 -c 1000 -P 8845986 -U abacus -o /clscratch/humema3/NGT_COUNTING_TEST_DATA/test_160824_D00237_0379_AC9MV2ANXX/final_report.txt -v -p *********
```

* `-N|--run_next_step`: automatically submit current step and prep next step (still requires confirmation to submit the next step)
```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh 1_demultiplex/demultiplex.sh -N /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX
2016-12-11 10:56:11             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
2016-12-11 10:56:14             /usr/prog/onc/seqcount/1.0.0/2_trim_reads/trim_reads.sh -r /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX -B 20 -n -b 10 -a 30 -H 3 -I 3 -s 3000 -c 1000 -P 8845981 -U abacus -o /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/final_report.txt -v -p ********* 
Submit job now? (y/n)
```

* `-A|--auto_cascade`: continue down the whole pipeline without any prompts
```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh -A /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX
2016-12-11 11:11:30             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/1_demultiplex/run_demultiplex.sh
2016-12-11 11:11:33             /usr/prog/onc/seqcount/1.0.0/2_trim_reads/trim_reads.sh -r /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX -B 20 -n -A -b 10 -a 30 -H 3 -I 3 -s 3000 -c 1000 -P 8846360 -U abacus -o /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/final_report.txt -v -p ********
2016-12-11 11:11:33             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/2_trim_reads/run_trim_reads.sh
2016-12-11 11:11:36             /usr/prog/onc/seqcount/1.0.0/CRISPR_Count/3_exact_matching/exact_matching.sh -r /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX -n -A -b 10 -a 30 -H 3 -I 3 -s 3000 -c 1000 -U abacus -o /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/final_report.txt -P 8846361 -t -p ******** -v
2016-12-11 11:11:37             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/3_exact_matching/start_exact_matching.sh
2016-12-11 11:11:43             qsub /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/3_exact_matching/run_exact_matching.sh
2016-12-11 11:11:43             /usr/prog/onc/seqcount/1.0.0/4_seed_based_matching/seed_based_matching.sh -r /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX -n -A -H 3 -I 3 -s 3000 -c 1000 -P 8846363 -U abacus -o /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX/final_report.txt -p ******** -v
Waiting for cluster job 8846363 to finish...
```

As illustrated here, steps 4 and 5 require full completion of the previous step before they can even be properly prepped, so if queued in this fashion (or more generally if passed a parent job argument with `-P`), they will display a message indicating that they are waiting for the appropriate job to finish.

Note that the `run_seqcount.sh` script is equivalent to `1_demultiplex.sh -A`.

#### Abacus upload ####

As mentioned previously, the final step to upload the count results into Abacus is kept separate from the rest of the pipeline chain, so that the user can perform any necessary checks before submitting the counts. Once submitted any changes would have to go through NX and will likely be somewhat cumbersome. 

Running the Abacus upload step is exactly like running any other individual step.

```
[humema3@nrusca-ldl30118 1.0.0]$ 6_abacus_upload/abacus_upload.sh --help

############# ABACUS UPLOAD OPTIONS #############
usage: abacus_upload.sh [-D] run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -D, --upload_to_abacus_test
                        Upload to Abacus test database instead of prod
```

You can pass in the `-D` or `--upload_to_abacus_test` flag to upload to the Abacus test database if you want to have a look at the results within the Abacus environment prior to the actual upload.

Note: Although the Abacus password is required for the "create report" step, which fetches data directly from the Abacus prod database, a password is not required for this step (regardless of whether the upload is to the prod or test database). This is because the scripts used here utilize the Abacus web services.

### Input: Run directory and sample sheet ###

A top-level directory for the sequencing run is required for all steps. The run directory should be compliant with the Illumina tool bcl2fastq2, as outlined [here](http://support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq2-v2-17-software-guide-15051736-g.pdf) in the section "BCL Conversion Input Files". Tthe output from HiSeq and MiSeq experiments should generally fit this requirement. One thing to keep in mind is that a `SampleSheet.csv` file should be present in the top-level directory; in some cases this may require copying from the `Data/Intensities/BaseCalls` subdirectory.

The SampleSheet format should match what is displayed in the [bcl2fastq2 guide](http://support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq2-v2-17-software-guide-15051736-g.pdf) in the "Sample Sheet" section. Here is the top of a "bare-bones" sample sheet which meets the absolute minimum requirements:

```
[Header],,,,,
,,,,,
[Reads],,,,,
,,,,,
[Settings],,,,,
,,,,,
[Data],,,,,
Lane,Sample_Name,Sample_Id,index,Sample_Project,FCID
1,TA-67-TF92,5,TGCTGAGCAT,EPICPool1,C9MV2ANXX
1,KA-83-EO90,6,ATGCAGATCG,EPICPool3,C9MV2ANXX
1,QE-78-DD95,7,GTCAGCTATC,EPICPool3,C9MV2ANXX
1,QC-99-LR91,8,ACGTAGCTGA,EPICPool2,C9MV2ANXX
1,PA-67-TJ92,9,CATGCGTACT,EPICPool2,C9MV2ANXX
1,ME-78-DH95,10,TATCGACGTG,EPICPool3,C9MV2ANXX
1,YE-77-DV95,11,GTCGTGATAC,EPICPool2,C9MV2ANXX
1,MC-99-LV91,12,AGTACTGCGA,EPICPool3,C9MV2ANXX
1,NB-65-II90,13,ATCAGACGCT,EPICPool3,C9MV2ANXX
1,UE-77-DZ95,14,AGACATGCTG,EPICPool2,C9MV2ANXX
1,IE-78-DL95,15,CGATGAGTAC,EPICPool2,C9MV2ANXX
1,IC-99-LZ91,16,TAGCTGCAGA,EPICPool3,C9MV2ANXX
1,TA-37-TF92,17,CGAGTACAGT,EPICPool1,C9MV2ANXX
1,MD-79-OL97,18,ATAGCTGTCG,EPICPool1,C9MV2ANXX
1,AB-91-WL94,19,TACGCATGAC,EPICPool1,C9MV2ANXX
# ...
```

The sheet has four sections: Header, Reads, Settings, Data. The first two sections are not utilized, i.e., are for record keeping only; however, such records are important and the user is encouraged to maintain them accurately. The Settings section, while technically optional since defaults are provided, is utilized by the bcl2fastq2 tool, so make sure to update that as necessary. See the [bcl2fastq2 guide](http://support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq2-v2-17-software-guide-15051736-g.pdf) for more details on these sections.

The only strictly required section of the SampleSheet is the final Data section. This comprises a header line with a series of column names, followed by a number of corresponding comma-delimited rows. The order of the columns doesn't matter as long as everything is lined up consistently. The following columns must be present (in this exact formatting, case-sensitive): 

* Lane: Contains the flow cell lane number
* Sample_Name: Contains the sample's SMF id
* Sample_Id: Contains a numeric identifier mapping one-to-one with the SMF id, from 1 to the number of different samples
* index: Contains the index sequence for the sample
* Sample_Project: Contains the name of the pool from which the sample is derived
* FCID: Contains the flowcell id

Other columns can be included, but are optional and are not utilized. The order of the data rows doesn't matter. However, the one other requirement for this section is: **if there are `n` different pools being utilized in a run, the first `n` Sample_Id numbers (i.e., 1 to `n`) must be reserved for the reference samples for each pool.**

### Supplying arguments ###

To see the full usage statement and list of arguments available beginning at your current step, call the script with the `-h` or `--help` flag.

The run directory must always be specified on the command line (unless `-h` is supplied). It can be supplied on its own as the final argument, or else earlier with a preceding `-r` or `--run_directory` flag. 

Other arguments and options can be specified via the command line, or else can be stored in a config file located in the run directory; the config filename is then supplied on the command line with a preceding `-C` or `--config_file` flag. (If you call your file `pipeline_config.txt`, the default name, then you don't need to specify it on the command line.) Values can be supplied in the config file and on command line simultaneously ("mix and matched"), with command line supplied values taking precedence over those in the config file. Each line of the config file is of the format `arg=val`, where `arg` is the long form of the argument flag, as displayed by the usage, in all uppercase (e.g., `BASE_QUALITY_CUTOFF`, `ABACUS_USERNAME`, `NO_EMAIL`, `RUN_NEXT_STEP`, etc.), and `val` is either:

* For arguments that take values: the value of the argument you want to use (e.g., `BASE_QUALITY_CUTOFF=10`, `ABACUS_USERNAME=humema3`)
* For arguments that are simply "as is": `1` (e.g., `NO_EMAIL=1`, `RUN_NEXT_STEP=1`). Note that all such arguments are 0 by default.

Do not include extra spaces between the `=` and its operands. Comments at the end of a line beginning with `#` are allowed. 

For the report creation step, or any queue that includes it, an Abacus password is required, and must be supplied on its own at the end of the command directly prior to the run directory, or else earlier with a preceding `-p` or `--abacus_password` flag, or in the config file. Including it as a part of your standard config file for every run is recommended.

### Logging ###

There are two parameters which control the logging process:

* `-L|--log_file <LOG_FILE>` will take all non-essential messages and redirect them to a file, e.g., script run commands. This is recommended if you don't care about the actual commands being run; otherwise they will come out on the console. Furthermore, using a file reduces the risk of losing the logged information. You can provide a full filepath, or just a filename, which will be saved in the run directory.

```
[humema3@nrusca-ldl30118 1.0.0]$ 1_demultiplex/demultiplex.sh -L log.txt /clscratch/humema3/NGT_COUNTING_TEST_DATA/test2_160824_D00237_0379_AC9MV2ANXX
Submit job now? (y/n)
y
Next step: trim_reads. Run? (y/n)
y
Submit job now? (y/n)
y
Next step: exact_matching. Run? (y/n)
y
Submit job now? (y/n)
y
Next step: seed_based_matching. Run? (y/n)
y
Waiting for cluster job 8848632 to finish...
```

* `-v|--verbose` will include some even less important messages which by default are excluded altogether, e.g., notifications for copying files, cluster job submission responses. See some of the above examples for verbose output.

### Full usage ###

This is the usage output from running `1_demultiplex/demultiplex.sh -h`. As this is the first step, it has the options for all of the downstream steps (except Abacus upload) built in, so it is the most illustrative. 

```
############# GENERIC PIPELINE OPTIONS #############
usage: demultiplex.sh [-C CONFIG_FILE] [-R] [-v] [-L LOG_FILE] run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -C CONFIG_FILE, --config_file CONFIG_FILE
                        Config filename found in run_directory to define
                        default options (use format arg=val, where arg is the
                        long form of the argument in all caps; for no-argument
                        flags, use val = 1, or 0 to explicitly not set the
                        flag)
  -R, --autorun         Submit the current step after preparation, without
                        prompting
  -v, --verbose         Turn on verbose logging output
  -L LOG_FILE, --log_file LOG_FILE
                        Write stderr to this log file instead of the console
                        (supply full path or else file is placed in run
                        directory)

############# DEMULTIPLEXING SPECIFIC OPTIONS #############
usage: demultiplex.sh [-O OPTS_STRING] run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -O OPTS_STRING, --opts_string OPTS_STRING
                        String of optional arguments to supply to bcl2fastq(2)
                        (empty by default; i.e., uses application defaults)

############# CLUSTER JOB OPTIONS #############
usage: demultiplex.sh [-j JOB_NAME] [-m MAX_RUNTIME] [-e EMAIL] [-n]
                      run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -j JOB_NAME, --job_name JOB_NAME
                        Name of cluster job (default = SEQ_COUNT_DEMULTIPLEX)
  -m MAX_RUNTIME, --max_runtime MAX_RUNTIME
                        Max runtime in seconds for submitted cluster jobs
                        (default = 14399)
  -e EMAIL, --email EMAIL
                        Email address for notifications on cluster jobs
                        (defaults to email for current user account unless -n
                        is supplied)
  -n, --no_email        No email notifications

############# CASCADING OPTIONS (FOR NEXT STEPS) ##############
usage: demultiplex.sh [-N] [-A] [-t] run_directory

positional arguments:
  run_directory        bcl2fastq2-compliant top level directory for the
                       sequencing run (can optionally supply with -r flag)

optional arguments:
  -N, --run_next_step  Submit the current step and prep the next one
                       automatically without prompting (can only use default
                       job name and max runtime; email notification settings
                       will be retained from this script if supplied)
  -A, --auto_cascade   Auto-cascade (keep running the next step without
                       prompting for all downstream steps; see caveats for -N)
  -t, --trim_reads     Trim reads after exact matching (only utilized when
                       running demultiplexing or exact matching

##### OPTIONS FOR READ TRIMMING: #####
usage: demultiplex.sh [-B NUM_BASES_TO_KEEP] run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -B NUM_BASES_TO_KEEP, --num_bases_to_keep NUM_BASES_TO_KEEP
                        Number of bases to keep after trimming (make sure it's
                        no more than the number of bases there currently,
                        default = 20)

##### OPTIONS FOR EXACT MATCHING: #####
usage: demultiplex.sh [-b BASE_QUALITY_CUTOFF] [-a AVERAGE_QUALITY_CUTOFF]
                      [-u]
                      run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -b BASE_QUALITY_CUTOFF, --base_quality_cutoff BASE_QUALITY_CUTOFF
                        Minimum individual base quality cutoff for read
                        filtering (default = 10)
  -a AVERAGE_QUALITY_CUTOFF, --average_quality_cutoff AVERAGE_QUALITY_CUTOFF
                        Minimum average base quality cutoff for read filtering
                        (default = 30
  -u, --unzip           Unzip fastq.gz files before reading and keep the
                        unzipped fastq files

##### OPTIONS FOR SEED-BASED MATCHING: #####
usage: demultiplex.sh [-H MAX_HAMMING_DISTANCE] [-I MAX_END_INDEL_DISTANCE]
                      [-i] [-s SEQUENCE_CHUNK_SIZE]
                      [-c MAX_NUM_CONCURRENT_TASKS]
                      run_directory

positional arguments:
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -H MAX_HAMMING_DISTANCE, --max_hamming_distance MAX_HAMMING_DISTANCE
                        Max Hamming distance for sequence matching (default =
                        3)
  -I MAX_END_INDEL_DISTANCE, --max_end_indel_distance MAX_END_INDEL_DISTANCE
                        Max 'end indel' based distance for sequence matching
                        (by default, match -H argument)
  -i, --no_indels       Don't use indels to calculate closest sequences
                        (mismatches only)
  -s SEQUENCE_CHUNK_SIZE, --sequence_chunk_size SEQUENCE_CHUNK_SIZE
                        Number of sequences per chunk per sample for
                        parallelization (default = 3000)
  -c MAX_NUM_CONCURRENT_TASKS, --max_num_concurrent_tasks MAX_NUM_CONCURRENT_TASKS
                        Number of tasks that can run in parallel on the
                        cluster (default = 1000)

##### OPTIONS FOR REPORT CREATION: #####
usage: demultiplex.sh [-o REPORT_FILENAME] [-U ABACUS_USERNAME]
                      abacus_password run_directory

positional arguments:
  abacus_password       Password to access Abacus database with supplied
                        username (see -U) (can optionally supply with -p flag)
  run_directory         bcl2fastq2-compliant top level directory for the
                        sequencing run (can optionally supply with -r flag)

optional arguments:
  -o REPORT_FILENAME, --report_filename REPORT_FILENAME
                        Output filename for report (ends up in run_directory,
                        default = final_report.txt)
  -U ABACUS_USERNAME, --abacus_username ABACUS_USERNAME
                        Username to access Abacus database (default = abacus)
```

### More usage examples ###

The module is assumed to have been loaded in all of these examples.

#### Queue all the steps (except for Abacus upload) at once using all default property values ####

##### Using config file #####

```
[my521@mymachine ~]$ cat my_run_directory/config.txt
AUTO_CASCADE=1
ABACUS_PASSWORD=myAbacusPassword
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 1_demultiplex/demultiplex.sh -C config.txt my_run_directory
```

##### On command line #####

```
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 1_demultiplex/demultiplex.sh -A myAbacusPassword my_run_directory
```

OR
```
[my521@mymachine ~]$ nohup run_seqcount.sh myAbacusPassword my_run_directory &
```

#### Queue all the steps (except for Abacus upload) at once, and change some of the default values ####

##### Using config file #####

```
[my521@mymachine ~]$ cat my_run_directory/pipeline_config.txt
AUTO_CASCADE=1
TRIM_READS=1
ABACUS_PASSWORD=myAbacusPassword
NO_EMAIL=1 # don't want email notifications for any cluster jobs
NUM_BASES_TO_KEEP=25 # our sequences are >= 25 bp and we want to keep more than the 20-bp default
UNZIP=1 # read from and keep unzipped FASTQ files during exact matching
MAX_NUM_CONCURRENT_TASKS=1500 # increase the efficiency of the parallelization for seed-based matching (if SciComp will be ok with us running this many tasks at once)
MAX_END_INDEL_DISTANCE=2 # we don't want to be as tolerant of indel-based count calls as of mismatch-based count calls
REPORT_FILENAME=summary.txt # change the name of the final report just to be contrary
LOG_FILE=log.txt
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 1_demultiplex/demultiplex.sh my_run_directory
```

##### On command line #####

```
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 1_demultiplex/demultiplex.sh -A -t -n -B 25 -u -c 1500 -I 2 -o summary.txt -p myAbacusPassword my_run_directory
```

(Note: you can "mix and match" between config file and command line, with command line values taking precedence)

#### Prep the exact matching step to begin the queue ####

```
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 3_exact_matching/exact_matching.sh my_run_directory
```

#### Prep the exact matching step, waiting for cluster job 123456 to finish ####

```
[my521@mymachine ~]$ cd /usr/prog/onc/seqcount/1.0.0
[my521@mymachine 1.0.0]$ 3_exact_matching/exact_matching.sh -P 123456 my_run_directory
```