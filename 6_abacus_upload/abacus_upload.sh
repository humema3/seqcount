#!/usr/bin/env bash

STEP_NUMBER=6
ARGS="$@"

SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)

assignPipelineFlags -a -p -C -U -J `basename $0`
assignArgs $ARGS
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ] || [ "$HELP" -eq "1" ]; then
    exit $EXIT_STATUS
fi
checkArgs
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit $EXIT_STATUS
fi

# check that all previous tasks succeeded
#if [ ! -z "$PARENT_JOB" ]; then
#    SAMPLES=( `getSamples $RUN_DIRECTORY` )
#    NUM_SAMPLES=${#SAMPLES[@]}
#    PREV_STEP_NUMBER=$((STEP_NUMBER-1))
#    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
#    TASK_CHECK=`checkSuccessfulTasks $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB $NUM_SAMPLES`
#    EXIT_STATUS=$?
#    if [ "$EXIT_STATUS" -ne "0" ]; then
#        echoAndLogMessage "$TASK_CHECK"
#        exit $EXIT_STATUS
#    fi
#fi

JOB_DIRECTORY=$RUN_DIRECTORY/`basename $SCRIPT_DIR`
mkdir -p $JOB_DIRECTORY
RUN_SCRIPT_TEMPLATE=$SCRIPT_DIR/run_abacus_upload_template.sh
RUN_SCRIPT=$JOB_DIRECTORY/run_abacus_upload.sh

# escape special characters for regex substitution
FORMATTED_JOB_DIRECTORY=${JOB_DIRECTORY//\//\\\/}

executeCmd "cp `dirname $SCRIPT_DIR`/util_functions.sh -t $RUN_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/abacus-upload.pl -t $JOB_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/abacus-upload-dev.pl -t $JOB_DIRECTORY"

executeCmd "cp $RUN_SCRIPT_TEMPLATE $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@PARENT_JOB\@/$PARENT_JOB/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@UPLOAD_TO_ABACUS_TEST\@/$UPLOAD_TO_ABACUS_TEST/g; $RUN_SCRIPT"

RUN_CMD="bash $RUN_SCRIPT"

function runCurrentStep {
    logMessage "$RUN_CMD"
    $RUN_CMD
}

if [ "$AUTORUN" -eq "1" ] || [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
    runCurrentStep
else
    echo "Run now? (y/n)"
    read ANSWER
    if [[ "$ANSWER" =~ ^[Yy] ]]; then
        runCurrentStep
    else
        if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
            echo "Response not understood - not running"
        fi
        echo "To run: $RUN_CMD"
    fi
fi