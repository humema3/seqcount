#!/usr/bin/perl
use warnings;
use strict;
use Socket;
use File::Basename;

sub fgrep_count {
    my ($file, $expression) = @_;
    my $fh;
    unless(open($fh, '<', $file)) {
        die "Could not open $file for reading: $!";
    }
    my $count = 0;
    while (my $line = <$fh>) {
        if ($line =~ /$expression/) {
            $count++;
        }
    }
    close($fh);
    return $count;
}

my $uri  = "/services/abacus/PoolSeqService.svc/result_upload";
my $host = "nrusca-sd139.nibr.novartis.net";
my $port = 88;

my $upfile = shift || 'sample.csv';

# If a reference file is provided, then concatenate the contents to the
# end of the sample file 
my $reffile = shift || '';
if ($reffile ne '' && fgrep_count($reffile, "# REF_COUNTS") == 0) {
  `cat $upfile > upfile.tmp`;
  `echo '# REF_COUNTS' >>upfile.tmp`;
  `cat $reffile>>upfile.tmp`;
  `mv upfile.tmp $upfile`;
}

$| = 1;

my $start = times;
my ( $iaddr, $paddr, $proto );
$iaddr = inet_aton($host);

$paddr = sockaddr_in( $port, $iaddr );
$proto = getprotobyname('tcp');
unless ( socket( SOCK, PF_INET, SOCK_STREAM, $proto ) ) {
    die "ERROR : init socket: $!";
}
unless ( connect( SOCK, $paddr ) ) { die "no connect: $!\n"; }

my $length = 0;

open (UH,"< $upfile") or warn "$!\n";
$length += -s UH;
my $nibr521 = (getpwuid($<))[0];
my @head = (
    "POST $uri HTTP/1.1",
    "Host: $host",
    "User-Agent: drive-uploader",
    "Content-Length: $length",
    "filename: " . basename($upfile),
		"NIBR521: $nibr521",
    "",
    "",
);
my $header = join( "\r\n", @head );
print $header;

select SOCK;
$| = 1;
binmode SOCK;

print SOCK $header;

while( sysread(UH, my $buf, 8196 ) ) {

    if( length($buf) < 8196 ) {
        syswrite SOCK, $buf, length($buf);
    } else { 
        syswrite SOCK, $buf, 8196 
    }
}
close UH;

shutdown SOCK, 1;

my @data = (<SOCK>);
print STDOUT "result->\n@data\n";
close SOCK;

__END__

