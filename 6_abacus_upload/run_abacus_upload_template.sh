#!/usr/bin/env bash

JOB_DIRECTORY=@JOB_DIRECTORY@
SCRIPT_DIR=$(dirname `readlink -f $0`)
if [ "$SCRIPT_DIR" != "$JOB_DIRECTORY" ]; then
    echo "Error in script location: is $SCRIPT_DIR, should be $JOB_DIRECTORY" >&2
    exit 100
fi
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "$RUN_DIRECTORY/util_functions.sh not found" >&2
    exit 100
fi
source $RUN_DIRECTORY/util_functions.sh

UPLOAD_TO_ABACUS_TEST=@UPLOAD_TO_ABACUS_TEST@
if [ "$UPLOAD_TO_ABACUS_TEST" -eq "1" ]; then
    UPLOAD_SCRIPT=$JOB_DIRECTORY/abacus-upload-dev.pl
else
    UPLOAD_SCRIPT=$JOB_DIRECTORY/abacus-upload.pl
fi

VERBOSE=@VERBOSE@
NUM_PROJECTS=`getNumProjects $RUN_DIRECTORY`
SAMPLES=( `getSamples $RUN_DIRECTORY` )
NUM_SAMPLES=${#SAMPLES[@]}

REF_SAMPLES=( `getReferenceSamples $RUN_DIRECTORY` )
checkExitStatus $? "Error retrieving reference samples"
#declare -A REF_SAMPLES
#for i in `seq 1 $NUM_PROJECTS`; do
#    REF_SAMPLE=( `echo ${SAMPLES[$((i-1))]} | tr "," "\n"` )
#    REF_SAMPLE_NAME=${REF_SAMPLE[1]}
#    REF_SAMPLE_PROJECT=${REF_SAMPLE[2]}
#    REF_SAMPLES[$REF_SAMPLE_PROJECT]=$REF_SAMPLE_NAME
#done

RESULTS_DIR=$RUN_DIRECTORY/Aligned/Seeded
FLOWCELL_ID=`getFlowcellId $RUN_DIRECTORY`
for i in `seq $((NUM_PROJECTS+1)) $NUM_SAMPLES`; do
    SAMPLE_INFO=( `echo ${SAMPLES[$((i-1))]} | tr "," "\n"` )
    SAMPLE_NAME=${SAMPLE_INFO[0]}
    SAMPLE_PROJECT=${SAMPLE_INFO[2]}
    for REF_SAMPLE in ${REF_SAMPLES[@]}; do
        REF_SAMPLE=( `echo $REF_SAMPLE | tr "," "\n"` )
        REF_SAMPLE_NAME=${REF_SAMPLE[0]}
        REF_SAMPLE_PROJECT=${REF_SAMPLE[2]}
        if [ "$REF_SAMPLE_PROJECT" == "$SAMPLE_PROJECT" ]; then
            MATCHED_REF_SAMPLE_NAME=$REF_SAMPLE_NAME
            break
        fi
    done
    INDEX=`getIndex $RUN_DIRECTORY $SAMPLE_NAME`
    REF_INDEX=`getIndex $RUN_DIRECTORY $MATCHED_REF_SAMPLE_NAME`
    COUNTS_FILE=$RESULTS_DIR/$SAMPLE_PROJECT/$SAMPLE_NAME/${SAMPLE_NAME}_${INDEX}_${SAMPLE_PROJECT}_FCID${FLOWCELL_ID}_count.csv
#    COUNTS_FILE=$RESULTS_DIR/$SAMPLE_PROJECT/$SAMPLE_NAME/counts.csv
    REF_COUNTS_FILE=$RESULTS_DIR/$SAMPLE_PROJECT/$MATCHED_REF_SAMPLE_NAME/${MATCHED_REF_SAMPLE_NAME}_${REF_INDEX}_${SAMPLE_PROJECT}_FCID${FLOWCELL_ID}_count.csv
    CMD="perl $UPLOAD_SCRIPT $COUNTS_FILE $REF_COUNTS_FILE"
    echo $CMD >&2
    $CMD
    EXIT_STATUS=$?
    echo
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "Bad exit from $UPLOAD_SCRIPT for sample $COUNTS_FILE (reference sample $REF_COUNTS_FILE)" >&2
        break
    fi
done

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
if [ ! -f $RUN_LOG_FILE ]; then
    echo -e "DATE\tSTATUS" > $RUN_LOG_FILE
fi
echo -n -e "`date +\"%F_%T\"`\t" >> $RUN_LOG_FILE
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "FAIL" >> $RUN_LOG_FILE
    exit 100
else
    echo "SUCCESS" >> $RUN_LOG_FILE
    exit 0
fi
