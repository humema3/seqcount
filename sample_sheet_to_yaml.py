import cx_Oracle as odb
import os
import pandas as pd
import sys
import tablib
from argparse import ArgumentParser

def fetchReferenceSampleMapping(conn):
    query = "SELECT CONCEPT, SAMPLE_SMF_ID FROM ABACUS.AB_POOL_MV"
    return pd.read_sql(query, conn)

def write_yaml(csv, yaml_file, **kwargs):
    sample_sheet = tablib.Dataset().load(open(csv, 'r').read())
    run_dir = os.path.dirname(csv)
    header = sample_sheet.headers
    data_dict = sample_sheet.dict
    # for entry in data_dict:
    #     samples.add(entry['Sample_Id'])
    # num_samples = len(samples)

    reference_samples = tablib.Dataset()
    reference_samples.headers = ['POOL', 'REFERENCE_SAMPLE']
    abacus_conn = odb.connect('abacus/ps4prd2abc@abacus_usca_prod')
    ref_sample_mapping_df = fetchReferenceSampleMapping(abacus_conn)
    pools = set()
    for entry in data_dict:
        pool = entry['Sample_Project']
        if pool not in pools:
            pools.add(pool)
            ref_sample = list(ref_sample_mapping_df[ref_sample_mapping_df.CONCEPT == pool].SAMPLE_SMF_ID)
            assert len(ref_sample) == 1, "More than one reference sample derived for pool " + pool + ": " + str(ref_sample)
            ref_sample = ref_sample[0]
            #print(ref_sample)
            reference_samples.append([pool, ref_sample])
    abacus_conn.close()
 
    samples = tablib.Dataset()
    samples.headers = [col for col in header if col != 'Lane']
    for entry in data_dict:
        row = []
        for k in header:
            if k != 'Lane':
                row.append(entry[k])
        samples.append(row)
    samples.remove_duplicates()
    samples = samples.sort('Sample_Id')

    fastq = tablib.Dataset()
    base_dir = os.path.join(run_dir, 'Data', 'Intensities', 'BaseCalls')
    for i in range(len(data_dict)):
        entry = data_dict[i]
        files = []
        for lane in range(1,9):
            filepath = os.path.join(base_dir, entry['Sample_Project'], entry['Sample_Id'], entry['Sample_Name'] + '_S_' + str(i) + '_L00' + str(lane) + '_R1_001.fastq.gz')
            files.append(filepath)
        fastq.append(files)

    # exact_matching = tablib.Dataset()
    # base_dir = os.path.join(run_dir, 'Aligned', 'Exact')
    # for entry in data_dict:
    #     output_dir = os.path.join(base_dir, entry['Sample_Project'], entry['Sample_Id'])
    #     exact_matching.extend(output_dir)

    # seed_based_matching = tablib.Dataset()
    # base_dir = os.path.join(run_dir, 'Aligned', 'Seeded')
    # seed_based_matching

    sequence_chunk_size = kwargs['sequence_chunk_size']
    num_chunks = tablib.Dataset()

    unmatched_exact_files = tablib.Dataset()
    base_dir = os.path.join(run_dir, 'Aligned', 'Exact')
    for entry in data_dict:
        unmatched_exact_files.append(os.path.join(base_dir, entry['Sample_Project']))

    yaml = "SAMPLES:\n    " + samples.yaml.replace("\n-", "\n    -")
    yaml += "REFERENCE_SAMPLES:\n    " + reference_samples.yaml.replace("\n-", "\n    -")
    yaml += "FASTQ:\n    " + fastq.yaml.replace("\n-", "\n    -")
    # yaml += "EXACT_MATCHING:\n    " + exact_matching.yaml.replace("\n-", "\n    -")
    with open(yaml_file, 'w') as f:
        f.write(yaml)

def remove_first_n_lines(num_lines_to_skip, input, output):
    o = open(output, 'w')
    with open(input, 'r') as f:
        for i in range(num_lines_to_skip):
            f.readline()
        for line in f.readlines():
            o.write(line)
    o.close()

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("input", type=str)
    parser.add_argument("-o", "--output", type=str, default = None)
    parser.add_argument("-n", "--skip_lines", help = "Number of lines at the beginning of the SampleSheet file to skip (before the data header line)", required=False, type = int, default = 0)
    parser.add_argument("-s", "--sequence_chunk_size", help = "Number of sequences to run together during parallelized seed-based matching (default = 3000)", required = False, type = int, default = 3000)
    args = parser.parse_args(sys.argv[1:])
    input = args.input
    assert input.endswith('.csv'), "Input file does not have a .csv extension"
    assert os.path.isfile(input), input + " not found on the file system"
    if args.output is not None:
        output = args.output
    else:
        output = os.path.join(os.path.dirname(input), os.path.splitext(os.path.basename(input))[0] + ".yaml")
    num_lines_to_skip = args.skip_lines
    sequence_chunk_size = args.sequence_chunk_size

    if num_lines_to_skip > 0:
        temp_file = 'temp.csv'
        remove_first_n_lines(num_lines_to_skip, input, temp_file)
        write_yaml(temp_file, output, {'sequence_chunk_size': sequence_chunk_size})
        os.remove(temp_file)
    else:
        write_yaml(input, output, sequence_chunk_size)
