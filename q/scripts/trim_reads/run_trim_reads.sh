#!/bin/bash
#q require $JOB_NAME $JOB_DIRECTORY $NUM_SAMPLES $MAX_RUNTIME $MAX_MEM
#$ -S /bin/bash
#$ -N $JOB_NAME
#$ -wd $JOB_DIRECTORY
#$ -j y
#$ -t 1-$NUM_SAMPLES
#$ -l h_rt=$MAX_RUNTIME
#$ -l m_mem_free=$MAX_MEM
#$ -binding linear:1
@PARENT_JOB_CONFIG@
@EMAIL_NOTIFICATIONS_CONFIG@
@EMAIL_ADDRESS_CONFIG@

VERBOSE=@VERBOSE@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "$RUN_DIRECTORY/util_functions.sh not found" >&1 >&2
    exit 100
fi
source $RUN_DIRECTORY/util_functions.sh
if [ ! -f $RUN_DIRECTORY/pipeline_steps.sh ]; then
    echo "$RUN_DIRECTORY/pipeline_steps.sh not found" >&1 >&2
    exit 100
fi
source $RUN_DIRECTORY/pipeline_steps.sh

STEP_NUMBER=@STEP_NUMBER@
PARENT_JOB=@PARENT_JOB@
if [ ! -z "$PARENT_JOB" ]; then
    PREV_STEP_NUMBER=$((STEP_NUMBER-1))
    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
    LAST_TASK=`checkLastTask $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$LAST_TASK"
        exit 100
    fi
fi

SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
FASTX_TRIMMER=/usr/prog/onc/fastx_toolkit/bin/fastx_trimmer
NUM_BASES_TO_KEEP=@NUM_BASES_TO_KEEP@

SAMPLES=( `getSamples $RUN_DIRECTORY` )
if [ "${#SAMPLES[@]}" -ne "@NUM_SAMPLES@" ]; then
    echoAndLogMessage "Number of samples provided for parallelization (@NUM_SAMPLES@) does not match number found in sample sheet (${#SAMPLES[@]})"
    exit 100
fi
SAMPLE_NUM=$SGE_TASK_ID
SAMPLE_INFO=( `echo ${SAMPLES[$((SAMPLE_NUM-1))]} | tr "," "\n"` )
SAMPLE_ID=${SAMPLE_INFO[1]}
SAMPLE_NAME=${SAMPLE_INFO[0]}
SAMPLE_PROJECT=${SAMPLE_INFO[2]}

OUTPUT_DIR=$RUN_DIRECTORY/Data/Intensities/BaseCalls/$SAMPLE_PROJECT/$SAMPLE_ID
ORIGINAL_FILES=$OUTPUT_DIR/$SAMPLE_NAME*.fastq.gz
UNTRIMMED_DIR=$RUN_DIRECTORY/Data/Intensities/BaseCalls/untrimmed/$SAMPLE_PROJECT/$SAMPLE_ID
mkdir -p $UNTRIMMED_DIR
if [ "$VERBOSE" -eq "1" ]; then
    echo "Moving files from $OUTPUT_DIR to $UNTRIMMED_DIR: `date +\"%F %T\"`" >&2
fi
mv $ORIGINAL_FILES -t $UNTRIMMED_DIR
if [ "$VERBOSE" -eq "1" ]; then
    echo "Unzipping files: `date +\"%F %T\"`" >&2
fi
gunzip $UNTRIMMED_DIR/*.gz

if [ "$VERBOSE" -eq "1" ]; then
    echo "Trimming files, compressing, moving back to $OUTPUT_DIR" >&2
fi
for FILE in $UNTRIMMED_DIR/*.fastq; do
    OUTPUT_FILE=$OUTPUT_DIR/`basename $FILE`.gz
    echo "Trimming $FILE: `date +\"%F %T\"`" >&2
    CMD="$FASTX_TRIMMER -Q 33 -l $NUM_BASES_TO_KEEP -z -i $FILE -o $OUTPUT_FILE"
    if [ "$VERBOSE" -eq "1" ]; then
        echo "$CMD" >&2
    fi
    $CMD
    if [ "$VERBOSE" -eq "1" ]; then
        echo "Testing $OUTPUT_FILE: `date +\"%F %T\"`" >&2
    fi
    L=`expr length $OUTPUT_FILE`
    UNZIPPED_OUTPUT_FILE=${OUTPUT_FILE:0:$((L-3))}
    zcat $OUTPUT_FILE > $UNZIPPED_OUTPUT_FILE
    EXIT_STATUS=$?
    rm $UNZIPPED_OUTPUT_FILE
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$OUTPUT_FILE was corrupted during trimming" >&1 >&2
        break
    fi
    if [ "$VERBOSE" -eq "1" ]; then
        echo "Recompressing $FILE: `date +\"%F %T\"`" >&2
    fi
    gzip $FILE
done

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
LOCKFILE=$JOB_DIRECTORY/cc.lock
while [ -e $LOCKFILE ]; do
    sleep 1
done
lockfile $LOCKFILE
if [ ! -f $RUN_LOG_FILE ]; then
    echo -e "JOB_ID\tTASK_ID\tJOB_NAME\tSAMPLE_NUMBER\tDATE\tSTATUS" > $RUN_LOG_FILE
fi
echo -n -e "$JOB_ID\t$SGE_TASK_ID\t$JOB_NAME\t$SAMPLE_NUM\t`date +\"%F_%T\"`\t" >> $RUN_LOG_FILE
if [ "$EXIT_STATUS" -ne "0" ]; then
    echo "FAIL" >> $RUN_LOG_FILE
else
    echo "SUCCESS" >> $RUN_LOG_FILE
fi
rm -f $LOCKFILE
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit 100
else
    exit 0
fi