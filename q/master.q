$RUN_DIRECTORY run pwd
# $EMAIL_ADDRESS <email> # defaults to email of the Unix account owner, uncomment and fill in if you want someone else to receive the emails
$EMAIL_NOTIFICATIONS_CONFIG n # by default, do not receive notification emails from the cluster - to receive notifications, change "n" to "beas"
$VERBOSE 0
$TRIM_READS 1
$SCRIPT_BASE /usr/prog/onc/seqcount/1.0.0/q/scripts

invoke slaves/demultiplex.q
invoke slaves/trim_reads.q
invoke slaves/exact_matching.q
invoke slaves/seed_based_matching.q
invoke slaves/create_report.q