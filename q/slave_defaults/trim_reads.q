$TIMESTAMP run date +"%Y_%m_%d"
$STEP_NUM 2
$JOB_DIRECTORY $RUN_DIRECTORY/${STEP_NUM}_trim_reads/$TIMESTAMP
mkdir -p $JOB_DIRECTORY
JOB_NAME SC.2_trim_reads
$MAX_RUNTIME 21600
$MAX_MEM 32G
