import sys
from argparse import ArgumentParser
from datetime import datetime

def currentTime():
    return datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')

def badQuality(qualLine, phredFormat = 33, baseCutoff = 15, averageCutoff = 25):
    totalScore = 0
    for charIndex in range(len(qualLine)):
        phredScore = ord(qualLine[charIndex]) - phredFormat
        if phredScore < baseCutoff:
            return True
        totalScore += phredScore

    if totalScore/len(qualLine) < averageCutoff:
        return True

    return False

def processReadSequence(sequence, quality, output):
    if badQuality(quality):
        return
    output.write(sequence + "\n")

def processReadFiles(readFiles, outputFile):
    o = open(outputFile, 'w')
    for readFile in readFiles:
        lineNum = 0
        sequence = ""
        quality = ""
        i = open(readFile, 'r')
        for line in i:
            if lineNum % 4 == 1:
                if lineNum % 4000000 == 1:
                    print("Processing sequence # " + str( int((lineNum - 1) / 4 + 1) ) + " in file " + readFile + ": " + currentTime())
                if lineNum > 1:
                    processReadSequence(sequence, quality, o)
                sequence = line.strip()
            elif lineNum %4 == 3:
               quality = line.strip()
            lineNum += 1
        if lineNum > 3:
            processReadSequence(sequence, quality, o)
        i.close()
    o.close()

def main(args):
    parser = ArgumentParser()
    parser.add_argument('-readFiles', required=True)
    parser.add_argument('-outputFile', required=True)
    args = parser.parse_args(args)
    readFiles = args.readFiles.split(",")
    outputFile = args.outputFile
    processReadFiles(readFiles, outputFile)

if __name__ == "__main__":
    main(sys.argv[1:])