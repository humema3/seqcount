#!/usr/bin/env bash

declare -A POOL_NAME_TO_REF_SAMPLE
PYTHON=/usr/prog/python/3.5.1-goolf-1.5.14-NX/bin/python
SCRIPT_BASE=/usr/prog/onc/seqcount/1.0.0

function ceil {
    local n=$1
    echo `$PYTHON -c "from math import ceil; print(ceil($n))"`
}

function chop() { local str=$1; local len=$((`expr length "$str"` - 1)); echo ${str:0:$len}; }

function stringJoin() { local IFS="$1"; shift; echo "$*"; }

function removeTrailingSlash() {
    local DIR=$1
    if [ "${DIR:(-1)}" == "/" ]; then
        echo `chop $DIR`
    else
        echo $DIR
    fi
}

function logMessage {
    local MESSAGE=$1
    if [ ! -z "$LOG_FILE" ]; then
        echo -e "`date +\"%F %T\"`\t\t$MESSAGE" >> $LOG_FILE
    else
        echo -e "`date +\"%F %T\"`\t\t$MESSAGE" >&2
    fi
}

function echoAndLogMessage {
    local MESSAGE=$1
    echo -e "$MESSAGE"
    if [ ! -z "$LOG_FILE" ]; then
        logMessage "$MESSAGE"
    fi
}

function convertToUnix {
    local FILE=$1
    local TEMP_STDERR_FILE=`dirname $FILE`/dos2unix_stderr.txt
    dos2unix $FILE 2> $TEMP_STDERR_FILE
    local STDERR_MSG=$(<$TEMP_STDERR_FILE)
    rm $TEMP_STDERR_FILE
    logMessage "$STDERR_MSG"
}

function convertSampleSheetToUnix {
    local RUN_DIRECTORY=$1
    convertToUnix $RUN_DIRECTORY/SampleSheet.csv
}

function getSamples {
    local RUN_DIRECTORY=$1
    local SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "Extracting samples from $SAMPLE_SHEET"
    fi
    local COLUMN_HEADER_LINE_NUM=$((`grep -n "\[Data\]" $SAMPLE_SHEET | cut -d':' -f1`+1))
    local COLUMN_HEADER_LINE=`sed "${COLUMN_HEADER_LINE_NUM}q;d" $SAMPLE_SHEET`
    local SAMPLE_ID_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "Sample_Id" | cut -d':' -f1`
    if [ -z "$SAMPLE_ID_COL_NUM" ]; then
        logMessage "No column Sample_Id found in $SAMPLE_SHEET"
        return 1
    fi
    local SAMPLE_NAME_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "Sample_Name" | cut -d':' -f1`
    if [ -z "$SAMPLE_NAME_COL_NUM" ]; then
        logMessage "No column Sample_Name found in $SAMPLE_SHEET"
        return 1
    fi
    local SAMPLE_PROJECT_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "Sample_Project" | cut -d':' -f1`
    if [ -z "$SAMPLE_PROJECT_COL_NUM" ]; then
        logMessage "No column Sample_Project found in $SAMPLE_SHEET"
        return 1
    fi
    local SAMPLES=( `tail -n +$((COLUMN_HEADER_LINE_NUM+1)) $SAMPLE_SHEET | awk "BEGIN{FS=\",\"; OFS=\",\"} { print \$ ${SAMPLE_NAME_COL_NUM}, \$ ${SAMPLE_ID_COL_NUM}, \$ ${SAMPLE_PROJECT_COL_NUM} }" | sort -t',' -k1,1 | uniq` )
    echo ${SAMPLES[@]}
    return 0
}

function getNumSamples {
    local RUN_DIRECTORY=$1
    local SAMPLES=( `getSamples $RUN_DIRECTORY` )
    echo ${#SAMPLES[@]}
}

function getProjects {
    local RUN_DIRECTORY=$1
    local SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
    local COLUMN_HEADER_LINE_NUM=$((`grep -n "\[Data\]" $SAMPLE_SHEET | cut -d':' -f1`+1))
    local COLUMN_HEADER_LINE=`sed "${COLUMN_HEADER_LINE_NUM}q;d" $SAMPLE_SHEET`
    local SAMPLE_PROJECT_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "Sample_Project" | cut -d':' -f1`
    local PROJECTS=`tail -n +$((COLUMN_HEADER_LINE_NUM+1)) $SAMPLE_SHEET | cut -d',' -f${SAMPLE_PROJECT_COL_NUM} | sort | uniq`
    echo ${PROJECTS[@]}
}
function getNumProjects {
    local RUN_DIRECTORY=$1
    local PROJECTS=( `getProjects $RUN_DIRECTORY` )
    echo ${#PROJECTS[@]}
}

function fetchReferenceSample {
    module load oraclient
    local POOL=$1
    local QUERY_OUTPUT=$($PYTHON -c "import cx_Oracle as odb;
import pandas as pd;
conn = odb.connect('abacus/ps4prd2abc@abacus_usca_prod')
def fetchReferenceSampleFromPool(pool, conn):
    query = 'SELECT SAMPLE_SMF_ID FROM ABACUS.AB_POOL_MV WHERE CONCEPT = \'' + pool + '\'';
    df = pd.read_sql(query, conn);
    if len(df) == 0:
        query = 'SELECT SAMPLE_SMF_ID FROM ABACUS.AB_POOL_MV WHERE CONCEPT = \'' + pool.replace('_', ' ') + '\'';
        df = pd.read_sql(query, conn);
    assert len(df) > 0, 'No sample found for pool ' + pool;
    assert len(df) == 1, 'More than one sample found for pool ' + pool + ': ' + result[0] + ', ' + result2[0];
    return df.iloc[0].SAMPLE_SMF_ID

print(fetchReferenceSampleFromPool('$POOL', conn));
conn.close()")
    if [[ "$QUERY_OUTPUT" =~ ^[A-Z]{2}-[0-9]{2}-[A-Z]{2}[0-9]{2}$ ]]; then
        echo $QUERY_OUTPUT
        return 0
    else
        echo "Bad query output: $QUERY_OUTPUT"
        return 1
    fi
}

function writePoolReferenceSamples {
    local RUN_DIRECTORY=$1
    local CONFIG=$2
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "Writing pool reference config file $CONFIG"
    fi
    local PROJECTS=( `getProjects $RUN_DIRECTORY` )
    for POOL in ${PROJECTS[@]}; do
        local REF_SAMPLE=`fetchReferenceSample $POOL`
        local EXIT_STATUS=$?
        if [ "$EXIT_STATUS" -ne "0" ]; then
            logMessage "Error fetching sample for $POOL"
            return $EXIT_STATUS
        fi
        echo "${POOL}=${REF_SAMPLE}" >> $CONFIG
    done
    return 0
}

function getPoolToReferenceSample {
    local RUN_DIRECTORY=$1
    if [ "${#POOL_NAME_TO_REF_SAMPLE[@]}" -gt "0" ]; then
        return 0
    fi
    if [ -z "$POOL_REF_CONFIG_FILE" ]; then
        POOL_REF_CONFIG_FILE=$RUN_DIRECTORY/pool_reference_config.txt
    elif [ ! -f $POOL_REF_CONFIG_FILE ] && [ "`dirname $POOL_REF_CONFIG_FILE`" != "$RUN_DIRECTORY" ]; then
        POOL_REF_CONFIG_FILE=$RUN_DIRECTORY/$POOL_REF_CONFIG_FILE
    fi
    if [ ! -f $POOL_REF_CONFIG_FILE ]; then
        writePoolReferenceSamples $RUN_DIRECTORY $POOL_REF_CONFIG_FILE
        local EXIT_STATUS=$?
        if [ "$EXIT_STATUS" -ne "0" ]; then
            logMessage "Error writing pool reference samples to file"
            return "$EXIT_STATUS"
        fi
    fi
    exec 10<"${POOL_REF_CONFIG_FILE}"
    if [ ! -z "$VERBOSE" ] && [ "$VERBOSE" -eq "1" ]; then
        logMessage "Reading from $POOL_REF_CONFIG_FILE"
    fi
    while read -u 10 LINE; do
        if [ ! -z "$VERBOSE" ] && [ "$VERBOSE" -eq "1" ]; then
            logMessage "$LINE"
        fi
        if [[ "$LINE" =~ (.+)\=(.+) ]]; then
            POOL_NAME_TO_REF_SAMPLE[${BASH_REMATCH[1]}]=${BASH_REMATCH[2]}
        fi
    done
#    if [ "$VERBOSE" -eq "1" ]; then
#        logMessage "Returning from getPoolToReferenceSample"
#        logMessage "Pools:"
#        for POOL in ${!POOL_NAME_TO_REF_SAMPLE[@]}; do
#            logMessage "$POOL"
#        done
#        logMessage "SMF ids:"
#        for SMF in ${POOL_NAME_TO_REF_SAMPLE[@]}; do
#            logMessage "$SMF"
#        done
#    fi
    return 0
}

function writePoolToReferenceSampleAsYAML {
    local RUN_DIRECTORY=$1
    local OUTPUT_FILE=$2
    if [ -z "$RUN_DIRECTORY" ]; then
        echo "writePoolToReferenceSampleAsYAML: RUN_DIRECTORY arg not supplied" >&2
        return 1
    fi
    if [ ! -d "$RUN_DIRECTORY" ]; then
        echo "writePoolToReferenceSampleAsYAML: RUN_DIRECTORY ($RUN_DIRECTORY) not found" >&2
        return 1
    fi
    getPoolToReferenceSample $RUN_DIRECTORY
    if [ ! -z "$OUTPUT_FILE" ]; then
        echo "REFERENCE_SAMPLES: {" > $OUTPUT_FILE
    else
        echo "REFERENCE_SAMPLES {"
    fi
    for POOL in ${POOL_NAME_TO_REF_SAMPLE[@]}; do
        REF_SAMPLE=${POOL_NAME_TO_REF_SAMPLE[$POOL]}
        if [ ! -z "$OUTPUT_FILE" ]; then
            echo -e "\t'$POOL': '$REF_SAMPLE'" >> $OUTPUT_FILE
        else
            echo -e "\t'$POOL': '$REF_SAMPLE'"
        fi
    done
    if [ ! -z "$OUTPUT_FILE" ]; then
        echo "}" >> $OUTPUT_FILE
    else
        echo "}"
    fi
}

function writeSampleSheetAsYAML {
    local RUN_DIRECTORY=$1
    local SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
    if [ ! -f "$SAMPLE_SHEET" ]; then
        logMessage $SAMPLE_SHEET not found
        return 1
    fi
    local COLUMN_HEADER_LINE_NUM=$((`grep -n "\[Data\]" $SAMPLE_SHEET | cut -d':' -f1`+1))
    $PYTHON $SCRIPT_BASE/sample_sheet_to_yaml.py $SAMPLE_SHEET -n $((COLUMN_HEADER_LINE_NUM-1))
}

function getReferenceSamples {
    local RUN_DIRECTORY=$1
    if [ "${#POOL_NAME_TO_REF_SAMPLE[@]}" -eq "0" ]; then
        getPoolToReferenceSample $RUN_DIRECTORY
        local EXIT_STATUS=$?
        if [ "$EXIT_STATUS" -ne "0" ]; then
            logMessage "Error populating reference samples array"
            exit $EXIT_STATUS
        fi
    fi
    local SAMPLES=( `getSamples $RUN_DIRECTORY` )
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "Constructing REF_SAMPLES array"
    fi
    local i=0
    declare -a REF_SAMPLES
    for SAMPLE in ${SAMPLES[@]}; do
        local SAMPLE_INFO=( `echo $SAMPLE | tr "," "\n"` )
        local SAMPLE_NAME=${SAMPLE_INFO[0]}
        local SAMPLE_PROJECT=${SAMPLE_INFO[2]}
        local REF_SAMPLE="${POOL_NAME_TO_REF_SAMPLE[$SAMPLE_PROJECT]}"
        if [ "$SAMPLE_NAME" == "$REF_SAMPLE" ]; then
            REF_SAMPLES[$i]=$SAMPLE
            i=$((i+1))
        fi
    done
    echo ${REF_SAMPLES[@]}
}

function getNonReferenceSamples {
    local RUN_DIRECTORY=$1
    if [ "${#POOL_NAME_TO_REF_SAMPLE[@]}" -eq "0" ]; then
        getPoolToReferenceSample $RUN_DIRECTORY
    fi
    local SAMPLES=( `getSamples $RUN_DIRECTORY` )
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "Constructing NON_REF_SAMPLES array"
    fi
    local i=0
    declare -a NON_REF_SAMPLES
    for SAMPLE in ${SAMPLES[@]}; do
        local SAMPLE_INFO=( `echo $SAMPLE | tr "," "\n"` )
        local SAMPLE_NAME=${SAMPLE_INFO[0]}
        local SAMPLE_PROJECT=${SAMPLE_INFO[2]}
        local REF_SAMPLE=${POOL_NAME_TO_REF_SAMPLE[$SAMPLE_PROJECT]}
#        if [ "$VERBOSE" -eq "1" ]; then
#            logMessage "Sample : $SAMPLE_NAME . Pool: $SAMPLE_PROJECT. Reference sample: $REF_SAMPLE"
#        fi
        if [ "$SAMPLE_NAME" != "$REF_SAMPLE" ]; then
#            if [ "$VERBOSE" -eq "1" ]; then
#                logMessage "Adding $SAMPLE_NAME to non-reference samples"
#            fi
            NON_REF_SAMPLES[$i]=$SAMPLE
            i=$((i+1))
        fi
    done
    echo ${NON_REF_SAMPLES[@]}
}

function getNumSequenceChunks {
    local RUN_DIRECTORY=$1
    local SEQUENCE_CHUNK_SIZE=$2
    local SAMPLES=( `getSamples $RUN_DIRECTORY` )
    local NUM_SEQUENCE_CHUNKS=0
    local SAMPLE_INFO=""
    local SAMPLE_NAME=""
    local SAMPLE_PROJECT=""
    local INPUT_DIR=""
    local SEQUENCE_FILE=""
    local NUM_UNMATCHED_SEQUENCES=""
    local SAMPLE_NUM_SEQUENCE_CHUNKS=""
    for SAMPLE in ${SAMPLES[@]}; do
        SAMPLE_INFO=( `echo $SAMPLE | tr "," "\n"` )
        SAMPLE_NAME=${SAMPLE_INFO[0]}
        SAMPLE_PROJECT=${SAMPLE_INFO[2]}
        INPUT_DIR=$RUN_DIRECTORY/Aligned/Exact/${SAMPLE_PROJECT}/${SAMPLE_NAME}
        SEQUENCE_FILE=${INPUT_DIR}/unmatched.txt
        if [ ! -f $SEQUENCE_FILE ]; then
            echo "ERROR: $SEQUENCE_FILE could not be found"
            return 1
        fi
        NUM_UNMATCHED_SEQUENCES=`wc -l $SEQUENCE_FILE | awk '{print $1}'`
        SAMPLE_NUM_SEQUENCE_CHUNKS=$(ceil `echo "$NUM_UNMATCHED_SEQUENCES/$SEQUENCE_CHUNK_SIZE" | bc -l`)
        NUM_SEQUENCE_CHUNKS=$((NUM_SEQUENCE_CHUNKS+SAMPLE_NUM_SEQUENCE_CHUNKS))
    done
    if [[ ! $NUM_SEQUENCE_CHUNKS =~ ^[0-9]+$ ]]; then
        # shouldn't get here
        echo "ERROR: num_sequence_chunks calculated is not an integer:" $NUM_SEQUENCE_CHUNKS >&2
        return 1
    fi
    echo $NUM_SEQUENCE_CHUNKS
    return 0
}

function getIndex {
    local RUN_DIRECTORY=$1
    local SAMPLE_SMF_ID=$2
    local SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
    local COLUMN_HEADER_LINE_NUM=$((`grep -n "\[Data\]" $SAMPLE_SHEET | cut -d':' -f1`+1))
    local COLUMN_HEADER_LINE=`sed "${COLUMN_HEADER_LINE_NUM}q;d" $SAMPLE_SHEET`
    local SAMPLE_INDEX_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "index" | cut -d':' -f1`
    local SAMPLES=( `getSamples $RUN_DIRECTORY` )
    local SAMPLE_INFO=""
    local SAMPLE_NAME=""
    local INDEX=""
    local NEW_INDEX=""
    local INDEXES=( `tail -n +$((COLUMN_HEADER_LINE_NUM+1)) $SAMPLE_SHEET | grep "$SAMPLE_SMF_ID" | cut -d',' -f ${SAMPLE_INDEX_COL_NUM} | sort | uniq` )
    local NUM_INDEXES=${#INDEXES[@]}
    if [ "$NUM_INDEXES" -eq "0" ]; then
        echo "Sample $SAMPLE_SMF_ID not found in $SAMPLE_SHEET"
        return 1
    elif [ "$NUM_INDEXES" -gt "1" ]; then
        echo "More than one index found for sample $SAMPLE_SMF_ID:"
        echo ${INDEXES[@]}
        return 1
    fi
    echo $INDEXES
    return 0
}

function getFlowcellId {
    local RUN_DIRECTORY=$1
    local SAMPLE_SHEET=$RUN_DIRECTORY/SampleSheet.csv
    local COLUMN_HEADER_LINE_NUM=$((`grep -n "\[Data\]" $SAMPLE_SHEET | cut -d':' -f1`+1))
    local COLUMN_HEADER_LINE=`sed "${COLUMN_HEADER_LINE_NUM}q;d" $SAMPLE_SHEET`
    local FLOWCELL_ID_COL_NUM=`echo $COLUMN_HEADER_LINE | tr "," "\n" | grep -n "FCID" | cut -d':' -f1`
    local FLOWCELL_IDS=( `tail -n +$((COLUMN_HEADER_LINE_NUM+1)) $SAMPLE_SHEET | cut -d',' -f${FLOWCELL_ID_COL_NUM}` )
    local FCID=${FLOWCELL_IDS[0]}
    if [ -z "$FCID" ]; then
        echo "FCID column not found in $SAMPLE_SHEET"
        return 1
    fi
    for fcid in ${FLOWCELL_IDS[@]}; do
        if [ "$fcid" != "$FCID" ]; then
            echo "More than one flowcell id detected for directory $RUN_DIRECTORY: $fcid, $FCID"
            return 1
        fi
    done
    echo $FCID
    return 0
}

function submitScriptAsClusterJob {
    local SCRIPT=$1
    SUBMIT_CMD="qsub $SCRIPT"
    RESPONSE=`$SUBMIT_CMD`
    echo $RESPONSE
}

function extractClusterJobIdFromSubmissionResponse {
    local RESPONSE="$1"
    if [[ "`echo $RESPONSE | tr ' ' ';'`" =~ Your\;job(-array)?\;([0-9]+) ]]; then
        local JOB_ID=${BASH_REMATCH[2]}
    else
        echo -e "Unexpected response to qsub - could not find job id:\n$RESPONSE"
        return 1
    fi
    echo $JOB_ID
    return 0
}

function checkFailedTasks {
    local FILE=$1
    local PARENT_JOB_ID=$2
    if [ -z "$PARENT_JOB_ID" ]; then
        return 0
    fi
    if [ ! -f $FILE ]; then
        echo "Error: Run log file $FILE not found"
        return 1
    fi
    local FAILED_TASKS=`grep "$PARENT_JOB_ID" $FILE | grep "FAIL"`
    if [ ! -z "$FAILED_TASKS" ]; then
        echo "Not all tasks from previous step (job id $PARENT_JOB_ID) succeeded. Failed tasks are as follows:"
        head -n 1 $FILE
        echo $FAILED_TASKS
        return 1
    fi
    return 0
}

function checkSuccessfulTasks {
    local FILE=$1
    local JOB_ID=$2
    local NUM_TASKS=$3
    if [ -z $JOB_ID ]; then
        return 0
    fi
    if [ ! -f $FILE ]; then
        echo "Error: Run log file $FILE not found"
        return 1
    fi
    if [ -z "$NUM_TASKS" ]; then
        echo "Bug: NUM_TASKS not supplied to checkSuccessfulTasks"
        return 1
    fi
    local NUM_SUCCESSFUL_TASKS=`grep "$JOB_ID" $FILE | grep -c "SUCCESS"`
    if [ "$NUM_SUCCESSFUL_TASKS" -ne "$NUM_TASKS" ]; then
        echo "Not all tasks from previous step (job id $JOB_ID) succeeded. Failed tasks are as follows:"
        head -n 1 $FILE
        for i in `seq 1 $NUM_TASKS`; do
            local TASK_STATUS=`grep -P "$JOB_ID\t$i\t" $FILE`
            if [ -z "$TASK_STATUS" ]; then
                echo -e "$JOB_ID\t$i\tINCOMPLETE"
            else
                if [[ "$TASK_STATUS" =~ .*FAIL.* ]]; then
                    echo $TASK_STATUS
                elif [[ ! "$TASK_STATUS" =~ .*SUCCESS.* ]]; then
                    echo -e "Inscrutable task status:\n$TASK_STATUS"
                    exit 1
                fi
            fi
        done
        return 1
    fi
    return 0
}

function checkLastTask {
    local FILE=$1
    local JOB_ID=$2
    if [ -z $JOB_ID ]; then
        return 0
    fi
    local LAST_TASK=`grep "^$JOB_ID" $FILE | tail -n 1`
    if [[ "$LAST_TASK" =~ .*SUCCESS.* ]]; then
        echo $LAST_TASK
        return 0
    elif [[ "$LAST_TASK" =~ .*FAIL.* ]]; then
        echo -e "Last task failed for job $JOB_ID:\n$LAST_TASK"
        return 1
    else
        echo -e "Inscrutable task log:\n$LAST_TASK"
        exit 1
    fi
}

function checkTask {
    local FILE=$1
    local JOB_ID=$2
    local TASK_ID=$3
    if [ -z $JOB_ID ]; then
        return 0
    fi
    local TASK=`grep -P "^$JOB_ID\t$TASK_ID" $FILE`
    if [[ "$TASK" =~ .*SUCCESS.* ]]; then
        echo $TASK
        return 0
    elif [[ "$TASK" =~ .*FAIL.* ]]; then
        echo "Task $TASK_ID failed for job $JOB_ID"
        return 1
    else
        echo -e "Inscrutable task log:\n$TASK"
        exit 1
    fi
}

function waitForClusterJob {
    local JOB_ID=$1
    if [ -z "$JOB_ID" ]; then
        return 0
    fi
    local WAIT_TIME_IN_SECS=30
    local TEMP_STDOUT_FILE=qstat_out.txt
    local TEMP_STDERR_FILE=qstat_err.txt
    qstat > $TEMP_STDOUT_FILE 2> $TEMP_STDERR_FILE
    if [ ! -f $TEMP_STDOUT_FILE ]; then
        if [ -f $TEMP_STDERR_FILE ]; then
            echoAndLogMessage "qstat stdout temp file does not exist but error function does"
            exit 1
        fi

    fi
    local JOB_RUNNING=`grep -P "^\s*$JOB_ID\s+" $TEMP_STDOUT_FILE`
    local ERROR=`grep -i "error" $TEMP_STDERR_FILE`
    while [ ! -z "$JOB_RUNNING" ] || [ ! -z "$ERROR" ]; do
        sleep $WAIT_TIME_IN_SECS
        qstat > $TEMP_STDOUT_FILE 2> $TEMP_STDERR_FILE
        JOB_RUNNING=`grep -P "^\s*$JOB_ID\s+" $TEMP_STDOUT_FILE`
        ERROR=`grep -i "error" $TEMP_STDERR_FILE`
    done
    rm $TEMP_STDOUT_FILE
    rm $TEMP_STDERR_FILE
    return 0
}

function waitForBashJob {
    local JOB_ID=$1
    if [ -z "$JOB_ID" ]; then
        return 0
    fi
    while [ -e /proc/$JOB_ID ]; do
        sleep 30
    done
    return 0
}

function checkExitStatus {
    local EXIT_STATUS=$1
    local ERR_MSG=$2
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$ERR_MSG"
        exit $EXIT_STATUS
    fi
}

function executeCmd {
    local CMD="$1"
    local REQUIRE_VERBOSE=$2
    if [ -z "$REQUIRE_VERBOSE" ]; then
        REQUIRE_VERBOSE=1
    fi
    if [ "$REQUIRE_VERBOSE" -ne "1" ] || [ "$VERBOSE" -eq "1" ]; then
        logMessage "$CMD"
    fi
    $CMD
}

function calculateNewAlignmentPercentage {
    local COUNTS=$1
    local UNMATCHED=$2

    if [ -z $COUNTS ] || [ -z $UNMATCHED ]; then
      echo "Usage: $0 <counts_file> <unmatched_file>"
      exit 1
    fi

    local ALIGN_PCT=`$PYTHON -c "def calculateAlignmentPercentage(countsFile, unmatchedFile):
    numMatched = 0
    c = open(countsFile, 'r')
    for line in c:
        numMatched += float(line.strip().split(\"\t\")[2])
    c.close()
    numUnmatched = 0
    u = open(unmatchedFile, 'r')
    for line in u:
        numUnmatched += int(line.strip().split(\"\t\")[1])
    u.close()
    return numMatched/(numMatched + numUnmatched) * 100

print(calculateAlignmentPercentage('$COUNTS', '$UNMATCHED'))"`

printf "%.2f\n" $ALIGN_PCT
}

function splitString {
    local STR=$1
    local DELIM=$2
    local IFS=$DELIM
    read -ra ADDR <<< "$STR"
    echo ${ADDR[@]}
}

