#!/usr/bin/env bash
#$ -S /bin/bash
#$ -N @JOB_NAME@
#$ -wd @JOB_DIRECTORY@
#$ -j y
#$ -t 1-@NUM_PROJECTS@
#$ -l h_rt=@MAX_RUNTIME@
#$ -binding linear:1
@EMAIL_NOTIFICATIONS_CONFIG@
@EMAIL_ADDRESS_CONFIG@
@PARENT_JOB_CONFIG@

#################################################
# Takes a bcl2fastq2-compliant root directory   #
# and uses the demultiplexed fastq.gz files to  #
# run the exact matching portion of the aligner.#
#################################################

VERBOSE=@VERBOSE@
ABACUS_USERNAME=@ABACUS_USERNAME@
ABACUS_PASSWORD=@ABACUS_PASSWORD@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "$RUN_DIRECTORY/util_functions.sh not found" >&1 >&2
    exit 1
fi
source $RUN_DIRECTORY/util_functions.sh
if [ ! -f $RUN_DIRECTORY/pipeline_steps.sh ]; then
    echo "$RUN_DIRECTORY/pipeline_steps.sh not found" >&1 >&2
    exit 1
fi
source $RUN_DIRECTORY/pipeline_steps.sh

LIB_LOC=@LIB_LOC@
UNZIP=@UNZIP@

#SAMPLES=( `getSamples $RUN_DIRECTORY` )
#SAMPLE_NUM=$SGE_TASK_ID
# SAMPLE_INFO=( `echo ${SAMPLES[$((SAMPLE_NUM-1))]} | tr "," "\n"` ) # select from the first NUM_PROJECTS samples
#SAMPLE_ID=${SAMPLE_INFO[0]}
#SAMPLE_NAME=${SAMPLE_INFO[1]}
#SAMPLE_PROJECT=${SAMPLE_INFO[2]}
SAMPLE_NUM=$SGE_TASK_ID
REF_SAMPLES=( `getReferenceSamples $RUN_DIRECTORY` )
if [ "$VERBOSE" -eq "1" ]; then
    echo "Reference samples:"
    for SAMPLE in ${REF_SAMPLES[@]}; do
        echo $SAMPLE
    done
fi
SAMPLE_INFO=( `echo ${REF_SAMPLES[$((SAMPLE_NUM-1))]} | tr "," "\n"` ) # select from the first NUM_PROJECTS samples
SAMPLE_NAME=${SAMPLE_INFO[0]}
SAMPLE_ID=${SAMPLE_INFO[1]}
SAMPLE_PROJECT=${SAMPLE_INFO[2]}

DEFAULT_LIB_ROOT_DIR=/mnt/tmplabdata/oncpar/ILLUMINA/MyGenome
if [ ! -z "$LIB_LOC" ] && [ ! -e "$LIB_LOC" ]; then
    echo "Library path $LIB_LOC does not exist" >&1 >&2
    exit 100
fi
if [ -z "$LIB_LOC" ]; then
    echo "Searching for library file for pool $SAMPLE_PROJECT in default directory $DEFAULT_LIB_ROOT_DIR" >&2
    LIB_ROOT_DIR=$DEFAULT_LIB_ROOT_DIR
    LIB_FILE=$LIB_ROOT_DIR/$SAMPLE_PROJECT/$SAMPLE_PROJECT.fa
    if [ ! -f "$LIB_FILE" ]; then
        LIB_FILE=$LIB_ROOT_DIR/$SAMPLE_PROJECT.fa
    fi
elif [ -d "$LIB_LOC" ]; then
    echo "Searching for library file for pool $SAMPLE_PROJECT in supplied directory $LIB_LOC" >&2
    LIB_ROOT_DIR=$LIB_LOC
    LIB_FILE=$LIB_ROOT_DIR/$SAMPLE_PROJECT/$SAMPLE_PROJECT.fa
    if [ ! -f "$LIB_FILE" ]; then
        LIB_FILE=$LIB_ROOT_DIR/$SAMPLE_PROJECT.fa
    fi
else
    echo "Attempting to use library file $LIB_LOC for pool $SAMPLE_PROJECT" >&2
    LIB_FILE=$LIB_LOC
fi
if [ ! -f "$LIB_FILE" ]; then
    echo "No library file could be found from the location $LIB_LOC" >&1 >&2
    exit 100
fi

STEP_NUMBER=@STEP_NUMBER@
PARENT_JOB=@PARENT_JOB@
TRIM_READS=@TRIM_READS@
if [ ! -z "$PARENT_JOB" ] && [ "$TRIM_READS" -eq "0" ]; then
    PREV_STEP_NUMBER=$((STEP_NUMBER-2))
    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
    # demultiplexing is not parallelized, check the most recent log entry
    LAST_TASK=`checkLastTask $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$LAST_TASK" >&1 >&2
        exit $EXIT_STATUS
    fi
elif [ ! -z "$PARENT_JOB" ]; then
    PREV_STEP_NUMBER=$((STEP_NUMBER-1))
    PREV_STEP=${PIPELINE_STEPS[$((PREV_STEP_NUMBER-1))]}
    NUM_SAMPLES=`getNumSamples $RUN_DIRECTORY`
    TASK_CHECK=`checkSuccessfulTasks $RUN_DIRECTORY/${PREV_STEP_NUMBER}_${PREV_STEP}/RUN_RESULT $PARENT_JOB $NUM_SAMPLES`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$TASK_CHECK" >&1 >&2
        exit $EXIT_STATUS
    fi
fi

EXACT_MATCHING_SCRIPT=$JOB_DIRECTORY/exact_matching.py
ROOT_OUTPUT_DIR=$RUN_DIRECTORY/Aligned/Exact
PROJECT_OUTPUT_DIR=$ROOT_OUTPUT_DIR/$SAMPLE_PROJECT
SAMPLE_OUTPUT_DIR=$PROJECT_OUTPUT_DIR/$SAMPLE_NAME
mkdir -p $SAMPLE_OUTPUT_DIR

if [ "$UNZIP" -eq "1" ]; then
    ZIPPED_FILES=( `ls $RUN_DIRECTORY/Data/Intensities/BaseCalls/${SAMPLE_PROJECT}/${SAMPLE_ID}/${SAMPLE_NAME}*.fastq.gz` )
    READ_FILES=""
    for ZIPPED_FILE in ${ZIPPED_FILES[@]}; do
        if [[ $ZIPPED_FILE =~ (.+)\.gz ]]; then
            UNZIPPED_FILE=${BASH_REMATCH[1]}
            echo "Unzipping $ZIPPED_FILE" >&2
            zcat $ZIPPED_FILE > $UNZIPPED_FILE
            READ_FILES+=${UNZIPPED_FILE},
        fi
    done
else
    READ_FILES=`ls $RUN_DIRECTORY/Data/Intensities/BaseCalls/${SAMPLE_PROJECT}/${SAMPLE_ID}/${SAMPLE_NAME}*.fastq.gz | tr "\n" ","`
fi
READ_FILES=`chop $READ_FILES`

PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python
CMD="$PYTHON -u $EXACT_MATCHING_SCRIPT -r $READ_FILES -l $LIB_FILE --matches $SAMPLE_OUTPUT_DIR/matched.txt --missing $SAMPLE_OUTPUT_DIR/unmatched.txt -b @BASE_CUTOFF@ -a @AVERAGE_CUTOFF@ --read_counts_pickle $SAMPLE_OUTPUT_DIR/read_counts.p --library_pickle $PROJECT_OUTPUT_DIR/library.p"
echo "$CMD" >&2
$CMD
EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]; then
    JOB_STATUS=SUCCESS
else
    JOB_STATUS=FAIL
fi
if [ "$JOB_STATUS" == "SUCCESS" ] && ([ ! -f $SAMPLE_OUTPUT_DIR/matched.txt ] ||  [ ! -f $SAMPLE_OUTPUT_DIR/unmatched.txt ]); then
    JOB_STATUS=FAIL
fi

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
LOCKFILE=$JOB_DIRECTORY/cc.lock
while [ -e $LOCKFILE ]; do
    sleep 1
done
lockfile $LOCKFILE
if [ ! -f $RUN_LOG_FILE ]; then
    echo -e "JOB_ID\tTASK_ID\tJOB_NAME\tSAMPLE_NUMBER\tDATE\tSTATUS" > $RUN_LOG_FILE
fi
echo -e "$JOB_ID\t$SGE_TASK_ID\t@JOB_NAME@\t$SAMPLE_NUM\t`date +\"%F_%T\"`\t$JOB_STATUS" >> $RUN_LOG_FILE
rm -f $LOCKFILE
if [ "$JOB_STATUS" == "FAIL" ] || [ "$EXIT_STATUS" -ne "0" ]; then
    exit 100
else
    exit 0
fi