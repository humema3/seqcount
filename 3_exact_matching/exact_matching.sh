#!/usr/bin/env bash

STEP_NUMBER=3
ARGS="$@"

SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_configuration.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/util_functions.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)
source `dirname $SCRIPT_DIR`/pipeline_steps.sh
SCRIPT_DIR=$(dirname `readlink -f $0`)

assignPipelineFlags -c -a -p -E -S -C -j SEQ_COUNT_EXACT_MATCH -m 345599 -J `basename $0`
assignArgs $ARGS
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ] || [ "$HELP" -eq "1" ]; then
    exit $EXIT_STATUS
fi
checkArgs
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    exit $EXIT_STATUS
fi

SAMPLES=( `getSamples $RUN_DIRECTORY` )
EXIT_STATUS=$?
if [ "$EXIT_STATUS" -ne "0" ]; then
    echoAndLogMessage "Step $STEP_NUMBER failed: Error extracting samples from SampleSheet.csv"
    exit $EXIT_STATUS
fi
NUM_SAMPLES=${#SAMPLES[@]}
NUM_PROJECTS=`getNumProjects $RUN_DIRECTORY`
if [[ ! $NUM_SAMPLES =~ ^[0-9]+$ ]]; then
    # shouldn't get here
    echoAndLogMessage "Step $STEP_NUMBER failed: num_samples calculated is not an integer: $NUM_SAMPLES"
    exit 1
fi
if [[ ! $NUM_PROJECTS =~ ^[0-9]+$ ]]; then
    # shouldn't get here
    echoAndLogMessage "Step $STEP_NUMBER failed: num_projects calculated is not an integer: $NUM_PROJECTS"
    exit 1
fi

JOB_DIRECTORY=$RUN_DIRECTORY/`basename $SCRIPT_DIR`
mkdir -p $JOB_DIRECTORY
START_SCRIPT_TEMPLATE=$SCRIPT_DIR/start_exact_matching_template.sh
START_SCRIPT=$JOB_DIRECTORY/start_exact_matching.sh
RUN_SCRIPT_TEMPLATE=$SCRIPT_DIR/run_exact_matching_template.sh
RUN_SCRIPT=$JOB_DIRECTORY/run_exact_matching.sh

# escape special characters for regex substitution
JOB_NAME=${JOB_NAME//\//\\\/}
JOB_NAME=${JOB_NAME//@/\\@}
PARENT_JOB_CONFIG=${PARENT_JOB_CONFIG//\$/\\\$}
START_JOB_NAME=start_${JOB_NAME}
EMAIL_ADDRESS_CONFIG=${EMAIL_ADDRESS_CONFIG//@/\\@}
FORMATTED_JOB_DIRECTORY=${JOB_DIRECTORY//\//\\\/}
FORMATTED_LIB_LOC=${LIBRARY_LOCATION//\//\\\/}

executeCmd "cp `dirname $SCRIPT_DIR`/util_functions.sh -t $RUN_DIRECTORY"
executeCmd "cp `dirname $SCRIPT_DIR`/pipeline_steps.sh -t $RUN_DIRECTORY"
executeCmd "cp $SCRIPT_DIR/exact_matching.py -t $JOB_DIRECTORY"

executeCmd "cp $START_SCRIPT_TEMPLATE $START_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/$START_JOB_NAME/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_NAME\@/$START_JOB_NAME/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@EMAIL\@/$EMAIL/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@NUM_PROJECTS\@/$NUM_PROJECTS/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@BASE_CUTOFF\@/$BASE_QUALITY_CUTOFF/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@AVERAGE_CUTOFF\@/$AVERAGE_QUALITY_CUTOFF/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@UNZIP\@/$UNZIP/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@PARENT_JOB\@/$PARENT_JOB/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@STEP_NUMBER\@/$STEP_NUMBER/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@TRIM_READS\@/$TRIM_READS/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@LIB_LOC\@/$FORMATTED_LIB_LOC/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@ABACUS_USERNAME\@/$ABACUS_USERNAME/g; $START_SCRIPT"
executeCmd "perl -i -pe s/\@ABACUS_PASSWORD\@/$ABACUS_PASSWORD/g; $START_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $START_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $START_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $START_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $START_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;\" $START_SCRIPT"
fi
perl -i -pe "s/\@PARENT_JOB_CONFIG\@/$PARENT_JOB_CONFIG/g;" $START_SCRIPT

if [ "$VERBOSE" -eq "1" ]; then
    logMessage "Writing to $RUN_SCRIPT"
fi
cp $RUN_SCRIPT_TEMPLATE $RUN_SCRIPT
executeCmd "perl -i -pe s/\@JOB_NAME\@/$JOB_NAME/g; $RUN_SCRIPT"
#perl -i -pe "s/\@PARENT_JOB\@/$START_JOB_NAME/g;" $RUN_SCRIPT
executeCmd "perl -i -pe s/\@EMAIL\@/$EMAIL/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@JOB_DIRECTORY\@/$FORMATTED_JOB_DIRECTORY/g; $RUN_SCRIPT"
#SUB_CMD="perl -i -pe s/\@START_SAMPLE\@/$((NUM_PROJECTS+1))/g; $RUN_SCRIPT"
#if [ "$VERBOSE" -eq "1" ]; then
#    logMessage "$SUB_CMD"
#fi
#$SUB_CMD
#SUB_CMD="perl -i -pe s/\@NUM_SAMPLES\@/$NUM_SAMPLES/g; $RUN_SCRIPT"
NUM_NON_REF_SAMPLES=$((NUM_SAMPLES-NUM_PROJECTS))
executeCmd "perl -i -pe s/\@NUM_NON_REF_SAMPLES\@/$NUM_NON_REF_SAMPLES/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@MAX_RUNTIME\@/$MAX_RUNTIME/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@BASE_CUTOFF\@/$BASE_QUALITY_CUTOFF/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@AVERAGE_CUTOFF\@/$AVERAGE_QUALITY_CUTOFF/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@UNZIP\@/$UNZIP/g; $RUN_SCRIPT"
executeCmd "perl -i -pe s/\@VERBOSE\@/$VERBOSE/g; $RUN_SCRIPT"
# explicit calls where interpolation doesn't work well
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_NOTIFICATIONS_CONFIG\@/$EMAIL_NOTIFICATIONS_CONFIG/g;" $RUN_SCRIPT
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "perl -i -pe \"s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;\" $RUN_SCRIPT"
fi
perl -i -pe "s/\@EMAIL_ADDRESS_CONFIG\@/$EMAIL_ADDRESS_CONFIG/g;" $RUN_SCRIPT

START_CMD="qsub $START_SCRIPT"
RUN_CMD="qsub $RUN_SCRIPT"

function runCurrentStep {
    logMessage "$START_CMD"
    local START_RESPONSE=`submitScriptAsClusterJob $START_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$START_RESPONSE"
    fi
    local START_JOB_ID=`extractClusterJobIdFromSubmissionResponse "$START_RESPONSE"`
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$START_JOB_ID"
        exit $EXIT_STATUS
    fi
    executeCmd "perl -i -pe s/\@PARENT_JOB\@/$START_JOB_ID/g; $RUN_SCRIPT" 0
    logMessage "$RUN_CMD"
    local RESPONSE=`submitScriptAsClusterJob $RUN_SCRIPT`
    if [ "$VERBOSE" -eq "1" ]; then
        logMessage "$RESPONSE"
    fi
    local JOB_ID=`extractClusterJobIdFromSubmissionResponse "$RESPONSE"`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echoAndLogMessage "$JOB_ID"
        exit $EXIT_STATUS
    fi

    local NEXT_STEP_NUMBER=$((STEP_NUMBER+1))
    local NEXT_STEP=${PIPELINE_STEPS[$((NEXT_STEP_NUMBER-1))]}
    local NEXT_CMD="`dirname $SCRIPT_DIR`/${NEXT_STEP_NUMBER}_${NEXT_STEP}/${NEXT_STEP}.sh -r $RUN_DIRECTORY"
    if [ ! -z "$EMAIL" ]; then
        NEXT_CMD+=" -e $EMAIL"
    elif [ "$NO_EMAIL" -eq "1" ]; then
        NEXT_CMD+=" -n"
    fi
    if [ "$AUTO_CASCADE" -eq "1" ]; then
        NEXT_CMD+=" -A"
    fi
    NEXT_CMD+=" -H $MAX_HAMMING_DISTANCE"
    NEXT_CMD+=" -I $MAX_END_INDEL_DISTANCE"
    NEXT_CMD+=" -s $SEQUENCE_CHUNK_SIZE"
    NEXT_CMD+=" -c $MAX_NUM_CONCURRENT_TASKS"
    if [ "$NO_INDELS" -eq "1" ]; then
        NEXT_CMD+=" -i"
    fi
    NEXT_CMD+=" -P $JOB_ID"
    NEXT_CMD+=" -U $ABACUS_USERNAME"
    NEXT_CMD+=" -o $REPORT_FILEPATH"
    if [ ! -z "$LOG_FILE" ]; then
       NEXT_CMD+=" -L $LOG_FILE"
    fi

    if [ -z "$ABACUS_PASSWORD" ]; then
        if [ "$AUTO_CASCADE" -eq "1" ]; then
            echoAndLogMessage "Abacus password (-p) is required for report creation step; terminating after current step"
            exit 0
        fi
    else
        NEXT_CMD+=" -p $ABACUS_PASSWORD"
    fi
    if [ "$VERBOSE" -eq "1" ]; then
        NEXT_CMD+=" -v"
    fi

    if [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
        executeCmd "$NEXT_CMD" 0
    else
        echo
        echo "Next step: $NEXT_STEP. Run? (y/n)"
        read ANSWER
        if [[ "$ANSWER" =~ ^[Yy] ]]; then
            executeCmd "$NEXT_CMD" 0
        else
            if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
                echo "Response not understood - not proceeding with next step"
            fi
            echo "To run: $NEXT_CMD"
        fi
    fi
}

if [ "$AUTORUN" -eq "1" ] || [ "$RUN_NEXT_STEP" -eq "1" ] || [ "$AUTO_CASCADE" -eq "1" ]; then
    runCurrentStep
else
    echo "Submit job now? (y/n)"
    read ANSWER
    if [[ "$ANSWER" =~ ^[Yy] ]]; then
        runCurrentStep
    else
        if [[ ! "$ANSWER" =~ ^[Nn] ]]; then
            echo "Response not understood - not running current step"
        fi
        echoAndLogMessage "To run:\n1) $START_CMD\n2) perl -i -pe \"s/\\@PARENT_JOB\\@/<job id from step 1>/g;\" $RUN_SCRIPT\n3) $RUN_CMD"
    fi
fi
