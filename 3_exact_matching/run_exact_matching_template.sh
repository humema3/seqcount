#!/usr/bin/env bash
#$ -S /bin/bash
#$ -N @JOB_NAME@
#$ -wd @JOB_DIRECTORY@
#$ -j y
#$ -t 1-@NUM_NON_REF_SAMPLES@
#$ -l h_rt=@MAX_RUNTIME@
#$ -binding linear:1
#$ -hold_jid @PARENT_JOB@
@EMAIL_NOTIFICATIONS_CONFIG@
@EMAIL_ADDRESS_CONFIG@

#################################################
# Takes a bcl2fastq2-compliant root directory   #
# and uses the demultiplexed fastq.gz files to  #
# run the exact matching portion of the aligner.#
#################################################

VERBOSE=@VERBOSE@

JOB_DIRECTORY=@JOB_DIRECTORY@
RUN_DIRECTORY=`dirname $JOB_DIRECTORY`
if [ ! -f $RUN_DIRECTORY/util_functions.sh ]; then
    echo "$RUN_DIRECTORY/util_functions.sh not found" >&1 >&2
    exit 1
fi
source $RUN_DIRECTORY/util_functions.sh

EXACT_MATCHING_SCRIPT=$JOB_DIRECTORY/exact_matching.py
ROOT_OUTPUT_DIR=$RUN_DIRECTORY/Aligned/Exact
UNZIP=@UNZIP@

# check that all previous tasks succeeded
PARENT_JOB=@PARENT_JOB@
if [ ! -z "$PARENT_JOB" ]; then
    NUM_PROJECTS=`getNumProjects $RUN_DIRECTORY`
    TASK_CHECK=`checkSuccessfulTasks $JOB_DIRECTORY/RUN_RESULT @PARENT_JOB@ $NUM_PROJECTS`
    EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "$TASK_CHECK" >&1 >&2
        exit 100
    fi
fi

#NUM_SAMPLES=${#SAMPLES[@]}
#if [ "$NUM_SAMPLES" -ne "@NUM_SAMPLES@" ]; then
#    echo "Number of samples provided for parallelization (@NUM_SAMPLES@) does not match number found in sample sheet ($NUM_SAMPLES)" >&1 >&2
#    exit 1
#fi

SAMPLE_NUM=$SGE_TASK_ID
NON_REF_SAMPLES=( `getNonReferenceSamples $RUN_DIRECTORY` )
if [ "${#NON_REF_SAMPLES[@]}" -ne "@NUM_NON_REF_SAMPLES@" ]; then
    echo "Number of samples provided for parallelization (@NUM_NON_REF_SAMPLES@) does not match number found in sample sheet (${#NON_REF_SAMPLES[@]})" >&1 >&2
    exit 100
fi
SAMPLE_INFO=( `echo ${NON_REF_SAMPLES[$((SAMPLE_NUM-1))]} | tr "," "\n"` )
SAMPLE_NAME=${SAMPLE_INFO[0]}
SAMPLE_ID=${SAMPLE_INFO[1]}
SAMPLE_PROJECT=${SAMPLE_INFO[2]}
if [ "$VERBOSE" -eq "1" ]; then
    logMessage "Sample: $SAMPLE_NAME ($SAMPLE_PROJECT)"
fi

PROJECT_OUTPUT_DIR=$ROOT_OUTPUT_DIR/$SAMPLE_PROJECT
SAMPLE_OUTPUT_DIR=$PROJECT_OUTPUT_DIR/$SAMPLE_NAME
mkdir -p $SAMPLE_OUTPUT_DIR

if [ "$UNZIP" -eq "1" ]; then
    ZIPPED_FILES=( `ls $RUN_DIRECTORY/Data/Intensities/BaseCalls/${SAMPLE_PROJECT}/${SAMPLE_ID}/${SAMPLE_NAME}*.fastq.gz` )
    READ_FILES=""
    for ZIPPED_FILE in ${ZIPPED_FILES[@]}; do
        if [[ $ZIPPED_FILE =~ (.+)\.gz ]]; then
            UNZIPPED_FILE=${BASH_REMATCH[1]}
            echo "Unzipping $ZIPPED_FILE" >&2
            zcat $ZIPPED_FILE > $UNZIPPED_FILE
            READ_FILES+=${UNZIPPED_FILE},
        fi
    done
else
    READ_FILES=`ls $RUN_DIRECTORY/Data/Intensities/BaseCalls/${SAMPLE_PROJECT}/${SAMPLE_ID}/${SAMPLE_NAME}*.fastq.gz | tr "\n" ","`
fi
READ_FILES=`chop $READ_FILES`

PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python
CMD="$PYTHON -u $EXACT_MATCHING_SCRIPT -r $READ_FILES --matches $SAMPLE_OUTPUT_DIR/matched.txt --missing $SAMPLE_OUTPUT_DIR/unmatched.txt -b @BASE_CUTOFF@ -a @AVERAGE_CUTOFF@ --read_counts_pickle $SAMPLE_OUTPUT_DIR/read_counts.p --library_pickle $PROJECT_OUTPUT_DIR/library.p --load_lib_from_pickle"
echo "$CMD" >&2
$CMD
EXIT_STATUS=$?

if [ "$EXIT_STATUS" -eq "0" ]; then
    JOB_STATUS=SUCCESS
else
    JOB_STATUS=FAIL
fi
if [ "$JOB_STATUS" == "SUCCESS" ] && ([ ! -f $SAMPLE_OUTPUT_DIR/matched.txt ] ||  [ ! -f $SAMPLE_OUTPUT_DIR/unmatched.txt ]); then
    JOB_STATUS=FAIL
fi

RUN_LOG_FILE=$JOB_DIRECTORY/RUN_RESULT
LOCKFILE=$JOB_DIRECTORY/cc.lock
while [ -e $LOCKFILE ]; do
    sleep 1
done
lockfile $LOCKFILE
echo -e "$JOB_ID\t$SGE_TASK_ID\t@JOB_NAME@\t$SAMPLE_NUM\t`date +\"%F_%T\"`\t$JOB_STATUS" >> $RUN_LOG_FILE
rm -f $LOCKFILE
if [ "$JOB_STATUS" == "FAIL" ] && [ "$EXIT_STATUS" -eq "0" ]; then
    exit 1
else
    exit $EXIT_STATUS
fi