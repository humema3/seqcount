#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PYTHON=/usr/prog/onc/python/Anaconda3-4.1.1/bin/python

source $SCRIPT_DIR/pipeline_steps.sh
source $SCRIPT_DIR/util_functions.sh

declare -A DEFAULTS
DEFAULTS[BASE_QUALITY_CUTOFF]=10
DEFAULTS[AVERAGE_QUALITY_CUTOFF]=30
DEFAULTS[NUM_BASES_TO_KEEP]=20
DEFAULTS[SEQUENCE_CHUNK_SIZE]=3000
DEFAULTS[MAX_HAMMING_DISTANCE]=3
DEFAULTS[MAX_NUM_CONCURRENT_TASKS]=1000
DEFAULTS[ABACUS_USERNAME]='abacus'
DEFAULTS[REPORT_FILENAME]='final_report.txt'
DEFAULTS[CONFIG_FILE]='pipeline_config.txt'
DEFAULTS[LIBRARY_LOCATION]='/mnt/tmplabdata/oncpar/ILLUMINA/MyGenome'

declare -A RUN_STEPS

# deprecated Bash functions
#function create_report_options_usage {
#    echo "-o|--report_filename <filename>: Output filename for report (ends up in run_directory, default = ${DEFAULTS[REPORT_FILENAME]})"
#    echo "-U|--abacus_username <username>: Username to access Abacus database (default = ${DEFAULTS[ABACUS_USERNAME]})"
#}
#
#function seed_based_matching_options_usage {
#    echo "-H|--max_hamming_distance <distance>: Max Hamming distance for sequence matching (default = ${DEFAULTS[MAX_HAMMING_DISTANCE]})"
#    echo "-I|--max_end_indel_distance <distance>: Max \"end indel\" based distance for sequence matching (by default, match -H argument)"
#    echo "-i|--no_indels: Don't use indels to calculate closest sequences (mismatches only)"
#    echo "-s|--sequence_chunk_size <size>: Number of sequences per chunk per sample for parallelization (default = ${DEFAULTS[SEQUENCE_CHUNK_SIZE]})"
#    echo "-c|--max_num_concurrent_tasks <number>: Number of tasks that can run in parallel on the cluster (default = ${DEFAULTS[MAX_NUM_CONCURRENT_TASKS]})"
#}
#
#function exact_matching_options_usage {
#    echo "-b|--base_quality_cutoff <cutoff>: Minimum individual base quality cutoff for read filtering (default = ${DEFAULTS[BASE_QUALITY_CUTOFF]})"
#    echo "-a|--average_quality_cutoff <cutoff>: Minimum average base quality cutoff for read filtering (default = ${DEFAULTS[AVERAGE_QUALITY_CUTOFF]})"
#    echo "-u|--unzip: Unzip fastq.gz files before reading and keep the unzipped fastq files"
#}
#
#function email_options_usage {
#    echo "-e|--email <address>: Email address for notifications on cluster jobs (defaults to email for current user account unless -n is supplied)"
#    echo "-n|--no_email: No email notifications"
#}
#
#function cascading_options_usage {
#    echo "############# Cascading options (for next steps) ##############"
#    echo "-N|--run_next_step: Start the next step (as well as the current one) automatically without prompting (can only use default job name and max runtime; email notification settings will be retained from this script if supplied)"
#    echo "-A|--auto_cascade: Auto-cascade (keep running the next step without prompting for all downstream steps)"
#}
#
#function trim_reads_options_usage {
#    echo "-B|--num_bases_to_keep <num_bases_to_keep>: Number of bases to keep after trimming (make sure it's no more than the number of bases there currently; default = ${DEFAULTS[NUM_BASES_TO_KEEP]})"
#}
#
#function parent_job_options_usage {
#    echo "-P|--parent_job <parent_job>: Id of parent job (either cluster job or bash process id, depending on job) - wait for this job to finish before staring current step"
#}
#
#function generic_options_usage {
#    echo "-r|--run_directory <run_directory>: bcl2fastq2-compliant top level directory for the sequencing run"
#    echo "Options:"
#    echo "-h|--help: Display this message"
#    echo "-C|--config_file: Config filename found in run_directory to define default options"
#    echo -e "\t(use format arg=val, where arg is the long form of the argument in all caps)"
#    echo -e "\t(for no-argument flags, use val = 1, or 0 to explicitly not set the flag)"
#    echo "-R|--autorun: Run the current step after script preparation without prompting"
#}
#
#function demultiplex_options_usage {
#    echo "-O|--opts_string <options_string>: string of optional arguments to supply to bcl2fastq(2); make sure to enclose the OPTS_STRING in quotes (empty by default; i.e., uses application defaults)"
#}
#
#function cluster_job_options_usage {
#    echo "-j <job_name>: Name of cluster job (default = ${DEFAULTS[JOB_NAME]})"
#    echo "-m <max_runtime>: Max runtime in seconds for submitted cluster jobs (default = ${DEFAULTS[MAX_RUNTIME]})"
#    email_options_usage
#}
#
#function usage {
#    echo "Usage: $SCRIPT_NAME -r <run_directory> [options]"
#    generic_options_usage
#    local STEP_NAME=${PIPELINE_STEPS[$((STEP_NUMBER-1))]}
#    ${STEP_NAME}_options_usage
#    if [ "$CLUSTER_JOB" -eq "1" ]; then
#        cluster_job_options_usage
#    fi
#    if [ "$CASCADING" -eq "1" ]; then
#        cascading_options_usage
#    fi
#    if [ "$USE_EMAIL" -eq "1" ] && [ "$CLUSTER_JOB" -eq "0" ]; then
#        email_options_usage
#    fi
#    declare -A FORMATTED_STEPS
#    FORMATTED_STEPS[exact_matching]="EXACT MATCHING"
#    FORMATTED_STEPS[trim_reads]="READ TRIMMING"
#    FORMATTED_STEPS[seed_based_matching]="SEED-BASED MATCHING"
#    FORMATTED_STEPS[create_report]="REPORT CREATION"
#    for i in `seq 2 ${#PIPELINE_STEPS[@]}`; do
#        STEP_NAME=${PIPELINE_STEPS[$((i-1))]}
#        if [ "$STEP_NAME" != "abacus_upload" ] && [ "${RUN_STEPS[$STEP_NAME]}" -eq "1" ] && [ "$SCRIPT_NAME" != "${STEP_NAME}.sh" ]; then
#            echo "##### OPTIONS FOR ${FORMATTED_STEPS[$STEP_NAME]}: #####"
#            ${STEP_NAME}_options_usage
#        fi
#    done
#}

#GENERIC_GETOPTS_STR="hr:C:R"
ABACUS_LOGIN_GETOPTS_STR="p:U:"
GENERIC_GETOPTS_STR="hC:RvL:F:${ABACUS_LOGIN_GETOPTS_STR}"
EMAIL_GETOPTS_STR="e:n"
CLUSTER_JOB_GETOPTS_STR="j:m:${EMAIL_GETOPTS_STR}"
CASCADING_GETOPTS_STR="NAt"
EXACT_MATCHING_GETOPTS_STR="l:b:a:u"
TRIM_READS_GETOPTS_STR="B:"
SEED_BASED_MATCHING_GETOPTS_STR="H:I:s:c:i"
CREATE_REPORT_GETOPTS_STR="o:"
PARENT_JOB_GETOPTS_STR="P:"
DEMULTIPLEX_GETOPTS_STR="O:"
ABACUS_UPLOAD_GETOPTS_STR="T"

LONG_ABACUS_LOGIN_GETOPTS_STR="abacus_password:,abacus_username:,"
LONG_GENERIC_GETOPTS_STR="help,config_file:,autorun,verbose,log_file:,pool_ref_config_file:,${LONG_ABACUS_LOGIN_GETOPTS_STR}"
LONG_EMAIL_GETOPTS_STR="email:,no_email"
LONG_CLUSTER_JOB_GETOPTS_STR="job_name:,max_runtime:,${LONG_EMAIL_GETOPTS_STR}"
LONG_CASCADING_GETOPTS_STR="run_next_step,auto_cascade"
LONG_EXACT_MATCHING_GETOPTS_STR="library_location:base_quality_cutoff:,average_quality_cutoff:,unzip"
LONG_TRIM_READS_GETOPTS_STR="num_bases_to_keep:"
LONG_SEED_BASED_MATCHING_GETOPTS_STR="max_hamming_distance:,max_end_indel_distance:,sequence_chunk_size:,max_num_concurrent_tasks:,no_indels"

LONG_CREATE_REPORT_GETOPTS_STR="report_filepath:"
LONG_ABACUS_UPLOAD_GETOPTS_STR="use_abacus_test"
LONG_PARENT_JOB_GETOPTS_STR="parent_job:"
LONG_DEMULTIPLEX_GETOPTS_STR="opts_string:"

function assignPipelineFlags {
    CLUSTER_JOB=0
    USE_EMAIL=0
    CASCADING=0
    HAS_PARENT_JOB=0
    RUN_STEPS[demultiplex]=0
    RUN_STEPS[exact_matching]=0
    RUN_STEPS[trim_reads]=0
    RUN_STEPS[seed_based_matching]=0
    RUN_STEPS[create_report]=0
    SCRIPT_NAME=""

    GETOPTS_STR=":${GENERIC_GETOPTS_STR}"
    LONG_GETOPTS_STR=$LONG_GENERIC_GETOPTS_STR
#    while getopts ":ceapDETSCAUj:m:J:" opt; do
    local ARGS=$(getopt -o ":ceapDETSCUj:m:J:" -- "$@")
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "Getopt malfunction" >&2
        return $EXIT_STATUS
    fi
    eval set -- "$ARGS"
    local opt=""
    while true; do
        opt=$1
        shift
        case $opt in
            -c)
                CLUSTER_JOB=1
                GETOPTS_STR+=$CLUSTER_JOB_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_CLUSTER_JOB_GETOPTS_STR"
                ;;
            -e)
                USE_EMAIL=1
                if [ "$CLUSTER_JOB" -eq "0" ]; then
                    GETOPTS_STR+=$EMAIL_GETOPTS_STR
                    LONG_GETOPTS_STR+=",$LONG_EMAIL_GETOPTS_STR"
                fi
                ;;
            -a)
                CASCADING=1
                GETOPTS_STR+=$CASCADING_GETOPTS_STR
                LONG_GETOPTS_STR+=$LONG_CASCADING_GETOPTS_STR
                ;;
            -p)
                HAS_PARENT_JOB=1
                GETOPTS_STR+=$PARENT_JOB_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_PARENT_JOB_GETOPTS_STR"
                ;;
            -D)
                RUN_STEPS[demultiplex]=1
                GETOPTS_STR+=$DEMULTIPLEX_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_DEMULTIPLEX_GETOPTS_STR"
                ;;
            -E)
                RUN_STEPS[exact_matching]=1
                GETOPTS_STR+=$EXACT_MATCHING_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_EXACT_MATCHING_GETOPTS_STR"
                ;;
            -T)
                RUN_STEPS[trim_reads]=1
                GETOPTS_STR+=$TRIM_READS_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_TRIM_READS_GETOPTS_STR"
                ;;
            -S)
                RUN_STEPS[seed_based_matching]=1
                GETOPTS_STR+=$SEED_BASED_MATCHING_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_SEED_BASED_MATCHING_GETOPTS_STR"
                ;;
            -C)
                RUN_STEPS[create_report]=1
                GETOPTS_STR+=$CREATE_REPORT_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_CREATE_REPORT_GETOPTS_STR"
                ;;
            -U)
                GETOPTS_STR+=$ABACUS_UPLOAD_GETOPTS_STR
                LONG_GETOPTS_STR+=",$LONG_ABACUS_UPLOAD_GETOPTS_STR"
                ;;
            -j)
                DEFAULTS[JOB_NAME]=$1
                shift
                ;;
            -m)
                DEFAULTS[MAX_RUNTIME]=$1
                shift
                ;;
            -J)
                SCRIPT_NAME=$1
                shift
                ;;
            --)
                break
                ;;
            *)
                echo "Argument processing error: Unrecognized pipeline flag: $opt" >&2
                exit 1
                ;;
        esac
    done
    if [ -z "$SCRIPT_NAME" ]; then
        echo "Script name not supplied to pipeline options" >&2
        exit 1
    fi
}

function demultiplex_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-O', '--opts_string', help='String of optional arguments to supply to bcl2fastq(2) (empty by default; i.e., uses application defaults)', type=str, required=False);
parser.print_help()"
}

function generic_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-C', '--config_file', help='Config filename found in run_directory to define default options (use format arg=val, where arg is the long form of the argument in all caps; for no-argument flags, use val = 1, or 0 to explicitly not set the flag)', type=str, required=False, default='pipeline_config.txt');
parser.add_argument('-F', '--pool_ref_config_file', help='Config filename found in run_directory to define mapping between pool and reference sample SMF id; default is to query Abacus.AB_POOL_MV', type=str, required=False);
parser.add_argument('-R', '--autorun', help='Submit the current step after preparation, without prompting', default=False, action='store_true');
parser.add_argument('-v', '--verbose', help='Turn on verbose logging output', default=False, action='store_true');
parser.add_argument('-L', '--log_file', help='Write stderr to this log file instead of the console (supply full path or else file is placed in run directory)', type=str, required=False, default=None)
parser.print_help()"
}

function cluster_job_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-j', '--job_name', help='Name of cluster job (default = ${DEFAULTS[JOB_NAME]})', type=str, required=False, default='${DEFAULTS[JOB_NAME]}');
parser.add_argument('-m', '--max_runtime', help='Max runtime in seconds for submitted cluster jobs (default = ${DEFAULTS[MAX_RUNTIME]})', type=int, required=False, default=${DEFAULTS[MAX_RUNTIME]});
parser.add_argument('-e', '--email', help='Email address for notifications on cluster jobs (defaults to email for current user account unless -n is supplied)', type=str, required=False);
parser.add_argument('-n', '--no_email', help='No email notifications', default=False, action='store_true');
parser.print_help()"
}

function email_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-e', '--email', help='Email address for notifications on cluster jobs (defaults to email for current user account unless -n is supplied)', type=str, required=False);
parser.add_argument('-n', '--no_email', help='No email notifications', default=False, action='store_true');
parser.print_help()"
}

function cascading_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-N', '--run_next_step', help='Submit the current step and prep the next one automatically without prompting (can only use default job name and max runtime; email notification settings will be retained from this script if supplied)', default=False, action='store_true');
parser.add_argument('-A', '--auto_cascade', help='Auto-cascade (keep running the next step without prompting for all downstream steps; see caveats for -N)', default=False, action='store_true');
parser.add_argument('-t', '--trim_reads', help='Trim reads after exact matching (only utilized when running demultiplexing or exact matching', default=False, action='store_true');
parser.print_help()"
}

function parent_job_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-P', '--parent_job', help='Wait for cluster job or Bash process (depending on step) with this id to stop running before executing step', default=False, action='store_true');
parser.print_help()"
}

function trim_reads_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-B', '--num_bases_to_keep', help='Number of bases to keep after trimming (make sure it\'s no more than the number of bases there currently, default = ${DEFAULTS[NUM_BASES_TO_KEEP]})', type=int, required=False, default=${DEFAULTS[NUM_BASES_TO_KEEP]});
parser.print_help()"
}

function exact_matching_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-l', '--library_location', help='Path to library FASTA file, or else the root directory where all library files (default = ${DEFAULTS[LIBRARY_LOCATION]}) can be found as <POOL>.fa or <POOL>/<POOL>.fa', type=str, required=False, default=${DEFAULTS[LIBRARY_LOCATION]});
parser.add_argument('-b', '--base_quality_cutoff', help='Minimum individual base quality cutoff for read filtering (default = ${DEFAULTS[BASE_QUALITY_CUTOFF]})', type=int, required=False, default=${DEFAULTS[BASE_QUALITY_CUTOFF]});
parser.add_argument('-a', '--average_quality_cutoff', help='Minimum average base quality cutoff for read filtering (default = ${DEFAULTS[AVERAGE_QUALITY_CUTOFF]}', type=int, required=False, default=${DEFAULTS[AVERAGE_QUALITY_CUTOFF]});
parser.add_argument('-u', '--unzip', help='Unzip fastq.gz files before reading and keep the unzipped fastq files', default=False, action='store_true');
parser.print_help()"
}

function seed_based_matching_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-H', '--max_hamming_distance', help='Max Hamming distance for sequence matching (default = ${DEFAULTS[MAX_HAMMING_DISTANCE]})', type=int, required=False, default=${DEFAULTS[MAX_HAMMING_DISTANCE]});
parser.add_argument('-I', '--max_end_indel_distance', help='Max \'end indel\' based distance for sequence matching (by default, match -H argument)', type=int, required=False);
parser.add_argument('-i', '--no_indels', help='Don\'t use indels to calculate closest sequences (mismatches only)', default=False, action='store_true');
parser.add_argument('-s', '--sequence_chunk_size', help='Number of sequences per chunk per sample for parallelization (default = ${DEFAULTS[SEQUENCE_CHUNK_SIZE]})', type=int, required=False, default=${DEFAULTS[SEQUENCE_CHUNK_SIZE]});
parser.add_argument('-c', '--max_num_concurrent_tasks', help='Number of tasks that can run in parallel on the cluster (default = ${DEFAULTS[MAX_NUM_CONCURRENT_TASKS]})', type=int, required=False, default=${DEFAULTS[MAX_NUM_CONCURRENT_TASKS]});
parser.print_help()"
}

function create_report_options_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-o', '--report_filename', help='Output filename for report (ends up in run_directory, default = ${DEFAULTS[REPORT_FILENAME]})', type=str, required=False, default='${DEFAULTS[REPORT_FILENAME]}');
parser.print_help()"
}

function abacus_login_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('abacus_password', help='Password to access Abacus database with supplied username (see -U) (can optionally supply with -p flag)', type=str);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-U', '--abacus_username', help='Username to access Abacus database (default = ${DEFAULTS[ABACUS_USERNAME]})', type=str, required=False, default='${DEFAULTS[ABACUS_USERNAME]}');
parser.print_help()"
}

function abacus_upload_usage_py {
    $PYTHON -c "from argparse import ArgumentParser; parser = ArgumentParser(prog='$SCRIPT_NAME', add_help=False);
parser.add_argument('run_directory', help='bcl2fastq2-compliant top level directory for the sequencing run (can optionally supply with -r flag)', type=str);
parser.add_argument('-T', '--upload_to_abacus_test', help='Upload to Abacus test database instead of prod', default=False, action='store_true');
parser.print_help()"
}

function usage_py {
    declare -A FORMATTED_STEPS/
    FORMATTED_STEPS[demultiplex]="DEMULTIPLEXING"
    FORMATTED_STEPS[exact_matching]="EXACT MATCHING"
    FORMATTED_STEPS[trim_reads]="READ TRIMMING"
    FORMATTED_STEPS[seed_based_matching]="SEED-BASED MATCHING"
    FORMATTED_STEPS[create_report]="REPORT CREATION"

    local STEP_NAME=${PIPELINE_STEPS[$((STEP_NUMBER-1))]}

    if [ "$STEP_NAME" == "abacus_upload" ]; then
        echo
        echo "############# ABACUS LOGIN OPTIONS ############"
        abacus_login_usage_py
        echo
        echo "############# ABACUS UPLOAD OPTIONS #############"
        abacus_upload_usage_py
    else
        echo
        echo "############# GENERIC PIPELINE OPTIONS #############"
        generic_options_usage_py
        echo
        echo "############# ABACUS LOGIN OPTIONS ############"
        abacus_login_usage_py

        if [ "$HAS_PARENT_JOB" -eq "1" ]; then
            echo
            echo "############# TO WAIT FOR PARENT JOB TO FINISH #############"
            parent_job_options_usage_py
        fi

        echo
        echo "############# ${FORMATTED_STEPS[$STEP_NAME]} SPECIFIC OPTIONS #############"
        ${STEP_NAME}_options_usage_py

        if [ "$CLUSTER_JOB" -eq "1" ]; then
            echo
            echo "############# CLUSTER JOB OPTIONS #############"
            cluster_job_options_usage_py
        fi

        if [ "$CASCADING" -eq "1" ]; then
            echo
            echo "############# CASCADING OPTIONS (FOR NEXT STEPS) ##############"
            cascading_options_usage_py
            if [ "$USE_EMAIL" -eq "1" ] && [ "$CLUSTER_JOB" -eq "0" ]; then
                echo
                echo "##### EMAIL OPTIONS ######"
                email_options_usage_py
            fi
            for i in `seq 2 $((${#PIPELINE_STEPS[@]}-1))`; do
                STEP_NAME=${PIPELINE_STEPS[$((i-1))]}
                if [ "${RUN_STEPS[$STEP_NAME]}" -eq "1" ] && [ "$SCRIPT_NAME" != "${STEP_NAME}.sh" ]; then
                    echo
                    echo "##### OPTIONS FOR ${FORMATTED_STEPS[$STEP_NAME]}: #####"
                    ${STEP_NAME}_options_usage_py
                fi
            done
        fi
    fi
    echo
}

function assignOptionsFromConfigFile {
    local CONFIG_FILE=$1
    declare -A CURRENT_OPTIONS
    for OPTION in ${OPTIONS[@]}; do
        CURRENT_OPTIONS[$OPTION]=${!OPTION}
    done
    source $CONFIG_FILE
    # for any values already assigned on the command line, re-override the config file values
    for OPTION in ${OPTIONS[@]}; do
        if [ "${ASSIGNED[$OPTION]}" -eq "1" ]; then
            eval "$OPTION=${CURRENT_OPTIONS[$OPTION]}"
        fi
    done
}

function assignArgs {
    # initialize arguments
    local NO_ARG_OPTIONS=( HELP AUTORUN NO_EMAIL RUN_NEXT_STEP AUTO_CASCADE UNZIP TRIM_READS NO_INDELS VERBOSE UPLOAD_TO_ABACUS_TEST )
    local NO_DEFAULT_OPTIONS=( LOG_FILE CONFIG_FILE RUN_DIRECTORY OPTS_STRING EMAIL PARENT_JOB MAX_END_INDEL_DISTANCE ABACUS_PASSWORD REPORT_FILEPATH POOL_REF_CONFIG_FILE )
    local DEFAULT_OPTIONS=( JOB_NAME MAX_RUNTIME LIBRARY_LOCATION BASE_QUALITY_CUTOFF AVERAGE_QUALITY_CUTOFF NUM_BASES_TO_KEEP MAX_HAMMING_DISTANCE SEQUENCE_CHUNK_SIZE MAX_NUM_CONCURRENT_TASKS ABACUS_USERNAME )
    for OPTION in ${NO_ARG_OPTIONS[@]}; do
        eval "$OPTION=0"
    done
    for OPTION in ${NO_DEFAULT_OPTIONS[@]}; do
        eval "$OPTION=\"\""
    done
    for OPTION in ${DEFAULT_OPTIONS[@]}; do
        eval "$OPTION=${DEFAULTS[$OPTION]}"
    done

    # define array of all runtime options
    OPTIONS=( "${NO_ARG_OPTIONS[@]}" "${NO_DEFAULT_OPTIONS[@]}" "${DEFAULT_OPTIONS[@]}" )
    # define associative array indicating whether option has been assigned on the command line
    declare -A ASSIGNED
    for OPTION in ${OPTIONS[@]}; do
        ASSIGNED[$OPTION]=0
    done

    # assign arguments their values from command line
    local ARGS=$(getopt -o "$GETOPTS_STR" --long "$LONG_GETOPTS_STR" -- "$@")
    local EXIT_STATUS=$?
    if [ "$EXIT_STATUS" -ne "0" ]; then
        echo "Getopt malfunction" >&2
        return $EXIT_STATUS
    fi
    eval set -- "$ARGS"
    local opt=""
    while true; do
        opt=$1
        shift
        case $opt in
            -h|--help)
                HELP=1
                usage_py
                return 0
                ;;
            -v|--verbose)
                VERBOSE=1
                ASSIGNED[VERBOSE]=1
                ;;
            -r|--run_directory)
                RUN_DIRECTORY=$1
                shift
                ASSIGNED[RUN_DIRECTORY]=1
                ;;
            -C|--config_file)
                CONFIG_FILE=$1
                shift
                ASSIGNED[CONFIG_FILE]=1
                ;;
            -F|--pool_ref_config_file)
                POOL_REF_CONFIG_FILE=$1
                shift
                ASSIGNED[POOL_REF_CONFIG_FILE]=1
                ;;
            -L|--log_file)
                LOG_FILE=$1
                shift
                ASSIGNED[LOG_FILE]=1
                ;;
            -R|--autorun)
                AUTORUN=1
                ASSIGNED[AUTORUN]=1
                ;;
            -O|--opts_string)
                OPTS_STRING=$1
                shift
                ASSIGNED[OPTS_STRING]=1
                ;;
            -j|--job_name)
                JOB_NAME=$1
                shift
                ASSIGNED[JOB_NAME]=1
                ;;
            -m|--max_runtime)
                MAX_RUNTIME=$1
                shift
                ASSIGNED[MAX_RUNTIME]=1
                ;;
            -e|--email)
                EMAIL=$1
                shift
                ASSIGNED[EMAIL]=1
                ;;
            -n|--no_email)
                NO_EMAIL=1
                ASSIGNED[NO_EMAIL]=1
                ;;
            -N|--run_next_step)
                RUN_NEXT_STEP=1
                ASSIGNED[RUN_NEXT_STEP]=1
                ;;
            -A|--auto_cascade)
                AUTO_CASCADE=1
                ASSIGNED[AUTO_CASCADE]=1
                ;;
            -P|--parent_job)
                PARENT_JOB=$1
                shift
                ASSIGNED[PARENT_JOB]=1
                ;;
            -l|--library_location)
                LIBRARY_LOCATION=$1
                shift
                ASSIGNED[LIBRARY_LOCATION]=1
                ;;
            -b|--base_quality_cutoff)
                BASE_QUALITY_CUTOFF=$1
                shift
                ASSIGNED[BASE_QUALITY_CUTOFF]=1
                ;;
            -a|--average_quality_cutoff)
                AVERAGE_QUALITY_CUTOFF=$1
                shift
                ASSIGNED[AVERAGE_QUALITY_CUTOFF]=1
                ;;
            -u|--unzip)
                UNZIP=1
                ASSIGNED[UNZIP]=1
                ;;
            -t|--trim_reads)
                TRIM_READS=1
                ASSIGNED[TRIM_READS]=1
                ;;
            -B|--num_bases_to_keep)
                TRIM_READS=1
                NUM_BASES_TO_KEEP=$1
                shift
                ASSIGNED[TRIM_READS]=1
                ASSIGNED[NUM_BASES_TO_KEEP]=1
                ;;
            -H|--max_hamming_distance)
                MAX_HAMMING_DISTANCE=$1
                shift
                ASSIGNED[MAX_HAMMING_DISTANCE]=1
                ;;
            -I|--max_end_indel_distance)
                MAX_END_INDEL_DISTANCE=$1
                shift
                ASSIGNED[MAX_END_INDEL_DISTANCE]=1
                ;;
            -s|--sequence_chunk_size)
                SEQUENCE_CHUNK_SIZE=$1
                shift
                ASSIGNED[SEQUENCE_CHUNK_SIZE]=1
                ;;
            -c|--max_num_concurrent_tasks)
                MAX_NUM_CONCURRENT_TASKS=$1
                shift
                ASSIGNED[MAX_NUM_CONCURRENT_TASKS]=1
                ;;
            -i|--no_indels)
                NO_INDELS=1
                ASSIGNED[NO_INDELS]=1
                ;;
            -p|--abacus_password)
                ABACUS_PASSWORD=$1
                shift
                ASSIGNED[ABACUS_PASSWORD]=1
                ;;
            -o|--report_filename)
                REPORT_FILENAME=$1
                shift
                ASSIGNED[REPORT_FILENAME]=1
                ;;
            -U|--abacus_username)
                ABACUS_USERNAME=$1
                shift
                ASSIGNED[ABACUS_USERNAME]=1
                ;;
            -T|--upload_to_abacus_test)
                UPLOAD_TO_ABACUS_TEST=1
                ASSIGNED[UPLOAD_TO_ABACUS_TEST]=1
                ;;
            --)
                break
                ;;
            *)
                echo "Argument processing error: Unrecognized argument: $opt" >&2
                usage_py >&2
                return 1
                ;;
        esac
    done
    if [ "$#" -eq "2" ]; then
        if [ "${ASSIGNED[ABACUS_PASSWORD]}" -eq "0" ]; then
            ABACUS_PASSWORD=$1
            ASSIGNED[ABACUS_PASSWORD]=1
            shift
        elif [ "$ABACUS_PASSWORD" != "$1" ]; then
            echo "Argument processing error: Two different Abacus passwords supplied: 1) $ABACUS_PASSWORD 2) $1" >&2
            usage_py >&2
            return 1
        fi
    fi
    if [ "$#" -eq "1" ]; then
        if [ "${ASSIGNED[RUN_DIRECTORY]}" -eq "0" ]; then
            RUN_DIRECTORY=$1
            ASSIGNED[RUN_DIRECTORY]=1
            shift
        elif [ "$RUN_DIRECTORY" != "$1" ]; then
            echo "Argument processing error: Two different run directories supplied: 1) $RUN_DIRECTORY 2) $1" >&2
            usage_py >&2
            return 1
        fi
    fi
    if [ "$#" -ne "0" ]; then
        echo Argument processing error: too many positional arguments. Arguments remaining: "$@" >&2
        usage_py >&2
        return 1
    fi
    # validate run directory
    if [ -z "$RUN_DIRECTORY" ]; then
        echo "Run directory (-r) not supplied" >&2
        usage_py >&2
        return 1
    fi
    # remove any trailing slash to avoid confusion
    RUN_DIRECTORY=`removeTrailingSlash $RUN_DIRECTORY`
    # convert to absolute path if necessary
    if [[ ! "$RUN_DIRECTORY" =~ /.+ ]]; then
        RUN_DIRECTORY=`pwd`/$RUN_DIRECTORY
    fi
    if [ ! -d $RUN_DIRECTORY ]; then
        echo "Directory $RUN_DIRECTORY not found" >&2
        return 1
    fi

    # assign remaining arguments from config file if it was supplied
    if [ ! -z "$CONFIG_FILE" ]; then
        # first make sure it's in the run directory and that the file exists
        if [ "`dirname $CONFIG_FILE`" != "$RUN_DIRECTORY" ]; then
            CONFIG_FILE=$RUN_DIRECTORY/$CONFIG_FILE
        fi
        if [ ! -f "$CONFIG_FILE" ]; then
            echo "Config file $CONFIG_FILE not found" >&2
            usage_py >&2
            return 1
        fi
        assignOptionsFromConfigFile $CONFIG_FILE
    else # try default
        CONFIG_FILE=$RUN_DIRECTORY/${DEFAULTS[CONFIG_FILE]}
        if [ -f "$CONFIG_FILE" ]; then
            assignOptionsFromConfigFile $CONFIG_FILE
        fi
    fi

    if [ ! -z "$LOG_FILE" ] && [ "$LOG_FILE" == "`basename $LOG_FILE`" ]; then
        LOG_FILE=$RUN_DIRECTORY/`basename $LOG_FILE`
    fi
}

function checkArgs {
    if [ "$CLUSTER_JOB" -eq "1" ]; then
        if [ -z "$EMAIL" ]; then
            EMAIL_ADDRESS_CONFIG=""
            if [ "$NO_EMAIL" -ne "0" ]; then
                EMAIL_NOTIFICATIONS_CONFIG='#\$ -m n'
            else
                EMAIL_NOTIFICATIONS_CONFIG='#\$ -m beas'
            fi
        else
            EMAIL_ADDRESS_CONFIG="#\$ -M $EMAIL"
            EMAIL_NOTIFICATIONS_CONFIG='#\$ -m beas'
        fi
        if [[ ! $MAX_RUNTIME =~ ^[0-9]+$ ]]; then
            echo "max_runtime is not an integer:" $MAX_RUNTIME >&2
            return 1
        fi
    fi
    if [ "$HAS_PARENT_JOB" -eq "1" ] && [ ! -z "$PARENT_JOB" ]; then
        PARENT_JOB_CONFIG="#\$ -hold_jid $PARENT_JOB"
    else
        PARENT_JOB_CONFIG=""
    fi
    if [ "${RUN_STEPS[exact_matching]}" -eq "1" ]; then
        if [[ ! $BASE_QUALITY_CUTOFF =~ ^[0-9]+$ ]]; then
            echo "base_quality_cutoff is not a positive integer:" $BASE_QUALITY_CUTOFF >&2
            return 1
        fi
        if [[ ! $AVERAGE_QUALITY_CUTOFF =~ ^[0-9]+$ ]]; then
            echo "average_quality_cutoff is not a positive integer:" $AVERAGE_QUALITY_CUTOFF >&2
            return 1
        fi
    fi
    if [ "${RUN_STEPS[trim_reads]}" -eq "1" ] && [[ ! $NUM_BASES_TO_KEEP =~ ^[0-9]+$ ]]; then
        echo "num_bases_to_keep is not a positive integer:" $NUM_BASES_TO_KEEP >&2
        return 1
    fi
    if [ "${RUN_STEPS[seed_based_matching]}" -eq "1" ]; then
        if [[ ! $MAX_NUM_CONCURRENT_TASKS =~ ^[0-9]+$ ]]; then
            echo "max_num_concurrent_tasks is not an integer:" $MAX_NUM_CONCURRENT_TASKS >&2
            return 1
        fi
        if [[ ! $MAX_HAMMING_DISTANCE =~ ^[0-9]+$ ]]; then
            echo "max_hamming_distance is not an integer:" $MAX_HAMMING_DISTANCE >&2
            return 1
        fi
        if [ -z "$MAX_END_INDEL_DISTANCE" ]; then
            MAX_END_INDEL_DISTANCE=$MAX_HAMMING_DISTANCE
        fi
        if [[ ! $MAX_END_INDEL_DISTANCE =~ ^[0-9]+$ ]]; then
            echo "max_end_indel_distance is not an integer:" $MAX_END_INDEL_DISTANCE >&2
            return 1
        fi
    fi
    if [ "${RUN_STEPS[create_report]}" -eq "1" ]; then
        if ([ "$SCRIPT_NAME" == "abacus_upload.sh" ] || [ "$AUTO_CASCADE" -eq "1" ] || ([ "$SCRIPT_NAME" == "create_report.sh" ] && [ "$RUN_NEXT_STEP" -eq "1" ])) && [ -z "$ABACUS_PASSWORD" ]; then
            echo "Abacus password (-p) not supplied" >&2
            usage_py >&2
            return 1
        fi
        if [ -z "$REPORT_FILENAME" ]; then
            REPORT_FILEPATH=$RUN_DIRECTORY/${DEFAULTS[REPORT_FILENAME]}
        elif [ "`dirname $REPORT_FILENAME`" != "`removeTrailingSlash $RUN_DIRECTORY`" ]; then
            REPORT_FILEPATH=$RUN_DIRECTORY/$REPORT_FILENAME
        else
            REPORT_FILEPATH=$REPORT_FILENAME
        fi
        mkdir -p `dirname $REPORT_FILEPATH`
    fi
    return 0
}
