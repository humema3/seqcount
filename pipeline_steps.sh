#!/usr/bin/env bash

declare -a PIPELINE_STEPS
PIPELINE_STEPS[0]=demultiplex
PIPELINE_STEPS[1]=trim_reads
PIPELINE_STEPS[2]=exact_matching
PIPELINE_STEPS[3]=seed_based_matching
PIPELINE_STEPS[4]=create_report
PIPELINE_STEPS[5]=abacus_upload