#!/bin/bash

COUNTS=$1
UNMATCHED=$2

if [ -z $COUNTS ] || [ -z $UNMATCHED ]; then
  echo "Usage: $0 <counts_file> <unmatched_file>"
  exit 1
fi

ALIGN_PCT=`python -c "def calculateAlignmentPercentage(countsFile, unmatchedFile):
    numMatched = 0
    c = open(countsFile, 'r')
    for line in c:
        numMatched += float(line.strip().split(\"\t\")[2])
    c.close()
    numUnmatched = 0
    u = open(unmatchedFile, 'r')
    for line in u:
        numUnmatched += int(line.strip().split(\"\t\")[1])
    u.close()
    return numMatched/(numMatched + numUnmatched) * 100

print(calculateAlignmentPercentage('$COUNTS', '$UNMATCHED'))"`

printf "%.2f\n" $ALIGN_PCT